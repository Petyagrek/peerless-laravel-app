<?php

namespace App\Services;
use App\Models\Company;

class CompanyService {

    public function get ($request, $shopID)
    {
        return $this->getCompanniesDB($request, $shopID);
    }

    /**
     * Get companies for customer select
     * @param int $shopID
     * 
     * @return array \App\Models\Company
     */
    public function getCustomerCompanies($shopID)
    {
        return Company::where('shop_id', $shopID)
            ->get(['id as value', 'name', 'ifs_id'])
            ->each(function ($company) {
                $company->label = $company->ifs_id . ', ' . $company->name;
                return ;
            });
    }

    public function show ($id)
    {
        return Company::with('customers')->find($id);
    }

    public function getCompanniesDB($request, $shopID) {
        $query = Company::where('shop_id', $shopID);
        if (isset($request['id']))
            return $query->find($request['id']);
        else if (isset($request['searchValue']))
            $query->where('name', 'like', '%'.$request['searchValue'].'%');

        return $query->get();
    }

    public function store ($createData, $shopID)
    {
        $companyExist = Company::where([
            'name' => $createData['name'],
            'shop_id' => $shopID,
        ])->get();

        if ($companyExist->count()) {
            return response()->json([
                'error' => true,
                'message' => "Company with name=\"".$createData['name']."\" already exist",
            ], 200);
        }

        $company          = new Company;
        $company->name    = $createData['name'];
        $company->ifs_id  = $createData['ifs_id'];
        $company->shop_id = $shopID;
        $company->save();

        return response()->json([
            'error' => false,
            'message' => "Company succesfuly created",
        ], 201);
    }

    public function update ($updateData, $id)
    {
        $error = false;
        $code = null;

        $Company = Company::find($id);
        try {
            $Company->update($updateData);
            $message = "Company succesfuly updated";
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
            $code = $e->getCode();
            // duplicated data error
            if ($code == '23000') {
                $message = 'Company with this name already exists';
            }
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'code' => $code
        ], 200);
    }
    
    public function destroy ($data) 
    {
        Company::destroy($data);
    }
}