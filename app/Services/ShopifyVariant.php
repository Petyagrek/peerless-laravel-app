<?php

namespace App\Services;

use App\Models\Variant;

class ShopifyVariant
{
  public static function updateOrCreate($variant, $productID, $shopID)
  {
    Variant::updateOrCreate(
      [
        'product_id' => (int)$productID, 'variant_id' => (int)$variant->id, 'shop_id' => (int)$shopID
      ],
      [
        'sku' => $variant->sku
      ]
    );
  }

  public static function storeVariants($product, $shopID)
  {
    foreach ($product->variants as $variant) {
      if (!empty($variant->sku))
        self::updateOrCreate($variant, $product->id, $shopID);
    }
  }
}
