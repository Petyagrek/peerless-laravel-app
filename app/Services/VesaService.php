<?php
namespace App\Services;

use App\Interfaces\VesaInterface;
use App\Models\Vesa;

class VesaService implements VesaInterface {

    const MAX_ITEMS = 5000;

    public function search($query)
    {
        return Vesa::queryPair1($query['x'], $query['y'])
            ->queryPair2($query['x'], $query['y'])
            ->queryPair3($query['x'], $query['y'])
            ->queryPair4($query['x'], $query['y'])
            ->queryPair5($query['x'], $query['y'])
            ->queryPair6($query['x'], $query['y'])
            ->queryPair7($query['x'], $query['y'])
            ->join('variants', function ($q) {
                $q->on('vesas.product_handle', '=', 'variants.sku')
                    ->where('shop_id', '=', env('MAIN_SHOP_ID'));
            })
            ->get(['variant_id']);
    }

    public function createFromCsvArr($items)
    {
        \DB::table('vesas')->truncate();
        for($index = 0; $index < count($items); $index+=$this->step($items))
            Vesa::insert(array_slice($items, $index, $this->step($items)));
    }

    private function step($items)
    {
        if($this->last_index($items) < self::MAX_ITEMS) {
            return $this->last_index($items);
        }
        else {
            return self::MAX_ITEMS;
        }
    }

    private function last_index($items)
    {
        return (count($items)-1);
    }

}