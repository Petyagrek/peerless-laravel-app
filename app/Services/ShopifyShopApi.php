<?php

namespace App\Services;

class ShopifyShopApi
{
  private $api;
  private const API_URL = '/admin/api/2020-01';

  private $response = [
    'success' => false,
    'data' => null,
  ];

  public function __construct($api)
  {
    $this->api = $api;
  }


  public function shop () {
    $url = self::API_URL . '/shop.json';

    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('GET', $url)->body
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      // $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }

  public function getMetafields ($params = [])
  {
    $url = self::API_URL . '/metafields.json';
    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('GET', $url, $params)
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      // \Log::debug(print_r($e->getResponse()->getBody()->getContents(), true));
      // $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }

  public function getMetafieldsCount ()
  {
    $url = self::API_URL . '/metafields/count.json';

    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('GET', $url)->body
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      // $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }

  public function createMetafield($data)
  {
    $url = self::API_URL . '/metafields.json';

    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('POST', $url, ['metafield' => $data])->body->metafield
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      // $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }

  public function updateMetafield($data, $id)
  {
    $url = self::API_URL . "/metafields/$id.json";
    $data['id'] = $id;
    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('PUT', $url, ['metafield' => $data])->body->metafield
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }

  public function deleteMetafield ($id)
  {
    $url = self::API_URL . "/metafields/$id.json";
    try {
      $this->response = [
        'success' => true,
        'data' => $this->api->rest('DELETE', $url)
      ];
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      // $this->response['errors'] = json_decode($e->getResponse()->getBody()->getContents())->errors;
    }

    return $this->response;
  }
}