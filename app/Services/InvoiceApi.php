<?php

namespace App\Services;

use GuzzleHttp\Client;

class InvoiceApi
{
    const API_URL = 'https://n8437v0722.execute-api.us-east-1.amazonaws.com/api/';
    const X_API_KEY = 'exm3e2ammz5rSVEYxXvr636FAFNydamqawaNBUnZ';

    private $client;
    private $response = [ 'success' => true, 'data' => null,];

    public function __construct()
    {
        $this->client = new Client([
            'x-api-key' => self::X_API_KEY,
            'content-Type' => 'application/json',
            'accept' => 'application/json',
        ]);
    }

    /**
     * Get invoice pdf url
     * 
     * @param integer $invoice_number
     * @param integer $company
     * @param string $user_id
     * 
     * @return mixed $response data
     */
    public function getInvoice($invoice_number, $company, $user_id)
    {
        // dd([
        //     "invoice_number" => $invoice_number,
        //     "company" => $company,
        //     "user_id" => $user_id,
        // ]);
        try {
            $request = $this->client->post(self::API_URL . 'get-invoice/',
                ['json' => [
                    "invoice_number" => $invoice_number,
                    "company" => $company,
                    "user_id" => $user_id,
                ]]);
            $this->response['data'] = json_decode($request->getBody()->getContents());
        } catch (\Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return (object) $this->response;
    }

    public function getInvoiceStatusById ($id)
    {
        try {
            $request = $this->client->post(self::API_URL . 'invoice-status-by-order-id/',
            ['json' => [
                'shopify_order_id' => (string) $id
            ]]);
            $this->response['data'] = json_decode($request->getBody()->getContents());
        } catch (\Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return (object) $this->response;
    }
}
