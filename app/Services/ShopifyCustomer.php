<?php

namespace App\Services;

// use PHPShopify\ShopifySDK;
// use App\Adapters\ShopifyApiAdapter;
use App\Helpers\States;
use Illuminate\Support\Arr;
use Mockery\CountValidator\Exception;

class ShopifyCustomer
{
    /// services
    protected $api;
    protected $customerApi;
    protected $customerManagement;
    protected $notificationService;

    protected $data_to_insert = [
        'metafields' => [],
        'success' => true
    ];

    protected $customersMeta = [];

    protected $update_calls_strategy = [
        'customer' => [
            'first_name', 'last_name', 'phone', 'email',
            'tags', 'state', 'accepts_marketing',
        ],
        'metafields' => [
            'company_id', 'language',
            'country', 'industry', 'phone_pin',
            'company', 'company_admin', 'account_type',
            'invoices', 'order_history',
            'purchasing', 'reviews', 'user_management', 'state',
        ],
    ];

    // private $company;

    protected $businessJoinType = 'B2B Join';
    protected $businessAdminType = 'B2B Admin';
    protected $businessUnidentifiedType = 'B2B Unidentified';
    protected $consumerType = 'B2C';

    protected $companyFirstTagPart = 'CompanyID:';

    protected $calls_strategy = [
        'customers' => [
            'first_name', 'last_name', 'phone', 'email',
            'password', 'password_confirmation', 'tags', 'accepts_marketing'
        ],
        'metafields' => [
            'company_id', 'language', 'country',
            'industry', 'phone_pin', 'company',
            'account_type', 'state',
            'invoices', 'order_history',
            'purchasing', 'reviews', 'user_management',
        ]
    ];

    protected $update_meta = [];

    protected $defaultPermissions = [
        'company_admin'   => 0,
        'purchasing'      => 1,
        'reviews'         => 1,
        'pricing'         => 'list',
        'order_history'   => 'self',
        'invoices'        => 'company',
        'user_management' => 0
    ];

    protected $defaultAdminPermissions = [
        'company_admin'   => 1,
        'purchasing'      => 1,
        'reviews'         => 1,
        'pricing'         => 'list',
        'order_history'   => 'self',
        'invoices'        => 'company',
        'user_management' => 1
    ];

    protected $permisionsKeys = [
        'company_admin', 'purchasing', 'reviews',
        'pricing', 'order_history', 'invoices',
        'user_management'
    ];

    const META_NAMESPACE = 'global';

    public function __construct($shop)
    {
        $this->customerApi         = new ShopifyCustomerApi($shop->api());
        $this->customerManagement  = new CustomerManagement();
        $this->api                 = $shop->api();
        $this->notificationService = new NotificationService($shop);
    }

    /** 
     * Create customer in Shopify and in app database
     * 
     * @param array $data customer Data
     * @return object error of success object
     */
    public function createCustomer($data)
    {
        $data['state'] = States::Enabled;

        $data['tags'] = $this->addStateToTags($data['tags'], $data['state']);

        if (isset($data['accepts_marketing']) && $data['accepts_marketing'])
            $data['tags'] = $this->addNewsletterToTags($data['tags']);

        $this->generateCreateRequestData($data);
        $customer = $this->customerApi->create($this->data_to_insert);

        if ($this->isErrorResponse($customer)) {
            $existCustomers = $this->customerApi->get(null, ['email' => $this->data_to_insert['email']]);

            $existCustomer = $this->sortCustomerByEmail($this->data_to_insert['email'], $existCustomers);

            if ($existCustomer && empty($existCustomer->last_name) && $existCustomer->state == States::Disabled) {
                // dd($this->data_to_insert);
                $updatedCustomer = $this->customerApi->update($this->data_to_insert, $existCustomer->id);

                if ($this->isErrorResponse($updatedCustomer)) {
                    $this->data_to_insert['success'] = false;
                    $this->data_to_insert['error'] = $updatedCustomer['error'];
                    return $this->data_to_insert;
                }
                $this->customerApi->sendInvite($existCustomer->id);

                $this->data_to_insert['customer'] = $updatedCustomer;
                $this->createLocalCustomer($data);
                $this->cretePermissions($data, $updatedCustomer, $data['account_type']);

                if ($data['account_type'] != $this->consumerType) {
                    $this->sendInvite($updatedCustomer->id);
                }

                $this->data_to_insert['updated'] = true;
                return $this->data_to_insert;
            }
            $this->data_to_insert['success'] = false;
            $this->data_to_insert['error'] = $customer['error'];
            return $this->data_to_insert;
        }

        $this->data_to_insert['customer'] = $customer;
        $this->createLocalCustomer($data);
        $this->cretePermissions($data, $customer, $data['account_type']);

        if ($data['account_type'] != $this->consumerType) {
            $this->sendInvite($customer->id);
        }

        return $this->data_to_insert;
    }

    public function adminCreateCustomer ($data) {
        $data['state'] = States::Invited;

        $data['tags'] = $this->addStateToTags($data['tags'], $data['state']);

        if (isset($data['accepts_marketing']) && $data['accepts_marketing'])
            $data['tags'] = $this->addNewsletterToTags($data['tags']);

        $company = $this->customerManagement->getCompany($data['company'], $data['shop_id']);
        $data['company_id'] = $company->id;

        $companyLessTags = $this->getCompanyLessTags($data['tags'], true);
        $companyLessTags[] = $this->companyFirstTagPart . $company->ifs_id;;
        $data['tags'] = implode(', ', $companyLessTags);

        $this->generateCreateRequestData($data);

        $customer = $this->customerApi->create($this->data_to_insert);

        if ($this->isErrorResponse($customer)) {
            $this->data_to_insert['success'] = false;
            $this->data_to_insert['error'] = $customer['error'];
            return $this->data_to_insert;
        }

        $this->data_to_insert['customer'] = $customer;
        $this->createLocalCustomer($data);
        $this->cretePermissions($data, $customer, $data['account_type']);

        $this->adminSendInvite($customer);

        $this->data_to_insert['appCustomer'] = $this->customerManagement
            ->getManagementCustomer($customer->id, $data['shop_id']);

        return $this->data_to_insert;
    }

    /**
     * Send invite to customer using Shopify API
     * 
     * @param int $customerId
     * @return mixed success or error object
     */
    public function sendInvite($customerId)
    {
        $customer = $this->customerApi->get($customerId);
        return $this->notificationService->sendInvite($customer);
    }

    public function adminResendInvite ($customerId) {
        $customer = $this->customerApi->get($customerId);
        try {
            $this->adminSendInvite($customer);
            return [
                'message' => "Invite was send successfuly",
                'success' => true,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'errors'   => ['Error happend please reload and try agin.'],
            ];
        }
    }

    public function adminSendInvite ($customer) {
        $activationURL = $this->customerApi->createActivationURL($customer->id);
        $this->notificationService->adminSendInvite($customer, $activationURL);
    }

    /**
     * Return customer meta array or specific metaObject by key
     * 
     * @param int $customerId
     * @param string $searchValue get metaObject by specific key
     * 
     * @return array|object 
     */
    public function getMeta($customerId, $searchValue = null)
    {
        if (!isset($this->customersMeta[$customerId]))
            $this->customersMeta[$customerId] = $this->customerApi->getMeta($customerId);
        return $searchValue ?
            $this->getMetafieldArr($this->customersMeta[$customerId], $searchValue) : $this->customersMeta[$customerId];
    }

    public function updateCustomer($data, $shopID)
    {
        $data['shop_id'] = $shopID;
        $customer = $this->customerApi->get($data['id']);
        $metafield = $this->getMeta($data['id']);

        $customerData = json_decode(json_encode($customer), true);
        $dataToInsert = [];

        if (isset($data['first_name']) && $data['first_name']) $dataToInsert['first_name'] = trim($data['first_name']);

        if (isset($data['last_name']) && $data['last_name']) $dataToInsert['last_name'] = trim($data['last_name']);

        if (isset($data['phone']) && $data['phone']) $dataToInsert['phone'] = trim($data['phone']);

        if (isset($data['email']) && $data['email']) $dataToInsert['email'] = trim($data['email']);

        if (isset($data['language']) && $data['language']) {
            $languageArr = $this->getMetafieldArr($metafield, 'language');
            $languageMetaKey = 'language';
            if ($languageArr) {
                $this->updateMetafield($languageArr, $data['language']);
            } else {
                $dataToInsert['metafields'][] = $this->buildMetaObj($languageMetaKey, $data['language']);
            }
        }

        if (isset($data['country']) && $data['country']) {
            $countryArr = $this->getMetafieldArr($metafield, 'country');
            $countryMetaKey = 'country';
            if ($countryArr) {
                $this->updateMetafield($countryArr, $data['country']);
            } else {
                $dataToInsert['metafields'][] = $this->buildMetaObj($countryMetaKey, $data['country']);
            }
        }

        if (isset($data['industry']) && $data['industry']) {
            $industryArr = $this->getMetafieldArr($metafield, 'industry');
            $industryMetaKey = 'industry';
            if ($industryArr) {
                $this->updateMetafield($industryArr, $data['industry']);
            } else {
                $dataToInsert['metafields'][] = $this->buildMetaObj($industryMetaKey, $data['industry']);
            }
        }

        if (isset($data['company']) && $data['company']) {
            $companyArr = $this->getMetafieldArr($metafield, 'company');
            $companyMetaKey = 'company';
            if ($companyArr) {
                $this->updateMetafield($companyArr, $data['company']);
            } else {
                $dataToInsert['metafields'][] = $this->buildMetaObj($companyMetaKey, $data['company']);
            }
        }

        if (isset($data['account_type'])) {
            $typeLessTagsAtrr = $this->getTypelessTags($customerData['tags'], true);
            $typeLessTagsAtrr[] = $data['account_type'];
            $dataToInsert['tags'] = $data['tags'] = implode(', ', $typeLessTagsAtrr);
        }

        if (isset($data['newsletter']) && $data['newsletter']) {
            $tags = isset($data['tags']) ? $data['tags'] : $customerData['tags'];
            $dataToInsert['tags'] = $data['tags'] = $this->addNewsletterToTags($tags);
            $dataToInsert['accepts_marketing'] = true;
        } else {
            $tags = isset($data['tags']) ? $data['tags'] : $customerData['tags'];
            $dataToInsert['tags'] = $data['tags'] = $this->removeNewsletterFromTags($tags);
            $dataToInsert['accepts_marketing'] = $customerData['accepts_marketing'];
        }

        if (isset($data['tags'])) {
            $dataToInsert['tags'] = $data['tags'];
        }

        $customer = $this->customerApi->update($dataToInsert, $data['id']);

        if ($this->isErrorResponse($customer)) {
            $data['success'] = false;
            $data['error'] = $customer['error'];
        } else {
            $this->customerManagement->updateCustomer($data, $shopID);
            $data['success'] = true;
            $data['customer'] = $customer;
            $data['message'] = 'Customer successfully updated.';
        }
        return $data;
    }

    /**
     * Check if user can be added to company, or if user aleready in some company
     * 
     * @param array $data['query', 'email', 'company', 'fields]
     * @param int $shopID
     * 
     * @return array 
     */
    public function checkUser($data, $shopID)
    {
        $customers = $this->customerApi->search($data);
        if (isset($customers['success']) && $customers['success'] == false) {
            return $customers;
        }

        $response = [
            'success' => true,
            'customers' => $customers,
            'email' => $data['email'],
        ];

        if (count($customers) > 0) {
            $current_company = $this->customerManagement->getCompany($data['company'], $shopID);
            $appCustomer = $this->customerManagement->getCustomerByEmail($data['email'],  $shopID);

            // if ($appCustomer->company_owner) {
            //     $response['join'] = false;
            //     $response['join_message'] = 'That user is already associated with another company account.';
            // } else
            if ($appCustomer->company_id == $current_company->id) {
                $response['join'] = false;
                $response['join_message'] = 'That email address is already associated with another user on your company account.';
            } else if ($appCustomer->company_id) {
                $response['join'] = false;
                $response['join_message'] = 'That user is already associated with another company account.';
            } else {
                $response['join'] = true;
                $response['join_message'] = 'A user with ' . $data['email'] . ' already exists. <br>
                Would you like the add them to your account?';
                $response['user_id'] = $appCustomer->id;
            }
        }
        return $response;
    }

    public function joinExistingUser($data, $shopID)
    {
        $customer = $this->customerApi->get($data['newCustomerId']);
        $customerMeta = $this->getMeta($customer->id, 'language');
        $company = $this->customerManagement->getCompany($data['companyName'], $shopID);
        $activation_token = $this->customerManagement->createActivationToken($customer->id, $company);
        $mailResponse = $this->notificationService->sendJoinExistingCustomer($customer, $company, $activation_token, $customerMeta['value']);

        // Handle errror
        if (!$mailResponse['success']) return $mailResponse;

        return [
            'success' => true,
            'message' => 'An email has been sent to this user to verify their account.'
        ];
    }

    public function joinUserActivation($token, $shopID)
    {
        $decodedToken = base64_decode($token);
        parse_str($decodedToken, $parsedToken);
        $customerMeta = $this->getMeta($customer->id, 'language');

        if (!isset($parsedToken['join_company_name']) || !isset($parsedToken['user_id'])) {
            return [
                'success' => false,
                'message' => 'This is not valid link.',
            ];
        }
        // get customer from app database
        $appCustomer = $this->customerManagement->getCustomerWhere([
            ['id', '=', $parsedToken['user_id']],
            ['activation_token', '=', $token],
            ['shop_id', '=', $shopID],
        ]);
        if (!$appCustomer) {
            return [
                'success' => false,
                'message' => 'This is not valid link anymore.'
            ];
        }
        // get company  from app database
        $company = $this->customerManagement->getCompany($parsedToken['join_company_name'], $shopID);
        $companyAdmins = $this->customerManagement->getCustomersWhere([
            ['company_id', '=', $company->id],
            ['account_type', '=', $this->businessAdminType],
            ['shop_id', '=', $shopID],
        ]);
        // update customer in shopify,  and in app
        $updatedCustomer = $customerUpdate = $this->updateCustomer([
            'id'           => $appCustomer->id,
            'company_id'   => $company->id,
            'company'      => $company->name,
            'account_type' => $this->businessJoinType,
            'industry'     => $companyAdmins[0]->industry,
            'country'      => $companyAdmins[0]->country,
            'activation_token' => null,
        ], $shopID);
        if (!$updatedCustomer['success']) {
            return [
                'success' => false,
                'mesaage' => 'Error happend please try again',
                'data' => $updatedCustomer,
            ];
        }
        $this->notificationService->sendUserJoinedMailToSuperAdmin($appCustomer, $company);
        $companyAdmins->each(function ($companyAdmin) use ($appCustomer, $company) {
            $this->notificationService->sendUserJoinedMailToAdmins($appCustomer, $companyAdmin, $company, $customerMeta['value']);
        });
        // create default permisssion for user
        $this->createDefaultPermissions($customerUpdate['customer'], $this->defaultPermissions);
        return [
            'success' => true,
            'message' => 'Thanks! You\'ve been connected to your company account.',
        ];
    }

    public function approve_deny($data, $shopID)
    {
        $customer  = $this->customerApi->get($data['id']);
        // get company  from app database
        $company = $this->customerManagement->getCompany($data['company'], $shopID);
        $newTags = $this->getStatelessTags($customer->tags) . ', ' . $data['state'];
        $data['tags'] = $newTags;
        try {
            $customer = $this->customerApi->update(
                [
                    'tags' => $newTags
                ],
                $data['id']
            );
            $this->customerManagement->updateCustomer($data, $shopID);
            if ($data['state'] == 'disabled') {
                $this->notificationService->rejectMail($customer, $company, $data['reason']);
            }
            if ($data['state'] == 'enabled') {
                $this->notificationService->approveMail($customer, $company);
            }
            return [
                'message' => 'Account was successfully ' . $data['state'],
                'success' => true,
                'id' => $data['id'],
                'state' => $data['state'],
                'tags' => $data['tags'],
            ];
        } catch (\Exception $e) {
            return [
                'error'   => $e->getMessage(),
                'success' => false,
            ];
        }
    }

    public function enable_disable($data, $shopID)
    {
        $customer  = $this->customerApi->get($data['id']);
        $metafields = $this->customerApi->getMeta($data['id']);
        $newTags = $this->getStatelessTags($customer->tags) . ', ' . $data['state'];
        $data['tags'] = $newTags;

        foreach ($metafields as $metaArr) {
            if ($metaArr['namespace'] == 'global' && $metaArr['key'] == 'state') {
                $this->updateMetafield($metaArr, $data['state']);
            }
        }

        try {
            $customer = $this->customerApi->update(
                [
                    'tags' => $newTags
                ],
                $data['id']
            );
            $this->customerManagement->updateCustomer($data, $shopID);
            return [
                'message' => 'Account was successfully ' . $data['state'],
                'success' => true,
                'id' => $data['id'],
                'state' => $data['state'],
                'tags' => $data['tags'],
            ];
        } catch (\Exception $e) {
            return [
                'error'   => $e->getMessage(),
                'success' => false,
            ];
        }
    }

    public function changePermissions($data)
    {
        $customerId = $data['customer_id'];
        // delete customer Id
        unset($data['customer_id']);

        $metafields = $this->customerApi->getMeta($customerId);
        $dataToInsert = [];
        foreach ($data as $permKey => $permValue) {
            $metaArr = $this->getMetafieldArr($metafields, $permKey);
            if ($metaArr) {
                $this->updateMetafield($metaArr, $permValue);
            } else {
                $dataToInsert['metafields'][] = $this->buildMetaObj($permKey, $permValue);
            }
        }

        if (!empty($dataToInsert)) {
            $customer = $this->customerApi->update($dataToInsert, $customerId);
            if ($this->isErrorResponse($customer)) {
                $data['success'] = false;
                $data['error'] = $customer['error'];
            } else {
                $this->customerManagement->updatePermisions($data, $customerId);
                $data['success'] = true;
                $data['customer'] = $customer;
                $data['message'] = 'Permissions succesfully changed.';
            }
        } else {
            $this->customerManagement->updatePermisions($data, $customerId);
            $data['message'] = 'Permissions succesfully changed.';
            $data['success'] = true;
        }
        $data['customer_id'] = $customerId;
        return $data;
    }

    public static function getStatelessTags($tags, $returnArr = false)
    {
        $tagsArr = explode(', ', $tags);
        $statlessTags = array_filter($tagsArr, function ($tag) {
            return !in_array($tag, [
                States::Enabled,
                States::Invited,
                States::Disabled,
                States::Declined,
            ]);
        });

        if ($returnArr) return $statlessTags;
        return implode(', ', $statlessTags);
    }

    public function getTypelessTags($tags, $returnArr = false)
    {
        $tagsArr = explode(', ', $tags);
        $typelessTags = array_filter($tagsArr, function ($tag) {
            return !in_array($tag, [
                $this->businessJoinType,
                $this->businessUnidentifiedType,
                $this->businessAdminType,
                $this->consumerType,
            ]);
        });

        if ($returnArr) return $typelessTags;
        return implode(', ', $typelessTags);
    }

    protected function addStateToTags($tags, $state) {
        $statelessTags = $this->getStatelessTags($tags, true);
        $statelessTags[] = $state;
        return implode(', ', $statelessTags);
    }

    protected function addNewsletterToTags($tags) {
        $tagsArr = explode(', ', $tags);
        $tagsArr[] = 'newsletter';
        return implode(', ', $tagsArr);
    }

    protected function removeNewsletterFromTags($tags) {
        $tagsArr = explode(', ', $tags);
        $tagsArr = array_filter($tagsArr, function ($tag) {
            return !in_array($tag, ['newsletter']);
        });
        return implode(', ', $tagsArr);
    }

    protected function getCompanyLessTags($tags, $returnArr = false)
    {
        $tagsArr = explode(', ', $tags);
        $companyLessTags = array_filter($tagsArr, function ($tag) {
            return stristr($tag, $this->companyFirstTagPart) === false;
        });

        if ($returnArr) return $companyLessTags;
        return implode(', ', $companyLessTags);
    }

    protected function generateCreateRequestData($data)
    {
        $this->generateRequestDataForCustomer($this->calls_strategy['customers'], 0, $data);
        $this->data_to_insert['send_email_welcome'] = $data['account_type'] == $this->businessJoinType ? false : true;

        $this->generateCreateRequestDataForMetafields($this->calls_strategy['metafields'], 0, $data);
    }

    protected function generateRequestDataForCustomer($keys, $next, $data)
    {
        try {
            if ($this->isValid($keys[$next], $data))
                $this->data_to_insert[$keys[$next]] = $data[$keys[$next]];

            if (isset($keys[$next + 1]))
                $this->generateRequestDataForCustomer($keys, $next + 1, $data);
        } catch (Exception $e) { }
    }

    protected function generateUpdateRequestDataForMetafields($keys, $next, $data, $customerMeta)
    {
        try {
            if (in_array($keys[$next], $keys)) {
                $metaObj = $this->getMetafieldArr($customerMeta, $keys[$next]);
                if ($metaObj) {
                    $this->update_meta[$keys[$next]] = $metaObj;
                } else if (isset($data[$keys[$next]])) {
                    $this->data_to_insert['metafields'][] = $this->buildMetaObj(
                        $keys[$next],
                        $data[$keys[$next]]
                    );
                }
            }

            if (isset($keys[$next + 1]))
                $this->generateUpdateRequestDataForMetafields($keys, $next + 1, $data, $customerMeta);
        } catch (Exception $e) { }
    }

    protected function debug($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    protected function generateCreateRequestDataForMetafields($keys, $next, $data)
    {
        try {
            if ($this->isValid($keys[$next], $data))
                $this->data_to_insert['metafields'][] = $this->buildMetaObj($keys[$next], $data[$keys[$next]]);

            if (isset($keys[$next + 1]))
                $this->generateCreateRequestDataForMetafields($keys, $next + 1, $data);
        } catch (Exception $e) { }
    }

    protected function isValid($key, $data)
    {
        return isset($data[$key]) && $data[$key];
    }

    protected function isErrorResponse($customer)
    {
        return is_array($customer) && $customer['success'] == false;
    }

    protected function getMetafieldArr($metafieldArr, $key)
    {
        foreach ($metafieldArr as $metaArr) {
            if ($metaArr['key'] == $key) return $metaArr;
        }
        return null;
    }

    protected function buildMetaObj($key, $value)
    {
        $metafield = (object)[
            'key' => $key,
            'namespace' => self::META_NAMESPACE,
            'value' => $value,
            'value_type' => 'string',
        ];
        return $metafield;
    }

    public function updateMetafield($metaArr, $newValue)
    {
        $this->api->rest('PUT', '/admin/metafields/' . $metaArr['id'] . '.json', [
            'metafield' => [
                'value' => $newValue
            ]
        ]);
    }

    protected function createLocalCustomer($data)
    {
        // check if customer created in Shopify
        if (!$this->data_to_insert['success']) return;

        if ($data['account_type'] == $this->businessUnidentifiedType) {
            $this->notificationService->adminCreateNewCompany($data);
        }

        if ($data['account_type'] == $this->businessJoinType) {
            $this->notificationService->adminNewUserJoinCompany($data);
        }

        // create customer in app
        $this->customerManagement->handleCustomer($data, $this->data_to_insert['customer']);
    }


    /**
     * Create customer permissions in shopify app
     * 
     * @param integer $customerId
     * @param array|null permissions
     * 
     * @return void
     */
    protected function createShopiyCustomerDefaultPermissions($customerId, $permissions = null)
    {
        $dataToInsert = ['metafields' => []];
        foreach ($permissions as $permKey => $permValue) {
            $dataToInsert['metafields'][] = $this->buildMetaObj($permKey, $permValue);
        }
        $this->customerApi->update($dataToInsert, $customerId);
    }

    /**
     * Create customer permissions in shopify and our local app
     * 
     * @param object $customer shopify customer
     * @param array $permissions customer permissions
     * 
     * @return void
     */
    protected function createDefaultPermissions($customer, $permissions)
    {
        $this->createShopiyCustomerDefaultPermissions($customer->id, $permissions);
        $this->customerManagement
            ->createDefaultPermissions($customer, $permissions);
    }

    protected function cretePermissions($data, $customer, $account_type)
    {
        // if consumer type no need to add permissions
        if (!in_array($account_type, 
            [$this->businessJoinType, $this->businessUnidentifiedType, $this->businessAdminType])
        ) {
            return;
        }
        // filter data
        $permissions = Arr::only((array)$data, $this->permisionsKeys);
        // setup default permissions if we do not have one in request
        $permissions = count($permissions) >= 6 ? $permissions : $this->defaultPermissions;

        // if admin user change permissions to admin
        if (in_array($account_type, [$this->businessUnidentifiedType, $this->businessAdminType]))
            $permissions = $this->defaultAdminPermissions;

        // create customer permission
        $this->createDefaultPermissions( $customer, $permissions);
    }

    protected function sortCustomerByEmail($email, $customers) {
        foreach($customers as $customer) {
            if (isset($customer->email) && $customer->email == $email) {
                return $customer;
            }
        }
        return null;
    }
}
