<?php

namespace App\Services;

use GuzzleHttp\Client;

class ShopifyWebEx {
  // Api URL
  protected $api = 'https://peerless-av.webex.com/WBXService/XMLService';

  // siteName used for Xml request
  protected $siteName;
  // WebExID used for xml request
  protected $WebExID;
  // password used for xml request
  protected $password;
  protected $host;
  protected $data;
  protected $notificationService;

  public $beetweenAccountAttendeeDates;

	const XML_VERSION         = '1.0';
	const XML_ENCODING        = 'UTF-8';
	const SUFIX_XML_API       = 'WBXService/XMLService';
	const WEBEX_DOMAIN        = 'webex.com';
	const PREFIX_HTTPS        = 'https';
  const API_SCHEMA_SERVICE  = 'http://www.webex.com/schemas/2002/06/service';
  const API_SCHEMA_EVENT    = 'http://www.webex.com/schemas/2002/06/service/event';
  const API_SCHEMA_MEETING  = 'http://www.webex.com/schemas/2002/06/service/meeting';
  const API_SCHEMA_ATTENDEE = 'http://www.webex.com/schemas/2002/06/service/attendee';
	const DATA_RESPONSE       = 'response';
	const DATA_RESPONSE_XML   = 'xml';
	const DATA_RESPONSE_DATA  = 'data';

  protected $client = null;

  public function __construct ($host, $siteName, $WebExID, $password, $shop = null) 
  {
    $this->host     = $host;
    $this->siteName = $siteName;
    $this->WebExID  = $WebExID;
    $this->password = $password;
    $this->client = new Client([
      'base_uri' => 'https://'.$this->host. '.' . self::WEBEX_DOMAIN,
    ]);

    // Dates between which we create account and send invite 
    // 'less_then' and 'bigger_then' for cron
    // 'less_then' and 'today' for email
    $this->beetweenAccountAttendeeDates = (object)[
      'less_then'   => strtotime('+1 day', strtotime('tomorrow')), // to get beginig of day
      'bigger_then' => strtotime('tomorrow'),
      'today'       => strtotime('today'),
    ];

    if ($shop)
      $this->notificationService = new NotificationService($shop);
  }

  public function getEvents ()
  {
    $requestData = [
      'service' => 'event.LstsummaryEvent',
      'xml_body' => $this->get_events_xml_body(),
    ];
    $this->build_events_response(
      $this->sendRequest("POST", $this->get_xml($requestData)));
    return $this->data;
  }

  public function createMeetingAttendee($id, $attendeeEmail)
  {
    $requestData = [
      'service' => 'attendee.CreateMeetingAttendee',
      'xml_body' => '<person><name>alterhost</name><address><addressType>PERSONAL</addressType></address><email>'.$attendeeEmail.'</email><type>VISITOR</type></person><role>ATTENDEE</role><sessionKey>'.$id.'</sessionKey>'
    ];
    $this->parseResponse(
      $this->sendRequest("POST", $this->get_xml($requestData)), 
      self::API_SCHEMA_ATTENDEE
    );
    return $this->data;
  }

  public function processMeetingAttendeeInvite($id, $attendeeEmail)
  {
    $event = $this->getEvent($id);
    $eventStartDate = strtotime((string) $event['data']->bodyContent['schedule']->startDate);
    if ($eventStartDate > $this->beetweenAccountAttendeeDates->today 
      && $eventStartDate < $this->beetweenAccountAttendeeDates->less_then) {
      $eventName = (string) $event['data']->bodyContent['metaData']->sessionName;
      $mail = $this->notificationService->sendRequestToJoinWebinar(
        $attendeeEmail, $eventName);
      return [
        'data' => [
          'header' => [
            'response' => [
              'result' => $mail['success'] ? 'SUCCESS' : 'FAILURE',
              'reason' => $mail['success'] ? 'Account successfully created.' 
                : 'Oops. Something bad happened, please try again.'
            ]
          ]
        ]
      ];
    }
    return $this->createMeetingAttendee($id, $attendeeEmail);
  }

  public function getEvent($id)
  {
    $requestData = [
      'service' => 'event.GetEvent',
      'xml_body' => "<sessionKey>$id</sessionKey>",
    ];
    $this->parseResponse(
      $this->sendRequest("POST", $this->get_xml($requestData)), 
      self::API_SCHEMA_EVENT
    );
    return $this->data;
  }

  public function getJoinUrlMeeting($id, $attendeeName)
  {
    $requestData = [
      'service' => 'meeting.GetjoinurlMeeting',
      'xml_body' => " <sessionKey>$id</sessionKey><attendeeName>$attendeeName</attendeeName>",
    ];
    $this->parseResponse(
      $this->sendRequest("POST", $this->get_xml($requestData)), 
      self::API_SCHEMA_MEETING);
    return $this->data;
  }

  protected function sendRequest($method, $body)
  {
    return $this->client->request($method, self::SUFIX_XML_API,
      [
        'body' => $body,
        'headers' => ['Content-Type' => 'text/xml']
      ]
    );
  }

  protected function parseResponse($response, $schema)
  {
    $body    = $response->getBody();
    $xml = \simplexml_load_string((string) $body);

    $Data                   = new \stdClass;
    $Data->header           = new \stdClass;
    $Data->header->response = new \stdClass;
    $Data->bodyContent      = array();

    $node                               = $xml->children(self::API_SCHEMA_SERVICE);
    $Data->header->response->result     = (string) $node[0]->response->result;
    $Data->header->response->gsbStatus  = (string) $node[0]->response->gsbStatus;
    $Data->header->response->reason     = $node[0]->response->reason ? 
      (string) $node[0]->response->reason : 'Account successfully created.';

    $node_xml = $node[1]->bodyContent;
    $Data->bodyContent = (array) $node_xml->children($schema);
    $this->data[self::DATA_RESPONSE_DATA] = $Data;
  }

  private function get_events_xml_body ()
  {
    $xml_body = array();
    $xml_body[] = '<listControl>';
    $xml_body[] =   '<startFrom>1</startFrom>';
    $xml_body[] =   '<maximumNum>500</maximumNum>';
    $xml_body[] =   '<listMethod>OR</listMethod>';
    $xml_body[] = '</listControl>';
    $xml_body[] = '<order>';
    $xml_body[] =   '<orderBy>STARTTIME</orderBy>';
    $xml_body[] =   '<orderAD>ASC</orderAD>';
    $xml_body[] =   '<orderBy>HOSTWEBEXID</orderBy>';
    $xml_body[] =   '<orderAD>ASC</orderAD>';
    $xml_body[] =   '<orderBy>EVENTNAME</orderBy>';
    $xml_body[] =   '<orderAD>ASC</orderAD>';
    $xml_body[] = '</order>';

    return implode('', $xml_body);
  }

  private function xml2array ( $xmlObject, $out = array () )
  {
    foreach ( (array) $xmlObject as $index => $node )
      $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;

    return $out;
  }

  /**
	 * Parse response to array.
	 *
	 * @param object $response \Illuminate\Support\Facades\Response instance.
	 * @return void
	 */
  private function build_events_response ($response)
  {
    $body                   = $response->getBody();
    $events                 = simplexml_load_string((string) $body);
    $Data                   = new \stdClass;
    $Data->header           = new \stdClass;
    $Data->header->response = new \stdClass;
    $Data->bodyContent      = array();

    $node                               = $events->children(self::API_SCHEMA_SERVICE);
    $Data->header->response->result     = (string) $node[0]->response->result;
    $Data->header->response->gsbStatus  = (string) $node[0]->response->gsbStatus;

    $node_meeting = $node[1]->bodyContent;
    foreach($node_meeting->children(self::API_SCHEMA_EVENT)->event as $event) {
      if($event->sessionKey) {
        $EventData              = new \stdClass;
        $EventData->sessionKey  = (string) $event->sessionKey;
        $EventData->sessionName = (string) $event->sessionName;
        $EventData->sessionType = (string) $event->sessionType;
        $EventData->hostWebExID = (string) $event->hostWebExID;
        $EventData->startDate   = (string) $event->startDate;
        $EventData->endDate     = (string) $event->endDate;
        $EventData->timeZoneID  = (string) $event->timeZoneID;
        $EventData->duration    = (string) $event->duration;
        $EventData->description = (string) $event->description; //$this->str_replace_first('Peerless-AV’s', 'Peerless-AV<sup>®</sup>’s', (string) $event->description);
        $EventData->listStatus  = (string) $event->listStatus;
        $EventData->panelists   = (string) $event->panelists;
        $EventData->status      = (string) $event->status;
        $Data->bodyContent[]    = $EventData;
      }
    }

    $this->data[self::DATA_RESPONSE_DATA] 	= $Data;
  }

  /**
   * Generates a XML to send a data to API.
   *
   * @param array $data Data to insert in XML in format:
   *		$data['service']	= 'meeting';
    *		$data['xml_header'] = '<item><subitem>data</subitem></item>';
    *		$data['xml_body']	= '<item><subitem>data</subitem></item>';
    * @return string Returns a XML generated.
    */
  protected function get_xml($data)
  {
    return '<?xml version="' . self::XML_VERSION . '" encoding="' . self::XML_ENCODING . '"?>
      <serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <header>
          <securityContext>
            <siteName>' . $this->siteName . '</siteName>
            <webExID>' . $this->WebExID . '</webExID>
            <password>' . $this->password . '</password>
          </securityContext>
        </header>
        <body>
          <bodyContent xsi:type="java:com.webex.service.binding.' . $data['service'] . '">
            '.$data['xml_body'].'
          </bodyContent>
        </body>
      </serv:message>';
  }
  
  private function str_replace_first ($from, $to, $content) {
    $from = '/'.preg_quote($from, '/').'/';
    return preg_replace($from, $to, $content, 1);
  }
}