<?php

namespace App\Services;

use App\Helpers\States;

class ShopifyCustomerApi {
    private $api;
    private const CUSTOMERS_URL = '/admin/customers';

    function __construct($api)
    {
        $this->api = $api;
    }

    // public function __call($action, $args) {
    //     try {
    //         return $this->{$action.'Customer'}($args);
    //     } catch(Exception $e) {}
    // }

    public function get($id=null, $params = [])
    {
        try {
            $url = self::CUSTOMERS_URL. ($id == null ? '' : '/'.$id). '.json';
            return $this->api->rest('GET', $url, $params)->body->{$id == null ? 'customers' : 'customer'};
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    /**
     * Return user meta data;
     *
     * @param int $id
     * @return object user meta data
     */
    public function getMeta($id)
    {
        $url = self::CUSTOMERS_URL. '/' . $id . '/metafields.json';
        return json_decode(json_encode($this->api->rest('GET', $url)->body->metafields), true);
    }

    public function create($data)
    {
        try {
            $url = self::CUSTOMERS_URL.'.json';
            $resp = $this->api->rest('POST', $url, ['customer' => $data])->body;
            if (!isset($resp->customer)) throw new \Exception((json_encode($resp)));
            return $resp->customer;
        } catch(\Exception $e) {
            return [
                'error'   => json_decode($e->getMessage()),
                'success' => false,
            ];
        }
    }

    public function update($data, $id)
    {
        $url = self::CUSTOMERS_URL.'/'.$id.'.json';
        try {
            $resp = $this->api->rest('PUT', $url, ['customer' => $data])->body;
            if (!isset($resp->customer)) throw new \Exception(json_encode($resp));
            return $resp->customer;
        } catch(\Exception $e) {
            return [
                'error'   => json_decode($e->getMessage()),
                'success' => false,
            ];
        }
    }

    public function search ($data)
    {
        try {
            $url = self::CUSTOMERS_URL. '/search' . '.json';
            return $this->api->rest('GET', $url, $data)->body->customers;
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    public function sendInvite ($id, $customer_invite = null)
    {
        try {
            $url = self::CUSTOMERS_URL.'/'.$id.'/send_invite.json';
            $customer_invite = $customer_invite ? (object)$customer_invite : (object)[];

            return $this->api->rest('POST', $url,['customer_invite' => $customer_invite]);
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    public function createActivationURL ($id)
    {
        try {
            $url = self::CUSTOMERS_URL.'/'.$id.'/account_activation_url.json';
            return $this->api->rest('POST', $url)->body->account_activation_url;
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    public function delete ($id)
    {
        try {
            $url = self::CUSTOMERS_URL.'/'.$id.'.json';
            return $this->api->rest('DELETE', $url);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }
}