<?php

namespace App\Services;

use App\Models\Product;

class ShopifyProduct
{
  protected $specMeta = [
    'product_spec' => [],
    'package_spec' => [],
  ];

  public $productMeta = [];

  protected $metaData;

  const SPECIFICATION_META_NAMESPACE = 'specifications';

  protected $dataToInsert = [];

  public function __construct($api)
  {
    $this->shopifyProductApi = new ShopifyProductApi($api);
  }

  public function getAdditianalProductData(&$order)
  {
    foreach ($order->line_items as $productItem) {
      // product id can be empty
      if (empty($productItem->product_id)) continue;
      $resp = $this->shopifyProductApi->get($productItem->product_id, [
        'fields' => 'handle,image'
      ]);
      $product = $resp->body->product;
      if (isset($product->handle)) $productItem->handle = $product->handle;
      if (isset($product->image)) $productItem->image = $product->image;
    }
  }

  /**
   * Rename product keys with one
   */
  public function renameOrderProductKeys(&$order, $renameArr)
  {
    foreach ($order->line_items as $productItem) {
      foreach ($renameArr as $renameOldKey => $renameNewKey) {
        if (property_exists($productItem, $renameOldKey)) {
          // replace old key with new one
          $productItem->{$renameNewKey} = $productItem->{$renameOldKey};
          // remove old key
          unset($productItem->{$renameOldKey});
        }
      }
    }
  }

  public function getMeta($id)
  {
    $this->productMeta = $this->shopifyProductApi->getMeta($id);
    return $this->productMeta;
  }

  public function productUpdate($product, $specCats, $shop)
  {
    $this->productMeta = $this->getMeta($product['id']);
    $this->updateAppProduct($product, $specCats, $shop);
    $this->prepareMetaData();
    $this->updateOrCreateMeta();

    if (!empty($this->dataToInsert))
      $this->shopifyProductApi->update($this->dataToInsert, $product['id']);
  }

  private function updateAppProduct($product, $specCats, $shop)
  {
    foreach ($specCats as $sCat) {
      foreach ($sCat['specification'] as $sItem) {
        $value = isset($sItem['value']) ? $sItem['value'] : $sItem['default_value'];
        \Log::info(print_r($sItem, true));
        if (!is_null($value)) {
          $productRecord = Product::updateOrCreate([
              'product_id' => $product['id'], 'specification_id' => $sItem['id'], 'shop_id' => $shop->id,],
            ['value' =>  $value, 'handle' => $product['handle'],]
          );
          $productRecord->save();
        } else {
          Product::where('product_id', $product['id'])
              ->where('specification_id', $sItem['id'])
              ->where('shop_id', $shop->id)
              ->delete();
        }

        $this->fillMetaData($value, $sCat['cat_name'], $sItem['name']);
      }
    }
  }

  public function fillMetaData($value, $catName, $specificationName)
  {
    $metaKey = str_slug($catName, '_');
    switch ($catName) {
      case 'Product Specifications':
        $metaKey = 'product_spec';
        break;
      case 'Package Specifications':
        $metaKey = 'package_spec';
        break;
      case 'Spécifications du produit': // Product Specifications fr
        $specKey = 'product_spec_fr';
        break;
      case 'Spécifications du colis': // Package Specifications fr
        $specKey = 'package_spec_fr';
        break;
      case 'Produktspezifikationen': // Product Specifications de
        $specKey = 'product_spec_de';
        break;
      case 'Paket-Spezifikationen': // Package Specifications de
        $specKey = 'package_spec_de';
        break;
      case 'Especificaciones del producto': // Product Specifications es
        $specKey = 'product_spec_es';
        break;
      case 'Especificaciones del paquete': // Package Specifications es
        $specKey = 'package_spec_es';
        break;
    }

    if (!is_null($value)) {
      $this->specMeta[$metaKey][] = $value ? $specificationName . '; ' . $value : '';
    } else {
      if (!isset($this->specMeta[$metaKey])) $this->specMeta[$metaKey] = [];
    }
  }

  public function prepareMetaData()
  {
    foreach ($this->specMeta as $specMetaKey => $specMeta) {
      if (!empty($specMeta))
        $this->specMeta[$specMetaKey] = implode(' | ', $specMeta);
      else
        $this->specMeta[$specMetaKey] = '';
    }
  }

  private function updateOrCreateMeta()
  {
    foreach ($this->specMeta as $specMetaKey => $specMetaValue) {
      $metaArr = $this->getMetafieldArr($specMetaKey);
      if ($metaArr && $specMetaValue) {
        $this->shopifyProductApi->updateMetafield($metaArr, $specMetaValue);
      } else if ($metaArr && !$specMetaValue) {
        $this->shopifyProductApi->deleteMetafield($metaArr);
      } else {
        $this->dataToInsert['metafields'][] = $this->buildMetaObj($specMetaKey, $specMetaValue);
      }
    }
  }

  public function getMetafieldArr($key)
  {
    foreach ($this->productMeta as $metaArr) {
      if ($metaArr['namespace'] == self::SPECIFICATION_META_NAMESPACE && $metaArr['key'] == $key) 
        return $metaArr;
    }
    return null;
  }


  private function buildMetaObj($key, $value)
  {
    $metafield = (object)[
      'key' => $key,
      'namespace' => self::SPECIFICATION_META_NAMESPACE,
      'value' => $value,
      'value_type' => 'string',
    ];
    return $metafield;
  }
}