<?php
namespace App\Services;

use App\Interfaces\AdaptorsInterface;
use App\Models\MountAdapters;

class AdaptorsService implements AdaptorsInterface {

    const MAX_ITEMS = 5000;

    public function createFromCsvArr($items)
    {
        \DB::table('mount_adapters')->truncate();
        for($index = 0; $index < count($items); $index+=$this->step($items))
            MountAdapters::insert(array_slice($items, $index, $this->step($items)));
    }

    private function step($items)
    {
        if($this->last_index($items) < self::MAX_ITEMS) {
            return $this->last_index($items);
        }
        else {
            return self::MAX_ITEMS;
        }
    }

    private function last_index($items)
    {
        return (count($items)-1);
    }
}
