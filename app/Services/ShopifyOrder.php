<?php

namespace App\Services;

use App\Models\OrderHistory;
use Carbon\Carbon;
use App\Helpers\OrderIFS_Status;

class ShopifyOrder
{
  private $customersId = null;

  private $IFSNamespace = 'IFS_data';

  public function __construct($shop)
  {
    $this->shopifyOrderApi = new ShopifyOrderApi($shop->api());
    $this->customerManagement = new CustomerManagement();
    $this->notificationService = new NotificationService($shop);
    $this->invoiceApi = new InvoiceApi();
  }

  public function get($id = null, $get = [])
  {
    return $this->shopifyOrderApi->get($id, $get);
  }

  /**
   * Return order depending on user perrmission
   *
   * @param array $requestData['userId', 'OrderHistoryPermission', 'orderId']
   * @return array $orders
   */
  public function getOrder($requestData)
  {
    extract($requestData);
    $take = ['order', 'meta', 'status'];
    $companyCustomersId = $this->getCompanyCustomersId($OrderHistoryPermission, $userId);
    $where = ['id'=> $orderId];
    return $this->getDBOrders($companyCustomersId, $take, $where)->map(function ($order) {
      $output = \json_decode($order->order);
      $output->meta = \json_decode($order->meta);
      $output->status = $order->status;
      return $output;
    });
  }

  /**
   * Return list of orders depending on user perrmission
   *
   * @param array $requestData['userId', 'OrderHistoryPermission']
   * @return array $orders
   */
  public function getOrders(array $requestData)
  {
    extract($requestData);
    $take = ['order', 'meta', 'status'];
    $companyCustomersId = $this->getCompanyCustomersId($OrderHistoryPermission, $userId);
    $where = $this->prepareWhereStatement($requestData, null);
    return $this->getDBOrders($companyCustomersId, $take, $where, null, $perPage, $offset)->map(function ($order) {
      $output = \json_decode($order->order);
      $output->meta = \json_decode($order->meta);
      $output->status = $order->status;
      return $output;
    });
  }

  /**
   * Return orders count
   *
   * @param array $requestData['userId', 'OrderHistoryPermission']
   * @return int $ordersCount
   */
  public function count(array $requestData)
  {
    extract($requestData);
    $companyCustomersId = $this->getCompanyCustomersId($OrderHistoryPermission, $userId);
    $where = $this->prepareWhereStatement($requestData, isset($status) ? $status : null);
    return $this->getDBOrders($companyCustomersId, 'userId', $where, true);
  }

  public function getInvoice($orderID, $ifsUserID, $companyId, $invoiceID = null) {
    $resp = ['success' => false, 'data' => null];

    if (!$invoiceID) {
      $invoiceResp = $this->invoiceApi->getInvoiceStatusById($orderID);

      if ($invoiceResp->success && gettype($invoiceResp->data->body) == 'array' && isset($invoiceResp->data->body[0])) {
        $invoice = $invoiceResp->data->body[0];
        $resp['data'] = $invoice;
        $resp['success'] = true;

        $invoiceID = $invoice->INVOICE_ID;
      }
    }
    if (!$invoiceID) return $resp;

    $invoicePdf = $this->invoiceApi->getInvoice($invoiceID, $companyId, $ifsUserID);
    if ($invoicePdf->success && isset($invoicePdf->data->body) && 
      strpos($invoicePdf->data->body, 'https') !== false) {

      if (!$resp['data']) {
        $resp['data'] = (object) ['pdfLink' => $invoicePdf->data->body];
        $resp['success'] = true;
      } else $invoice->pdfLink = $invoicePdf->data->body;

    }
    return $resp;
  }

  /**
   * Create new row in DB or update it
   * 
   * @param object $shopifyOrderObject Order object
   * @return void
   */
  public static function updateOrCreate($shopifyOrderObject)
  {
    $dataToUpdate = [
      'customer_id' => $shopifyOrderObject->customer->id,
      'order' => json_encode($shopifyOrderObject),
      'status' => OrderIFS_Status::Released,
    ];
    if (isset($shopifyOrderObject->meta) && $shopifyOrderObject->meta)
      $dataToUpdate['meta'] = json_encode($shopifyOrderObject->meta);

    self::updateOrderStatus($shopifyOrderObject, $dataToUpdate);

    $order = OrderHistory::updateOrCreate(
      [
        'id' => $shopifyOrderObject->id,
      ],
      $dataToUpdate
    );
    $order->save();
  }

  /**
   * Get ifs data from shopify order meta
   */
  public function getIFSOrderData($shopifyOrder)
  {
    $this->metaArr = $this->shopifyOrderApi->getMeta($shopifyOrder->id);
    return $this->filterMetadata($this->IFSNamespace);
  }

  /**
   * Filter meta
   */
  private function filterMetadata($namespace)
  {
    $output = [];
    if (empty($this->metaArr)) return null;
  
    foreach($this->metaArr as $metaObj) {
      if ($metaObj->namespace == $namespace) {
        $output[$metaObj->key] = $metaObj->value;
      }
    }
    return $output;
  }

  /**
   * Return array of customers id depends on permission
   *
   * @param string $ordersPermission
   * @param integer $id
   * @return array
   */
  private function getCompanyCustomersId(string $ordersPermission, int $id)
  {
    if (!$this->customersId) {
      $this->customersId = $ordersPermission === 'company' ? $this->getCompanyCustomers($id, 'c.id')->toArray() : [$id];
    }
    return $this->customersId;
  }

  /**
   * Update order status if canceled or have IFS order data
   * 
   * @param object $shopifyOrderObject Order object
   * @return void
   */
  private static function updateOrderStatus($shopifyOrderObject, &$dataToUpdate) {
    if (isset($shopifyOrderObject->meta['IFSOrderStatus']) && $shopifyOrderObject->meta['IFSOrderStatus']) {
      $dataToUpdate['status'] = $shopifyOrderObject->meta['IFSOrderStatus'];
    }
  }

  /**
   * Return customers collection
   * 
   * 
   */
  public function getCompanyCustomers($userId, $select)
  {
    return $this->customerManagement->getCompanyUsers($userId, $select)
      ->map(function ($customer) {
        return $customer->id;
      });
  }

  /**
   *  Send mail to request return productr from order
   */
  public function orderReturnRequest($orderFormData)
  {
    $customer = $this->customerManagement->getCustomerWithCompany($orderFormData['userId']);
    $orderFormData['customer'] = $customer;

    $mail = $this->notificationService->orderReturnRequest(
      $orderFormData
    );

    if ($mail['success']) {
      return [
        'success' => true,
        'message' => "Thank you for submitting your request. A Peerless-AV Customer Care professional will be in touch shortly.",
      ];
    } else {
      return [
        'success' => false,
        'message' => 'Something went wrong, please reload page and try again',
      ];
    }
  }

  public function firstCreatedAt($requestData){
    extract($requestData);
    $companyCustomersId = $this->getCompanyCustomersId($OrderHistoryPermission, $userId);
    return OrderHistory::whereIn('customer_id', $companyCustomersId)
      ->orderBy('created_at', 'ASC')
      ->first(['created_at']);
  }

  /**
   * Get history orders from database
   *
   * @param array $customersId array of Customer ids
   * @param array $select What data should we get from DB
   * @param array $where 
   * @param int $count
   * @param int $limit
   * @param int $offset
   * @return void
   */
  private function getDBOrders($customersId, $select = '*', $where = [], $count = null, $limit = null, $offset = 0)
  {
    $query = OrderHistory::whereIn('customer_id', $customersId);
    if (!empty($where)) {
      $this->buildWhereStatement($query, $where);
    }
    // \Log::debug(print_r($query->toSql(), true));
    // \Log::debug(print_r($query->getBindings(), true));
    if ($count) return $query->count();
    if ($offset) $query->skip($offset);
    if ($limit) $query->take($limit);
    $query->orderBy('created_at', 'DESC');
    return $query->get($select);
  }

  /**
   * Build where request to DB for order History
   * 
   */
  private function prepareWhereStatement($data, $status = null)
  {
    extract($data);
    $where = [];
    if ($search != 'false') {
      $where['searchStartDate'] = $searchStartDate;
      $where['searchEndDate'] = $searchEndDate;
      $where['searchInputValue'] = $searchInputValue;
      $where['search'] = $search;
    }

    if ($status && $status != 'all') {
      $where['status'] = $status;
    }
    return $where;
  }

  private function buildWhereStatement($query, $where)
  {
    if (isset($where['id']) && $where['id']) {
      $query->where('id', '=', $where['id']);
    }
    if (isset($where['status']) && $where['status']) {
      $query->status($where['status']);
    }
    if (isset($where['search']) && $where['search'] == 'true') {
      if (isset($where['searchInputValue']) && $where['searchInputValue'] != '') {
        $query->search($where['searchInputValue']);
      }
      $query->whereBetween('created_at', [
        Carbon::parse($where['searchStartDate'])->toDateTimeString(),
        Carbon::parse($where['searchEndDate'])->toDateTimeString()
      ]);
    }
  }
}
