<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Specification;
use App\Models\SpecificationCategory;

class ShopifyProductSync extends ShopifyProduct
{
  public function __construct($api)
  {
    parent::__construct($api);
  }

  public function syncFromShopify($shopId, $handle, $productId)
  {
    $haveSpec = false;
    if (empty($this->productMeta)) return;
    $specCats = SpecificationCategory::where('shop_id', $shopId)
      ->get(['cat_name', 'id', 'lang']);
    $specCatsArr = $specCats->toArray();

    foreach ($this->productMeta as $metaArr) {
      foreach ($specCatsArr as $sCatArr) {
        $specKey = $this->getSpeckey($sCatArr['cat_name']);
        
        if ($specKey == $metaArr['key']) {
          $productStoredData = $this->getProductStoredSpec($sCatArr, $productId, $shopId);

          $parsedMeta = $this->parseShopifyMeta($metaArr['value']);
          $this->deleteProductData($productStoredData, $parsedMeta);
          $this->updateProduct($parsedMeta, $sCatArr['id'], $shopId, $handle, $productId, $sCatArr['lang']);
          $haveSpec = true;
          continue;
        }
      }
    }
    return $haveSpec;
  }

  private function getProductStoredSpec ($sCatArr, $productId, $shopId) {
    return Specification::leftJoin('products', 'products.specification_id', '=', 'specifications.id')
      ->where('specifications.shop_id', $shopId)
      ->where('products.product_id', $productId)
      ->where('specifications.category_id', $sCatArr['id'])
      ->where('specifications.lang', $sCatArr['lang'])->get(['products.id as productId', 'specifications.name as specName'])->toArray();
  }

  private function deleteProductData($productStoredData, $parsedMeta) {
    foreach ($productStoredData as $prodStoredSpec) {
      if (!isset($parsedMeta[$prodStoredSpec['specName']])) {
        Product::where('id', $prodStoredSpec['productId'])
          ->delete();
      }
    }

  }

  private function getSpeckey($catName)
  {
    $specKey = str_slug($catName, '_');
    switch ($catName) {
      case 'Product Specifications':
        $specKey = 'product_spec';
        break;
      case 'Package Specifications':
        $specKey = 'package_spec';
        break;
      case 'Spécifications du produit': // Product Specifications fr
        $specKey = 'product_spec_fr';
        break;
      case 'Spécifications du colis': // Package Specifications fr
        $specKey = 'package_spec_fr';
        break;
      case 'Produktspezifikationen': // Product Specifications de
        $specKey = 'product_spec_de';
        break;
      case 'Paket-Spezifikationen': // Package Specifications de
        $specKey = 'package_spec_de';
        break;
      case 'Especificaciones del producto': // Product Specifications es
        $specKey = 'product_spec_es';
        break;
      case 'Especificaciones del paquete': // Package Specifications es
        $specKey = 'package_spec_es';
        break;
    }
    return $specKey;
  }

  private function updateProduct($parsedMeta, $catId, $shopId, $handle, $productId, $lang)
  {
    $postion = 1;
    // dd($parsedMeta);
    foreach ($parsedMeta as $specName => $specValue) {
      $specObj = $this->getOrCreateSpec($specName, $catId, $shopId, $lang);
      Product::updateOrCreate([
        'product_id' => (int)$productId, 'specification_id' => (int)$specObj->id, 'shop_id' => (int)$shopId,
      ], ['value' =>  \strip_tags($specValue), 'handle' => $handle, 'lang' => $lang, 'position' => $postion]);
      $postion++;
    } 
  }

  private function getOrCreateSpec($specName, $catId, $shopId, $lang)
  {
    return Specification::firstOrCreate([
      'name' => trim(\strip_tags($specName), ' | '), 'category_id' => $catId, 'lang' => $lang,
      'shop_id' => $shopId, 'active' => 1, 'position' => 200,
    ]);
  }

  private function parseShopifyMeta($metaValue)
  {
    $metaValue = trim($metaValue, ' | ');
    $metaArr = explode(' | ', $metaValue);
    $response = [];
    foreach ($metaArr as $spec) {
      $specArr = explode(';', $spec);
      $specKey = !empty($specArr[0]) ? trim($specArr[0]) : null;
      $specValue = !empty($specArr[1]) ? trim($specArr[1]) : null;

      if ($specKey && $specValue) $response[$specKey] = $specValue;
    }
    return $response;
  }
}