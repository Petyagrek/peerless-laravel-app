<?php

namespace App\Services;

class ShopifyProductApi
{
    private $api;
    private const API_URL = '/admin/products';

    public function __construct ($api) {
        $this->api = $api;

    }

    public function get ($id = null, $params = []) {
        try {
            $url = self::API_URL . (!is_null($id) ? '/'.$id : '') . '.json';
            return $this->api->rest('GET', $url, $params);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error' => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false
            ];
        }
    }

    /**
     * Return user meta data;
     *
     * @param int $id
     * @return object user meta data
     */
    public function getMeta($id)
    {
        $url = self::API_URL. '/' . $id . '/metafields.json';
        return json_decode(json_encode($this->api->rest('GET', $url)->body->metafields), true);
    }

    public function updateMetafield($metaArr, $newValue)
    {
        $this->api->rest('PUT', '/admin/metafields/' . $metaArr['id'] . '.json', [
            'metafield' => [
                'value' => $newValue
            ]
        ]);
    }
    
    public function deleteMetafield($metaArr)
    {
        $this->api->rest('DELETE', '/admin/metafields/' . $metaArr['id'] . '.json');
    }

    /**
     * Update product in Shopify Store
     * 
     * @param array $data Data to be updated
     * @param int $id product id
     * 
     * @return mixed
     */
    public function update($data, $id)
    {
        try {
            $url = self::API_URL.'/'.$id.'.json';
            return $this->api->rest('PUT', $url, ['product' => $data])->body->product;
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    /**
     * Return products count
     * 
     */
    public function count()
    {
        try {
            return $this->api->rest('GET', self::API_URL . '/count.json')->body->count;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error'   => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }
}