<?php

namespace App\Services;

use App\Models\MonitorMounts;
use App\Models\MountAdapters;
use App\Models\MountsException;
use App\Models\Variant;

class MountFinderService {

    /** 
     * Preapare data for search in DB
     * 
     * @param array $requestData['query', 'limit']
     * @return array App\Models\MonitorMounts 
     */
    public function get ($requestData) {
        if (isset($requestData['query'])) {
            // return MountsException::whereHas('monitor_mounts')->where('display_name', 'like', '%'.$requestData['query'].'%')
            //     ->where('status', 0)->groupBy('display_name')
            //     ->take(isset($requestData['limit']) ? $requestData['limit'] : 10)->get();
            return MonitorMounts::where('display_name', 'like', '%'.$requestData['query'].'%')
            ->active()
            // ->activeQuery($requestData['query'])
            ->uniqByDisplayName()
            ->take(isset($requestData['limit']) ? $requestData['limit'] : 10)->get();
        }
        else {
            return $this->getMonitorMountsDb($requestData, isset($requestData['limit']) ? $requestData['limit'] : 10);
        }
    }

     /**
     * Return adaptors 
     *
     * @param string $requestData
     * @return array
    */
    public function getAdaptors($requestData)
    {
        return $this->getAdaptorsDB($requestData);
    }

    public function getMountException($requestData) {
        return $this->getMountsException($requestData);
    }

    /**
     * Return mount adaptors count
     *
     * @param string $query
     * @return int App\Models\MonitorMounts count
    */
    public function adaptorsCount () {
        return $this->getAdaptorsDb(null, true);
    }

    /**
     * Search for monitors in DB
     * 
     * @param string $query
     * @param string $limit
     * @param bool $count
     * @return mixed
     */
    private function getMonitorMountsDb($requestData, $limit = '10' ) {
        if (isset($requestData['display_name']) && isset($requestData['product_handle'])) {
            $query =  MonitorMounts::where('display_name', 'like', $requestData['display_name'])
            ->where('product_handle', 'like', $requestData['product_handle']);

        } else if (isset($requestData['query'])) {
            $query =  MonitorMounts::where('display_name', 'like', "%".$requestData['query']."%");

        } else if (isset($requestData['display_manufacturer']) && isset($requestData['display_part_number'])) {

            $query =  MonitorMounts::where('display_manufacturer', 'like', $requestData['display_manufacturer'])
                        ->where('display_part_number', 'like', $requestData['display_part_number']);
        }

        $query->groupBy('display_name');
        return $query->take($limit)->get();
    }


    /**
     * Search for mounts exception in DB
     * 
     * @param string $query
     * @param string $limit
     * @param bool $count
     * @return mixed
     */
    private function getMountsException($requestData, $count = false, $limit = '10' ) {
        $query =  \App\Models\MountsException::where('display_name', 'like', $requestData['display_name'])
            ->where('product_handle', 'like', $requestData['product_handle']);

        if ($count) return $query->count();

        $query->groupBy('display_name');
        return $query->take($limit)->get();
    }

    public function getProductByMountName($query, $adapters_only='no') {
        $productHandles = collect([]);
        if($adapters_only != 'yes')
            $productHandles = MonitorMounts::displayName($query)->active()->get(['product_handle'])->unique('product_handle')->pluck('product_handle');
        $adaptorHandles = MountAdapters::displayName($query)->active()->get(['product_handle'])->unique('product_handle')->pluck('product_handle');

        $handles = $productHandles->merge($adaptorHandles)->unique();
        if($handles->count() > 0) {
            $handles = Variant::where('shop_id', env('MAIN_SHOP_ID'))->whereIn('sku', $handles)->get()->pluck('variant_id');
        }
        return [
            'items' => $handles,
            'count' => $handles->count(),
        ];
    }

    /**
     * Search for adaptors in DB
     * 
     * @param string $requestData
     * @return array
     */
    public function getAdaptorsDB ($requestData)
    {
        $adaptors = MountAdapters::where('display_name', $requestData['display_name'])
                    ->where('product_handle', $requestData['product_handle'])
                    ->get();
        return [
            'items' => $adaptors,
            'count' => $adaptors->count(),
        ];
    }

    /**
     * Search for monitors in DB
     * 
     * @param string $query
     * @param string $limit
     * @param bool $count
     * @return mixed
     */
    // private function getAdaptorsDb($requestData, $count = false, $limit = '10' ) {
        // if (isset($requestData['query'])) {
        //     $query =  MonitorMounts::where('display_name', 'like', "%".$requestData['query']."%");

        // } else if (isset($requestData['display_manufacturer']) && isset($requestData['display_part_number'])) {

        //     $query =  MonitorMounts::where('display_manufacturer', 'like', $requestData['display_manufacturer'])
        //                 ->where('display_part_number', 'like', $requestData['display_part_number']);

        // }
        // return MountAdapters::count();
        // if ($count) return $query->count();
        // return $query->take($limit)->get();
    // }
}