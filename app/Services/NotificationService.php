<?php 

namespace App\Services;

use App\Mail\CustomerNotification;
use Illuminate\Support\Facades\Mail;

class NotificationService {
    private $shop;

    public function __construct($shop) {
        $this->shop = $shop;
    }

    public function sendInvite ($customer)
    {
        $mailData = [
            'template' => 'vendor.mail.html.invite',
            'subject' => 'Confirm your Peerless-AV account!',
            'customer' => $customer,
        ];
        try {
            Mail::to([
                $customer->email,
                // 'jsuthar@codal.com',
                // 'ivanchuda@gmail.com',
                // 'iabbott@codal.com'
            ])->send(new CustomerNotification($mailData, $this->shop));
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'errors'   => ['Error happend please reload and try agin.'],
            ];
        }
        return [
            'success' => true,
            'message' => 'Invite was send successfuly',
        ];
    }

    public function sendJoinExistingCustomer ($customer, $company, $activation_token, $lang)
    {
        $mailData = [
            'customer' => $customer,
            'company' => $company,
            'template' => 'vendor.mail.html.join',
            'subject' => 'Confirm your Peerless-AV company account',
            'activation_token' => 'https://peerless-av.myshopify.com/pages/join/?token='.$activation_token,
        ];
        switch ($lang) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.join';
                $mailData['subject'] = 'Confirmer le compte Peerless-AV de votre entreprise';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.join';
                $mailData['subject'] = 'Bestätigung Ihres Peerless-AV-Firmenkontos';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.join';
                break;
        }
        try {
            Mail::to([
                $customer->email,
                // 'jsuthar@codal.com',
                // 'ivanchuda@gmail.com',
            ])->send(new CustomerNotification($mailData, $this->shop));
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'errors'   => ['Error happend please reload and try agin.'],
            ];
        }
        return [
            'success' => true,
        ];
    }

    public function sendUserJoinedMailToSuperAdmin($appCustomer, $company)
    {
        $superAdminMailData = [
            'customer' => $appCustomer,
            'company' => $company,
            'template' => 'vendor.mail.html.join_super_admin',
            'subject' => 'New Peerless-AV company user connected',
        ];

        try {
            Mail::to([
                // 'ivanchuda@gmail.com', 
                'orderdesk@peerless-av.com', 
                // 'jsuthar@codal.com',
                // 'iabbott@codal.com'
            ])->send(new CustomerNotification($superAdminMailData, $this->shop));
            //Mail::to($companyOwner->email)->send(new CustomerNotification($companyOwnerMailData, $this->shop));
        } catch (\Exception $e) {
            \Log::debug(print_r($e->getMessage(), true));
        }
    }

    public function sendUserJoinedMailToAdmins ($appCustomer, $companyAdmin, $company, $lang)
    {
        $companyOwnerMailData = [
            'customer' => $appCustomer,
            'owner' => $companyAdmin,
            'template' => 'vendor.mail.html.join_company_owner',
            'subject' => 'New Peerless-AV company user connected',
        ];

        switch ($companyAdmin->language) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.join_company_owner';
                $mailData['subject'] = 'Un nouvel utilisateur Peerless-AV s’est connecté';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.join_company_owner';
                $mailData['subject'] = 'Verbindung eines neuen Peerless-AV-Firmenbenutzers abgeschlossen';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.join_company_owner';
                break;
        }

        try {
            Mail::to([
                // 'ivanchuda@gmail.com', 
                $companyAdmin->email,
                // 'jsuthar@codal.com',
                // 'iabbott@codal.com'
            ])->send(new CustomerNotification($companyOwnerMailData, $this->shop));
        } catch (\Exception $e) {
            \Log::debug(print_r($e->getMessage(), true));
        }
    }

    public function adminCreateNewCompany ($data) {
        $mailData = [
            'data'     => $data,
            'template' => 'vendor.mail.html.admin_create_new_company_request',
            'subject'  => 'Request to create new business account',
        ];

        return $this->sendMail([
            // 'ivanchuda@gmail.com', 
            'webmaster@peerless-av.com',
            'info@peerless-av.com',
        ], $mailData );
    }

    public function adminNewUserJoinCompany ($data) {
        $mailData = [
            'data'     => $data,
            'template' => 'vendor.mail.html.admin_join_company_request',
            'subject'  => 'Request to join existing business account',
        ];

        return $this->sendMail([
            // 'ivanchuda@gmail.com', 
            'webmaster@peerless-av.com',
        ], $mailData );
    }

    public function adminSendInvite ($customerData, $activationURL) {
        $customerData->activationURL = $activationURL;
        $mailData = [
            'data'     => $customerData,
            'template' => 'vendor.mail.html.admin_send_invite',
            'subject'  => 'Confirm your new Peerless-AV account',
        ];

        return $this->sendMail([
            $customerData->email
        ], $mailData);
    }

    public function rejectMail($customer, $company, $reason)
    {
        $rejectMailData = [
            'customer' => $customer,
            'company'  => $company,
            'reason'   => $reason,
            'template' => 'vendor.mail.html.reject_customer',
            'subject'  => 'Peerless-AV company account rejected',
        ];

        try {
            Mail::to([
                $customer->email, 
                // 'ivanchuda@gmail.com',
                // 'jsuthar@codal.com',
                // 'iabbott@codal.com'
            ])->send(new CustomerNotification($rejectMailData, $this->shop));
        } catch (\Exception $e) {
            // \Log::debug(print_r($e->getMessage(), true));
        }
    }

    public function approveMail ($customer, $company) {
        $rejectMailData = [
            'customer' => $customer,
            'company'  => $company,
            'template' => 'vendor.mail.html.approve',
            'subject'  => 'Peerless-AV company account approved',
        ];

        try {
            Mail::to([
                $customer->email, 
                // 'ivanchuda@gmail.com',
                // 'jsuthar@codal.com',
                // 'iabbott@codal.com'
            ])->send(new CustomerNotification($rejectMailData, $this->shop));
        } catch (\Exception $e) {
            \Log::debug(print_r($e->getMessage(), true));
        }
    }

    public function sendReserveCertifiedToCustomer ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.reserve_customer',
            'subject'  => 'Peerless-AV certified installer training request submitted',
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.reserve_customer';
                $mailData['subject'] = 'Demande de formation pour installateur agréé Peerless-AV envoyée';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.reserve_customer';
                $mailData['subject'] = 'Antrag auf Teilnahme an Peerless-AV-Zertifizierungsschulung für Installationstechniker eingereicht';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.reserve_customer';
                break;
        }

        return $this->sendMail([
            $data['email'], 
        ], $mailData );
    }

    public function sendReserveCertifiedToAdmin ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.reserve_admin',
            'subject'  => 'New request for in-person certified installer training',
        ];

        return $this->sendMail([
            "training@peerless-av.com", 
        ], $mailData );
    }

    public function sendReserveSalesToCustomer($data)
    {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.reserve_sales_customer',
            'subject'  => 'Peerless-AV sales training request submitted',
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.reserve_sales_customer';
                $mailData['subject'] = 'Demande de formation à la vente Peerless-AV envoyée';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.reserve_sales_customer';
                $mailData['subject'] = 'Antrag auf Peerless-AV-Verkaufsschulung eingereicht';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.reserve_sales_customer';
                break;
        }

        return $this->sendMail([
            $data['email'], 
        ], $mailData );
    }

    public function sendReserveSalesToAdmin($data)
    {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.reserve_sales_admin',
            'subject'  => 'New request for in-person sales training',
        ];

        return $this->sendMail([
            "training@peerless-av.com", 
        ], $mailData );
    }

    public function tradeShowMeetingRequestCustomer ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.meeting_customer',
            'subject'  => 'Meeting Request with Peerless-AV' . 
                ' at ' . $data['title'],
        ];


        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.meeting_customer';
                $mailData['subject'] = 'Demande de réunion avec' . $data['title'];
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.meeting_customer';
                $mailData['subject'] = 'Bitte um Treffen bei' . $data['title'];
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.meeting_customer';
                break;
        }

        return $this->sendMail([
            $data['email'], 
        ], $mailData );
    }
    

    public function tradeShowMeetingRequestAdmin ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.meeting_admin',
            'subject'  => 'Meeting Request' . 
                ( isset($data['company']) ?  ' from ' . $data['company'] : '' ) . 
                ' at ' .$data['title'],
        ];

        $recivers = ['marketing@peerless-av.com', 'bethg@lotus823.com'];
        switch ($data['site']) {
            case 'UK':
            case 'FR':
            case 'DE':
                $recivers = ['marketing@peerless-av.eu.com'];
        }

        return $this->sendMail($recivers, $mailData );
    }

    public function customSolutions ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.customers_solutions',
            'subject' => 'New Peerless-AV custom solutions request',
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.customers_solutions';
                $mailData['subject'] = 'Nouvelle demande de solutions sur mesure Peerless-AV';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.customers_solutions';
                $mailData['subject'] = 'Neue Anforderung einer kundenspezifischen Lösung von Peerless-AV';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.customers_solutions';
                break;
        }

        $reciver = 'info@peerless-av.com';
        switch ($data['site']) {
            case 'UK':
            case 'FR':
            case 'DE':
                $reciver = 'sales@peerless-av.eu.com';
                break;
            case 'Latin America':
                $reciver = 'servicioalcliente@peerless-av.com';
                break;
        }
        return $this->sendMail([
            $reciver 
        ], $mailData );
    }

    public function literatureRequest ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.literature_request',
            'subject' => 'New customer literature request',
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.literature_request';
                $mailData['subject'] = 'Nouvelle demande de documentation client';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.literature_request';
                $mailData['subject'] = 'Neue Kunden-Lesematerialanforderung';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.literature_request';
                break;
        }

        $recivers = ['info@peerless-av.com', 'marketing@peerless-av.com'];
        switch ($data['site']) {
            case 'UK':
            case 'FR':
            case 'DE':
                $recivers = ['sales@peerless-av.co.uk'];
                break;
            case 'Latin America';
                $recivers = ['literature@peerless-av.com'];
                break;
        }

        return $this->sendMail($recivers, $mailData);
    }

    public function hospitality ($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.hospitality',
            'subject'  => 'New Hospitality customer message',
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.hospitality';
                $mailData['subject'] = 'Nouveau message à l’attention d’un client du secteur hôtellerie/restauration';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.hospitality';
                $mailData['subject'] = 'Neue Hospitality-Kundennachricht';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.hospitality';
                break;
        }

        return $this->sendMail([
            'Hospitality@peerless-av.com',
        ], $mailData );
    }

    public function orderReturnRequest ($data)
    {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.return_order',
            'subject' => 'Peerless-AV order return request'
        ];

        switch ($data['lang']) {
            case 'FR':
                $mailData['template'] = 'vendor.mail.html.fr.return_order';
                $mailData['subject'] = 'Demande de retour de commande Peerless-AV';
                break;
            case 'DE':
                $mailData['template'] = 'vendor.mail.html.ge.return_order';
                $mailData['subject'] = 'Antrag auf Rücksendung einer Peerless-AV-Bestellung';
                break;
            case 'LA':
                $mailData['template'] = 'vendor.mail.html.la.return_order';
                break;
        }

        return $this->sendMail([
            'returns@peerless-av.com',
        ], $mailData);
    }

    public function sendLangifySyncDoneToSuperAdmin($data) {
        $mailData = [
            'data' => $data,
            'template' => 'vendor.mail.html.langify_sync',
            'subject' => "Langify sync to ".  $this->shop->shopify_domain ." finished." 
        ];

        return $this->sendMail([
            // 'returns@peerless-av.com',
            'imansuri@codal.com',
        ], $mailData);
    }

    public function sendRequestToJoinWebinar($customer_email, $eventName)
    {
        $mailData = [
            'customer_email' => $customer_email,
            'event_name' => $eventName,
            'template' => 'vendor.mail.html.webinar_join_request',
            'subject' => 'Requested to create meeting attendee account'
        ];

        return $this->sendMail([
            'ivanchuda@gmail.com',
            'CRecchia@peerless-av.com'
        ], $mailData);
    }

    private function sendMail ($toArr, $data) 
    {
        // $toArr[] = 'iabbott@codal.com';
        // $toArr[] = 'ivanchuda@gmail.com';
        // $toArr[] = 'jsuthar@codal.com';
        // $toArr[] = 'vitalikmihaylishin@gmail.com';
        try {
            Mail::to($toArr)->send(new CustomerNotification($data, $this->shop));
            return ['success' => true];
        } catch (\Exception $e) {
             \Log::debug(print_r($e->getMessage(), true));
        }
        return ['success' => false];
    }
}