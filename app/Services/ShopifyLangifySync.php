<?php

namespace App\Services;

use \OhMyBrew\ShopifyApp\Models\Shop;
use \OhMyBrew\BasicShopifyAPI;

class ShopifyLangifySync 
{
  public function __construct($shop)
  {
    $this->shop = $shop;
    $this->shopifyShopApi = new ShopifyShopApi($shop->api());

    $this->mainShopModel = Shop::find(env('MAIN_SHOP_ID'));
    $this->mainShopApi = $this->mainAppApi($this->mainShopModel);
    $this->shopifyMainShopApi = new ShopifyShopApi($this->mainShopApi);
    
    $this->notificationService = new NotificationService($shop);
    $this->pageInfo = null;
  }

  public function sync ()
  {
    $metaCount = $this->shopifyMainShopApi->getMetafieldsCount();
    if (!$metaCount['success']) return;

    $this->handleMeta();

    $this->notificationService
          ->sendLangifySyncDoneToSuperAdmin($this->mainShopModel);
  }

  private function handleMeta () {
    $metaLimit = 50;

    $lyCount = 0;
    do {
      $this->processMetasSync($metaLimit, $lyCount);
      sleep(1);
    } while ($this->pageInfo);
  }

  private function processMetasSync ($limit, &$lyCount)
  {
    $args = ['limit' => $limit];
    if ($this->pageInfo) $args['page_info'] = $this->pageInfo;
    $mainShopMetaFieldsResp = $this->shopifyMainShopApi->getMetafields($args);

    if (!$mainShopMetaFieldsResp['success']) return;

    $mainShopMetaFields = $mainShopMetaFieldsResp['data'];
    $this->pageInfo = $mainShopMetaFields->link->next;
    $mainShopMetaFields = $mainShopMetaFields->body->metafields;

    foreach ($mainShopMetaFields as $metaObj) {
      if ($this->isLangifyMeta($metaObj)) {
        $this->updateOrCreateMeta($metaObj);
        $lyCount++;
        \Log::info(print_r($lyCount, true));
      }
    }
  }

  private function updateOrCreateMeta($metaObj)
  {
    $currentShopMeta = $this->shopifyShopApi->getMetafields(
      ['namespace' => $metaObj->namespace, 'key' => $metaObj->key]
    );
    if ($currentShopMeta['success'] && $currentShopMeta['data'] && isset($currentShopMeta['data']->body->metafields[0])) {
      $id = $currentShopMeta['data']->body->metafields[0]->id;
      $updateResp = $this->shopifyShopApi->updateMetafield(["value" => $metaObj->value, "value_type" => $metaObj->value_type,], $id);
    } else {
      $newMetaObj = $this->buildMetaObj($metaObj);
      $this->shopifyShopApi->createMetafield($newMetaObj);
    }
  }

  protected function buildMetaObj($metaObj)
  {
    return (object)[
      'key' => $metaObj->key,
      'namespace' => $metaObj->namespace,
      'value' => $metaObj->value,
      'value_type' => $metaObj->value_type,
    ];
  }

  private function isLangifyMeta($metaObj) {
    return strpos($metaObj->namespace, 'ly') === 0 || strpos($metaObj->key, 'ly') === 0;
  }

  private function mainAppApi ($model)
  {
    $api = new BasicShopifyAPI();
    $api->setShop($model->shopify_domain);
    $api->setAccessToken($model->shopify_token);
    return $api;
  }
}