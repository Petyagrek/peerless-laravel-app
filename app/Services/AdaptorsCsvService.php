<?php
namespace App\Services;

use App\Interfaces\AdaptorsCsvInterface;
use App\Helpers\Base64ToTemp;

class AdaptorsCsvService implements AdaptorsCsvInterface {

    private $csv_to_temp = null;
    private $temp_data = null;
    private $separator = ',';
    private $allowed_keys = [
        'product_handle' => 'product_handle',
        'brand' => 'brand',
        'adapter_name' => 'adapter_name',
        'av_model_display' => 'av_model_display'
    ];
    private $keys = [];

    public function set_base64_data($base64_data)
    {
        $this->csv_to_temp = new Base64ToTemp($base64_data);
        $this->temp_data = $this->csv_to_temp->get_b64_content();
        return $this;
    }

    public function set_file($file_path)
    {
        $this->temp_data = fopen($file_path, 'r+');
        return $this;
    }

    public function to_raw_array()
    {
        $raw_arr = [];
        while($csv_row = fgetcsv($this->temp_data, 0, $this->separator))
            $raw_arr[] = $csv_row;
        fclose($this->temp_data);
        return $raw_arr;
    }

    public function as_array()
    {
        $items = [];
        $raw_arr = $this->to_raw_array();
        $raw_size = count($raw_arr);
        if($raw_size > 1) {
            $this->keys = $this->get_keys($raw_arr[0]);
            return array_map([$this, 'to_array'], array_slice($raw_arr, 1));
        }
        else {
            return [];
        }
    }

    private function get_keys($raw_arr)
    {
        $keys = array_map([$this, 'to_handle'], $raw_arr);
        return $keys;
    }

    private function to_handle($key)
    {
        if(array_key_exists(strtolower($key), $this->allowed_keys)) {
            return $this->allowed_keys[strtolower($key)];
        }
    }

    private function to_array($row)
    {
        $_row = array_map([$this, 'to_int_if_number'], $row);
        $_row = array_combine($this->keys, $_row);
        $_row['display_name'] = $_row['brand'] .' '.$_row['av_model_display'];
        unset($_row['']);
        return $_row;
    }

    private function to_int_if_number($value)
    {
        return is_numeric($value) ? intval($value) : $value;
    }
}
