<?php

namespace App\Services;

use App\Models\Invoice;
use Carbon\Carbon;

class ShopifyInvoices {

    protected $businessJoinType = 'B2B Join';
    protected $businessAdminType = 'B2B Admin';
    protected $businessUnidentifiedType = 'B2B Unidentified';
    protected $consumerType = 'B2C';

    public function __construct()
    {
        $this->invoicesApi = new InvoiceApi();
        $this->customerManagement = new CustomerManagement();
    }

    /**
     * Return list of invoices depending on user perrmission
     *
     * @param array $requestData['id', 'invoicesPermission']
     * @return array $invoices
     */
    public function get (array $requestData)
    {
        extract($requestData);
        $companyCustomersId = $this->getCompanyCustomersId($invoicesPermission, $id);
        $where = $this->prepareWhereStatement($requestData);
        return $this->getInvoices($companyCustomersId, 
            ['id', 'INVOICE_DATE', 'DUE_DATE', 'CUSTOMER_PO_NO', 'INVOICE_NO', 'INVOICE_ID', 'GROSS_AMOUNT', 'customer_id'], 
            $where, null, $perPage, $offset);
    }

    /**
     * Return invoices count
     *
     * @param array $requestData['id', 'invoicesPermission']
     * @return void
     */
    public function count(array $requestData) 
    {
        extract($requestData);
        $companyCustomersId = $this->getCompanyCustomersId($invoicesPermission, $id);
        $where = $this->prepareWhereStatement($requestData);
        return $this->getInvoices($companyCustomersId, 'id', $where, true);
    }

    public function firstCreatedAt($requestData){
      extract($requestData);
      $companyCustomersId = $this->getCompanyCustomersId($invoicesPermission, $id);
      return Invoice::whereIn('customer_id', $companyCustomersId)
        ->orderBy('INVOICE_DATE', 'ASC')
        ->first(['INVOICE_DATE']);
    }

    public function getCompanyCustomers($userId, $select) 
    {
        return $this->customerManagement->getCompanyUsers($userId, 'c.id')
            ->map(function ($customer) {
                return $customer->id;
            });
    }

    /**
     * Get invoices from database
     *
     * @param array $customersId array of Customer ids
     * @param string $select What date should we get from DB
     * @param array $where 
     * @param int $count
     * @param int $limit
     * @param int $offset
     * @return void
     */
    public function getInvoices($customersId, $select = ['*'], $where = [], $count = null, $limit = null, $offset = 0)
    {
        $query = Invoice::whereIn('customer_id', $customersId);
        if (!empty($where)) {
            $this->buildWhereStatement($query, $where);
        }
        if ($count) return $query->count();
        if ($offset) $query->skip($offset);
        if ($limit) $query->take($limit);
        // \Log::debug(print_r($query->toSql(), true));
        // \Log::debug(print_r($query->getBindings(), true));
        $query->with(['customer' => function ($q) {
            $q->select('id', 'tags');
        }]);
        return $query->get($select);
    }

    /**
     * Create or update invoice
     *
     * @param object $draftObject Invoice object
     * @return void
     */
    public static function updateOrCreate ($draftObject) {

        $invoice = \App\Models\Invoice::updateOrCreate(
            [
                'id' => $draftObject->id,
            ],
            [
                'order_id' => $draftObject->order_id,
                'order_draft' => json_encode($draftObject),
                'customer_id' => $draftObject->customer->id,
                'invoice_url' => $draftObject->invoice_url,
                'status' => $draftObject->status,
            ]
        );
        // \Log::debug(print_r($invoice));
        $invoice->save();
    }

    /**
     * Return array of customers id depends on permission
     *
     * @param string $invoicesPermission
     * @param integer $id
     * @return array
     */
    private function getCompanyCustomersId (string $invoicesPermission, int $id)
    {
        return $invoicesPermission === 'company' ? 
                $this->getCompanyCustomers($id, 'c.id')->toArray() : [$id];
    }

    private function prepareWhereStatement ($data) {
        extract($data);
        $where = [];
        if($search != 'false') {
            $where['searchStartDate'] = $searchStartDate;
            $where['searchEndDate'] = $searchEndDate;
            $where['searchInputValue'] = $searchInputValue;
            $where['search'] = $search;
        }
        return $where;
    }

    private function buildWhereStatement ($query, $where) {
        if (isset($where['search']) && $where['search'] == 'true') {
            if (isset($where['searchInputValue']) && $where['searchInputValue'] != '') {

                $query->where(function ($inner_q) use ($where) {
                    $inner_q->where(function ($q) use ($where) {
                        $q->where('CUSTOMER_PO_NO', 'like', '%'.$where['searchInputValue'].'%');
                    })->orWhere(function ($q) use ($where) {
                        $q->where('INVOICE_NO', 'like', '%'.$where['searchInputValue'].'%');
                    });
                });

            }
            $query->whereBetween('INVOICE_DATE', [
                Carbon::parse($where['searchStartDate'])->toDateTimeString(), 
                Carbon::parse($where['searchEndDate'])->toDateTimeString()
            ]);
        }
    }

    /**
     * Sync all invoices from IFS
     */
    public function sync($invoiceDoesNotExist = false) {
        $b2bUserOrders = $this->getB2B_UsersOrders($invoiceDoesNotExist)->get(['order_history.id as orderId', 'order_history.customer_id']);

        // dd($b2bUserOrders->count());
        $b2bUserOrders->each(function ($order) {
            $invoiceResp = $this->invoicesApi->getInvoiceStatusById($order->orderId);
            $this->maybeStoreInvoice($invoiceResp, $order);
        });

        return ['succes' => true, 'message' => 'Invoices synchronized'];
    }

    private function getB2B_UsersOrders($invoiceDoesNotExist = false) {
        $query = \App\Models\OrderHistory::whereHas('customer', function ($q) {
            $q->accountType([$this->businessJoinType, $this->businessAdminType]);
        });
        if ($invoiceDoesNotExist) {
            $query->leftJoin('invoices', function ($join) {
                $join->on('order_history.id', '=', 'invoices.id');
            })->whereNull('invoices.id');
        }
        return $query;
    }

    private function maybeStoreInvoice($invoiceResp, $order) {
        if (gettype($invoiceResp->data->body) !== 'array' || !isset($invoiceResp->data->body[0])) return;
        $invoice = (array) $invoiceResp->data->body[0];

        if (isset($invoice['DELIVERY_ADDRESS']))
            $invoice['DELIVERY_ADDRESS'] = json_encode($invoice['DELIVERY_ADDRESS']);

        if (isset($invoice['INVOICE_ADDRESS']))
            $invoice['INVOICE_ADDRESS'] = json_encode($invoice['INVOICE_ADDRESS']);

        $this->convertInvoiceDateField($invoice, ['INVOICE_DATE', 'DUE_DATE', 'ORDER_DATE', 'DELIVERY_DATE']);
        Invoice::updateOrCreate([
            'id' => $order->orderId,
            'customer_id' => $order->customer_id,
        ], $invoice);
    }

    private function convertInvoiceDateField(&$invoice, $fieldsArr) {
        foreach($fieldsArr as $field) {
            if (isset($invoice[$field]) && !empty($invoice[$field])) {
                $invoice[$field] = Carbon::createFromFormat('m/d/Y', $invoice[$field])->toDateTimeString();
            }
        }
    }
}