<?php

namespace App\Services;

use GuzzleHttp\Client;

class ConfiguratorService
{

  // For Stage environment
  // const API_URL = 'https://configurator-stage-api.peerless-av.com';
  // const LED_API_URL = 'http://ledconfigurator-stage-api.peerless-av.com';

  // For Production environment
  const API_URL = 'http://configurator-api.peerless-av.com';
  const LED_API_URL = 'http://ledconfigurator-api.peerless-av.com';

  private $client;
  private $response = ['success' => true, 'data' => null,];

  public function __construct()
  {
    $this->client = new Client([
      'content-Type' => 'application/json',
      'accept' => 'application/json',
    ]);
    $this->configurations_token = config('app.configurations_token');
  }

  public function configurations($data, $userID)
  {
    try {
      $requestData = [
        'perPage'       => $data['perPage'],
        'user_id'       => $this->genereteToken($userID),
        'config_type'   => $data['type'],
        'shopify_view'  => 'true',
        'currentPage'   => $data['currentPage'],
        'orderByName'   => isset($data['orderByName']) ? $data['orderByName'] : 'quotations.updated_at',
        'orderBy'       => isset($data['orderBy']) ? $data['orderBy'] : 'desc',
        'is_deleted'    => 'false',
        // 'rfq_submitted' => 'false',
      ];

      if (isset($data['status'])) {
        $status = json_decode($data['status']);

        if (!$status->rfq_submitted || !$status->draft) {
          if ($status->rfq_submitted) $requestData['rfq_submitted'] = 'true';
          if ($status->draft) $requestData['rfq_submitted'] = 'false';
        }
      }
      // dd(self::API_URL . '/quotation?' . http_build_query($requestData));
      $request = $this->client->get(self::API_URL . '/quotation?' . http_build_query($requestData));

      $this->response['data'] = json_decode($request->getBody()->getContents());
    } catch (\Exception $e) {
      $this->response['success'] = false;
      $this->response['message'] = $e->getMessage();
    }

    return $this->response;
  }

  public function deleteConfigurator ($data, $userID)
  {
    try {
      $client = new Client([
        'content-Type' => 'application/json',
        'accept' => 'application/json',
        'Authorization' => "test",
      ]);

      $request = $client->delete(
        self::API_URL . "/quotation/" . $data['rfq_id'].
        "?user_id=" . $this->genereteToken($userID)
      );

      $this->response['data'] = json_decode($request->getBody()->getContents());
    } catch (\Exception $e) {
      $this->response['success'] = false;
      $this->response['message'] = $e->getMessage();
    }

    return $this->response;
  }
  
  public function ledConfigurations ($data, $userID) 
  {
    try {
      $requestData = [
        'perPage'          => $data['perPage'],
        'customer_id'      => $this->genereteToken($userID),
        'shopify_view'     => '1',
        'shopify_request'  => 'true',
        'currentPage'      => $data['currentPage'],
        'orderByName'      => isset($data['orderByName']) ? $data['orderByName'] : 'quotation.updated_at',
        'orderBy'          => isset($data['orderBy']) ? $data['orderBy'] : 'desc',
        'skip_deleted'     => 'false',
      ];

      if (isset($data['status'])) {
        $status = json_decode($data['status']);

        if (!$status->rfq_submitted || !$status->draft) {
          if ($status->rfq_submitted) $requestData['rfq_submitted'] = 'true';
          if ($status->draft) $requestData['rfq_submitted'] = 'false';
        }
      }
      // dd(self::LED_API_URL . '/prospect?' . http_build_query($requestData));
      $request = $this->client->get(self::LED_API_URL . '/prospect?' . http_build_query($requestData));

      $this->response['data'] = json_decode($request->getBody()->getContents());
    } catch (\Exception $e) {
      $this->response['success'] = false;
      $this->response['message'] = $e->getMessage();
    }

    return $this->response;
  }

  public function ledDeleteConfigurator ($data, $userID)
  {
    try {
      $request = $this->client->put(self::LED_API_URL . '/prospect/' . $data['config_no']. "?shopify_view=true", 
        [
          'form_params' => [
            'is_deleted'  => 1,
            'customer_id' => $this->genereteToken($userID),
          ]
        ]
      );

      $this->response['data'] = json_decode($request->getBody()->getContents());
    } catch (\Exception $e) {
      $this->response['success'] = false;
      $this->response['message'] = $e->getMessage();
    }

    return $this->response;
  }

  private function genereteToken ($userID)
  {
    return base64_encode($userID . '.' . $this->configurations_token);
  }
}
