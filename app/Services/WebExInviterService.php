<?php

namespace App\Services;

use GuzzleHttp\Client;

class WebExInviterService extends ShopifyWebEx {

  public function handleSendInvite()
  {
    $this->sendInvites((object) $this->getEvents());
  }

  private function sendInvites($events) {
    foreach ($events->data->bodyContent as $event) {
      $eventStartDate = strtotime($event->startDate);
      if ($eventStartDate > $this->beetweenAccountAttendeeDates->bigger_then
        && $eventStartDate < $this->beetweenAccountAttendeeDates->less_then) {
        $this->SendInvitationEmail($event->sessionKey);
      }
    }
  }

  public function SendInvitationEmail($id)
  {
    $requestData = [
      'service' => 'event.SendInvitationEmail',
      'xml_body' => '<sessionKey>' . $id . '</sessionKey><attendees>true</attendees><panelists>false</panelists>'
    ];
    $this->parseResponse(
      $this->sendRequest("POST", $this->get_xml($requestData)), 
      self::API_SCHEMA_EVENT
    );
    return $this->data;
  }
}