<?php

namespace App\Services;

class ShopifyOrderApi 
{
    private $api;
    private const API_URL = '/admin/api/2020-01/orders';

    public function __construct ($api) {
        $this->api = $api;
    }

    public function get ($id = null, $params = []) {
        try {
            $url = self::API_URL . (!is_null($id) ? '/'.$id : '') . '.json';
            return $this->api->rest('GET', $url, $params)->body->{is_null($id) ? 'orders' : 'order'};
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error' => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    public function cancel ($id, $get = []) {
        try {
            $url = self::API_URL . "/" . $id . "/close.json";
            $url .= !empty($get) ? "?".http_build_query($get) : '';
            return $this->api->rest("POST", $url)->body->order;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return [
                'error' => json_decode($e->getResponse()->getBody()->getContents())->errors,
                'success' => false,
            ];
        }
    }

    /**
     * Return user meta data;
     *
     * @param int $id
     * @return object user meta data
     */
    public function getMeta($id)
    {
        $url = self::API_URL. '/' . $id . '/metafields.json';
        return $this->api->rest('GET', $url)->body->metafields;
    }
}