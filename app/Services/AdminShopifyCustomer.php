<?php

namespace App\Services;

use App\Helpers\States;
use Illuminate\Support\Arr;

class AdminShopifyCustomer extends ShopifyCustomer
{
    private $companyService;

    public function __construct($shop)
    {
        parent::__construct($shop);

        $this->companyService = new CompanyService();
    }

    /**
     * Update customer in shopify and in app
     * 
     * @param array $data request data
     */
    public function adminUpdateCustomer($data, $shopID)
    {
        $customerRequest = $data['customer'];
        $customerId = $customerRequest['id'];
        $this->addConsumarCompany($customerRequest);
        // if ($customerRequest[''])
        // load customer data from shopify
        $metafields = $this->customerApi->getMeta($customerId);
        // update tags with account type that comes from request
        $customerRequest['tags'] =
            $this->addAccountTypeToTags($customerRequest['tags'], $customerRequest['account_type']);

        $customerRequest['tags'] = 
            $this->addStateToTags($customerRequest['tags'], $customerRequest['state']);

        if (isset($customerRequest['accepts_marketing']) && $customerRequest['accepts_marketing']) {
            $customerRequest['tags'] = $this->addNewsletterToTags($customerRequest['tags']);
            $this->data_to_insert['accepts_marketing'] = $customerRequest['accepts_marketing'];
        } else {
            $customerRequest['tags'] = $this->removeNewsletterFromTags($customerRequest['tags']);
            $this->data_to_insert['accepts_marketing'] = false;
        }

        $this->maybeMakeUnindentified($customerRequest);
        // get company and update tags
        $this->addOrRemoveCompany($customerRequest, $shopID);
        // change from boolean to int
        $this->prepareIntegerValues($customerRequest);
        // generete data to update
        $this->genereteUpdateRequestData($customerRequest, $metafields);
        $customer = $this->customerApi->update($this->data_to_insert, $customerId);
        // error updating user in shopify
        if ($this->isErrorResponse($customer)) {
            $data['success'] = false;
            $data['error'] = $customer['error'];
            $data['message'] = 'Error happened';
        } else {
            $this->customerManagement->updateCustomer($customerRequest, $shopID);
            $this->updateCustomerMetaInShopify($customerRequest);
            if (isset($customerRequest['customer_permission'])) {
                $this->customerManagement->updateOrCreatePermisions(
                    Arr::only($customerRequest['customer_permission'], $this->permisionsKeys),
                    $customerId
                );
            }
            $data['success'] = true;
            $data['customer'] = $customer;
            $data['message'] = 'Customer successfully updated.';
        }

        return $data;
    }

    /**
     * Update customer in shopify and in app
     * 
     * @param array $data request data
     */
    public function superAdminCreateCustomer($data, $shopID)
    {
        $customerRequest = $data['customer'];
        // add Account type to customer tags
        $customerRequest['tags'] = 
            $this->addAccountTypeToTags($customerRequest['tags'], $customerRequest['account_type']);

        
        $customerRequest['tags'] = 
            $this->addStateToTags($customerRequest['tags'], $customerRequest['state']);

        if (isset($customerRequest['accepts_marketing']) && $customerRequest['accepts_marketing']) {
            $customerRequest['tags'] = $this->addNewsletterToTags($customerRequest['tags']);
        } // else {
        //     $customerRequest['tags'] = $this->removeNewsletterFromTags($customerRequest['tags']);
        // }

        $this->maybeMakeUnindentified($customerRequest);
        $this->addOrRemoveCompany($customerRequest, $shopID);
        $this->addConsumarCompany($customerRequest);
        // change from boolean to int
        $this->prepareIntegerValues($customerRequest);
        $this->generateCreateRequestData($customerRequest);
        $this->data_to_insert['send_email_invite'] = true;

        $customer = $this->customerApi->create($this->data_to_insert);

        // handle shopify error
        if ($this->isErrorResponse($customer)) {
            $this->data_to_insert['success'] = false;
            $this->data_to_insert['error'] = $customer['error'];
            $this->data_to_insert['message'] = "Please fix errors above";
            return $this->data_to_insert;
        }
        $this->data_to_insert['customer'] = $customer;
        // create customer in app
        $this->customerManagement->handleCustomer($customerRequest, $customer);

        $this->cretePermissions(
            $customerRequest['customer_permission'],
            $customer,
            $customerRequest['account_type']
        );

        // $this->customerApi->sendInvite($customer->id);
        // customer get this state when no pass was provided
        // if ($customer->state !== States::Disabled) {
        //     $this->sendInvite($customer->id);
        // } else {
        //     $this->customerApi->sendInvite($customer->id);
        // }

        $this->data_to_insert['message'] = 'Customer was created succesfuly';
        return $this->data_to_insert;
    }

    /**
     * Return customer from DB
     * 
     * @param array $searchValue
     * @return array \App\Models\Customer  array of customers
     */
    public function getCustomers($searchValue, $shopID)
    {
        return $this->customerManagement
            ->getCustomers($searchValue, $shopID);
    }

    /** 
     * Return customer from DB
     * 
     * @param int $id Customer id
     * @param int $shopID
     * @return object \App\Models\Customer
     */
    public function getCustomerByID($id, $shopID)
    {
        return $this->customerManagement->getCustomerByID($id, $shopID);
    }

    /**
     * Delete customers
     * 
     * @param array $customerIds
     * @return void
     */
    public function destroy($customerIds)
    {
        foreach ($customerIds as $id) {
            $this->customerApi->delete($id);
        }
    }

    /**
     * Add account type to customer tags
     * 
     * @param string $tags
     * @param string $account_type
     * 
     * @return string $tags
     */
    private function addAccountTypeToTags($tags, $account_type)
    {
        $typlessTagsArr = !empty($tags) ? $this->getTypelessTags($tags, true) : [];
        $typlessTagsArr[] = $account_type;
        return implode(', ', $typlessTagsArr);
    }

    /**
     * Update customer $this->businessUnidentifiedType type 
     * if no company added and account type = $this->businessAdminType
     * 
     * @param array $customerRequest
     */
    private function maybeMakeUnindentified(&$customerRequest)
    {
        if (!$customerRequest['company_id'] && $customerRequest['account_type'] == $this->businessAdminType) {
            $typlessTagsArr =  $this->getTypelessTags($customerRequest['tags'], true);
            $customerRequest['account_type'] = $typlessTagsArr[] = $this->businessUnidentifiedType;
            $customerRequest['tags'] = implode(', ', $typlessTagsArr);

            $customerRequest['company_id'] = 0;
        }
    }

    /**
     * Update meta data in Shopify
     * 
     * @param array $customerRequest
     */
    private function updateCustomerMetaInShopify($customerRequest)
    {
        $permission = isset($customerRequest['customer_permission']) ? $customerRequest['customer_permission'] : [];
        foreach ($this->update_meta as $metaArr) {
            if (array_key_exists($metaArr['key'], $customerRequest) && isset($customerRequest[$metaArr['key']])) {
                $this->updateMetafield($metaArr, $customerRequest[$metaArr['key']]);
            }
            if ($permission && array_key_exists($metaArr['key'], $permission)) {
                $this->updateMetafield($metaArr, $permission[$metaArr['key']]);
            }
        }
    }

    /**
     * Add or remove company from customer
     * 
     * @param array $customerRequest
     */
    private function addOrRemoveCompany(&$customerRequest, $shopID)
    {
        $companyLessTags = !empty($customerRequest['tags']) 
            ? $this->getCompanyLessTags($customerRequest['tags'], true) 
            : [];
        if (
            $customerRequest['company_id'] &&
            in_array($customerRequest['account_type'], [$this->businessAdminType, $this->businessJoinType])
        ) {
            $company = $this->companyService->get(['id' => $customerRequest['company_id']],  $shopID);
            $companyLessTags[] = $this->companyFirstTagPart . $company->ifs_id;
            $customerRequest['company'] = $company->name;
            $customerRequest['tags'] = implode(', ', $companyLessTags);
        } else {
            $customerRequest['company_id'] = 0;
            $customerRequest['tags'] = implode(', ', $companyLessTags);
        }
    }

    /**
     *  Change to int field with boolean type, so shopify metafields to not store data as true or false
     * 
     * @param array $customerRequest
     */
    protected function prepareIntegerValues(&$customerRequest)
    {
        if (isset($customerRequest['customer_permission'])) {
            foreach ($customerRequest['customer_permission'] as $permissionKey => $permissionValue) {
                if (gettype($permissionValue) == 'boolean') {
                    $customerRequest['customer_permission'][$permissionKey] = (int)$permissionValue;
                }
            }
        }
    }

    protected function addConsumarCompany(&$customerRequest)
    {
        if ($customerRequest['account_type'] == $this->consumerType &&
            isset($customerRequest['company_consumar'])) 
        {
            if (!$customerRequest['customer_permission'])
                $customerRequest['customer_permission'] = [];
            $customerRequest['customer_permission']['company'] = $customerRequest['company_consumar'];
        }
        return $customerRequest;
    }

    /**
     * Genere
     */
    protected function genereteUpdateRequestData($data, $customerMeta)
    {
        $this->generateRequestDataForCustomer($this->update_calls_strategy['customer'], 0, $data);
        $this->generateUpdateRequestDataForMetafields(
            $this->update_calls_strategy['metafields'],
            0,
            $data,
            $customerMeta
        );
        if (isset($data['customer_permission'])) {
            $this->generateUpdateRequestDataForMetafields(
                $this->update_calls_strategy['metafields'],
                0,
                $data['customer_permission'],
                $customerMeta
            );
        }
    }

    protected function generateCreateRequestData($data)
    {
        $this->generateRequestDataForCustomer($this->calls_strategy['customers'], 0, $data);

        $this->generateCreateRequestDataForMetafields(
            $this->calls_strategy['metafields'],
            0,
            $data
        );
        if (isset($data['customer_permission'])) {
            $this->generateCreateRequestDataForMetafields(
                $this->calls_strategy['metafields'],
                0,
                $data['customer_permission']
            );
        }
    }
}
