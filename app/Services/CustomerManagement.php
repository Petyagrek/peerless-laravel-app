<?php 

namespace App\Services;

use App\Models\Company;
use App\Models\Customer;
use App\Models\CustomerPermission;

class CustomerManagement {
    private $customerData;

    public function handleCustomer ($data, $customer)
    {
        $this->fillCustomerData($data, $customer);
        return $this->storeCustomer();
    }

    private function storeCustomer ()
    {
        return Customer::create($this->customerData);
    }

    private function fillCustomerData ($data, $customer)
    {
        $this->customerData = [
            'id'           => $customer->id,
            'first_name'   => $data['first_name'],
            'last_name'    => $data['last_name'],
            'email'        => $data['email'],
            'state'        => $data['state'],
            'tags'         => $data['tags'],
            'account_type' => $data['account_type'],
            'shop_id'      => $data['shop_id'],
        ];

        if (isset($data['phone_pin'])) $this->customerData['phone_pin'] = $data['phone_pin'];
        if (isset($data['phone'])) $this->customerData['phone'] = $data['phone'];
        if (isset($data['company_id'])) {
            $this->customerData['company_id'] = $data['company_id'];
        }
        if (isset($data['industry'])) $this->customerData['industry'] = $data['industry'];
        if (isset($data['country'])) $this->customerData['country'] = $data['country'];
        if (isset($data['language'])) $this->customerData['language'] = $data['language'];
    }

    public function getCustomerWithCompany ($customerId)
    {
        return Customer::with('company')->find($customerId);
    }

    /**
     * Retrive customers from database
     * 
     * @param string $searchValue
     * @param int $shopiID
     * @param array $select
     * 
     * @return \Illuminate\Support\Collection
     */
    public function getCustomers ($searchValue, $shopID, $select = "*")
    {
        $query = Customer::with('company')->where('shop_id', $shopID);
        if ($searchValue) $query->adminSearch($searchValue);
        return $query->orderBy('updated_at', 'desc')->get([$select]);
    }
    
    public function getCustomerByID($id, $shopID)
    {
        return Customer::with('customer_permission')
            ->where('shop_id', $shopID)
            ->find($id);
    }

    public function getCustomer ($companyId) 
    {
        return Customer::where('company_id', $companyId)->first();
    }
    public function getCustomerByEmail($value, $shopID) 
    {
        return Customer::whereEmail($value)->where('shop_id', $shopID)->first();
    }

    public function getCustomerWhere($data) 
    {
        $query = Customer::where($data);
        // \Log::debug(print_r($query->toSql(), true));
        return $query->first();
    }

    public function getCustomersWhere($data) 
    {
        $query = Customer::where($data);
        // \Log::debug(print_r($query->toSql(), true));
        return $query->get();
    }

    public function getCompany ($name, $shopID) 
    {
        return Company::whereName($name)->where('shop_id', $shopID)->first();
    }

    public function updateCustomer ($data, $shopID)
    {
        return Customer::where('shop_id', $shopID)
            ->find($data['id'])
            ->update($data);
    }

    public function updateOrCreateCustomer($data, $customerId) 
    {
        return Customer::updateOrCreate(
            ['id' => $customerId],
            $data
        );
    }

    public function createDefaultPermissions($customer, $defaultPermissions) 
    {
        $defaultPermissions['customer_id'] = $customer->id;
        return CustomerPermission::create($defaultPermissions);
    }

    public function updatePermisions($data, $customerId) 
    {
        return CustomerPermission::find($customerId)
            ->update($data);
    }
    
    public function updateOrCreatePermisions($data, $customerId) 
    {
        return CustomerPermission::updateOrCreate(
            ['customer_id'=> $customerId],
            $data
        );
    }

    public function createActivationToken ($customerId, $company) 
    {
        $activation_token = base64_encode('join_company_name='.$company->name.'&user_id='.$customerId);
        $customer = Customer::find($customerId);
        $customer->activation_token =  $activation_token;
        $customer->save();
        return $activation_token;
    }

    public function getCompanyUsers($userId, $select = "*")
    {
        return Customer::where('customers.id', '=', $userId)
                ->join('customers as c', function ($customers) {
                    $customers->on('c.company_id', '=', 'customers.company_id')
                        ->where('c.company_id', '<>', '0');
                })
                // ->toSql();
                ->get([$select]);
    }

    /**
     * Get all user from company 
     *
     * @param [integer] $userId
     * @return array $users
     */
    public function getManagementCustomers ($userId, $shopID) {
        $customer = Customer::where('id', '=', $userId)
                ->where('shop_id', $shopID)
                ->first(['id', 'company_id']);
        
        $query = Company::where('companies.id', '=', $customer->company_id)
            ->where('companies.shop_id', $shopID)
            ->join('customers', function ($customers) use ($customer, $shopID) {
                $customers->on('companies.id', '=', 'customers.company_id')
                    ->whereNotIn('customers.id', [$customer->id])
                    ->where('customers.shop_id', $shopID)
                    ->leftJoin('customer_permissions', function ($permission) {
                        $permission->on('customers.id', '=', 'customer_permissions.customer_id');
                    });
            })
            ->orderBy('customers.created_at', 'desc');

        //  \Log::debug(print_r($query->toSql(), true));
        return $query->get(['name as company_name', 'customers.*', 
                'customer_permissions.*', \DB::raw('DATE_FORMAT(customers.created_at, "%m/%d/%Y") as formated_created_at'), 
                \DB::raw('DATE_FORMAT(customers.updated_at, "%m/%d/%Y") as formated_updated_at')
            ]);
    }
    
    public function getManagementCustomer ($userID, $shopID) {
        return Customer::where('customers.id', $userID)
            ->where('customers.shop_id', $shopID)
            ->join('customer_permissions', 'customer_permissions.customer_id', '=', 'customers.id')
            ->join('companies', 'customers.company_id', '=', 'companies.id')
            ->first(['companies.name as company_name', 'customers.*', 
                'customer_permissions.*', \DB::raw('DATE_FORMAT(customers.created_at, "%m/%d/%Y") as formated_created_at'), 
                \DB::raw('DATE_FORMAT(customers.updated_at, "%m/%d/%Y") as formated_updated_at')
            ]);
    }
}