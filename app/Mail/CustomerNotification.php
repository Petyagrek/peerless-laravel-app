<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * $data variable
     *
     * @var $data
     */
    public $data;

    /**
     * Shopify $shop object
     * 
     * @var $shop
     */
    public $shop;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $shop)
    {
        $this->data = $data;
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // \Log::debug(print_r($this->data, true));

        return $this->from('noreply@peerless-av.com')
                    ->subject($this->data['subject'])
                    // ->bcc(['ivanchuda@gmail.com, vitalikmihaylishin@gmail.com'])
                    ->markdown($this->data['template']);
                    // ->markdown('vendor.mail.html.invite');
    }
}
