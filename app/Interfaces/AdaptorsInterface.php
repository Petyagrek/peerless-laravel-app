<?php
namespace App\Interfaces;

interface AdaptorsInterface
{
    public function createFromCsvArr($items);
}
