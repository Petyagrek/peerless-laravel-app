<?php
namespace App\Interfaces;

interface VesaInterface
{
    public function search($query);
    public function createFromCsvArr($items);
}
