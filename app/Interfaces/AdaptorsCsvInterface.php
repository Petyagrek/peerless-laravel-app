<?php
namespace App\Interfaces;

interface AdaptorsCsvInterface
{
    public function set_file($file_path);
    public function set_base64_data($base64_data);
    public function to_raw_array();
    public function as_array();
}