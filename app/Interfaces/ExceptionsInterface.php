<?php
namespace App\Interfaces;

interface ExceptionsInterface
{
    public function createFromCsvArr($items);
}
