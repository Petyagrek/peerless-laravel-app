<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //protected $primaryKey = 'product_id';
    protected $fillable = ['product_id', 'handle', 'specification_id', 'value', 'lang', 'shop_id', 'position'];
}
