<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonitorMounts extends Model
{
    use Traits\MountsScopeTrait;

    protected $fillable = ['display_manufacturer', 'display_part_number', 'display_name', 'product_handle', 'exception_enabled'];

    public function exceptions()
    {
        return $this->hasMany(MountsException::class, 'product_handle', 'product_handle');
    }

    public function adaptors()
    {
        return $this->hasMany(MountAdapters::class, 'product_handle', 'product_handle');
    }

    public function adaptorsByDisplay()
    {
        return $this->hasMany(MountAdapters::class, 'display_name', 'display_name');
    }

}
 