<?php
namespace App\Models\Traits;


trait MountsScopeTrait {

    public function variant () {
        return $this->hasOne(\App\Models\Variant::class, 'sku', 'product_handle')->where('shop_id', '=', env('MAIN_SHOP_ID'));
    }

    public function scopeDisplayName($query, $name)
    {
        return $query->where('display_name', $name);
    }

    public function scopeActive($query)
    {
        return $query->where('exception_enabled', false);
        // return $query->whereDoesntHave('exceptions', function($query) use ($name) {
        //     $query->where('display_name', $name);
        // });
    }

    public function scopeActiveQuery($query, $name)
    {
        return $query->whereDoesntHave('exceptions', function($query) use ($name) {
            $query->select('id')->where('display_name', 'like', '%'.$name.'%')->where('status', 1);
        });
    }

    public function scopeUniqByMount($query)
    {
        return $query->groupBy('product_handle');
    }

    public function scopeUniqByDisplayName($query)
    {
        return $query->groupBy('display_name');
    }

}