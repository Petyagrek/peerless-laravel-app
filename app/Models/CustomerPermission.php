<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPermission extends Model
{
    protected $primaryKey = 'customer_id';

    protected $fillable = ['customer_id', 'company_admin', 'purchasing',
        'reviews', 'pricing', 'order_history', 'invoices', 'user_management'];

    public function customer () 
    {
        return $this->belongsTo('App\User', 'customer_id');
    }
}
