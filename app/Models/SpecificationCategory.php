<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecificationCategory extends Model
{
    protected $fillable = ['cat_name', 'lang', 'position', 'shop_id', 'active'];

    public function specification () {
        return $this->hasMany('App\Models\Specification', 'category_id', 'id');
    }
}
