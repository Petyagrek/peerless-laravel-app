<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vesa extends Model
{
    protected $fillable = [ 'product_handle',
                            'minx1', 'maxx1', 'miny1', 'maxy1',
                            'minx2', 'maxx2', 'miny2', 'maxy2',
                            'minx3', 'maxx3', 'miny3', 'maxy3',
                            'minx4', 'maxx4', 'miny4', 'maxy4',
                            'minx5', 'maxx5', 'miny5', 'maxy5',
                            'minx6', 'maxx6', 'miny6', 'maxy6',
                            'minx7', 'maxx7', 'miny7', 'maxy7'
                        ];
    public $timestamps = false;

    public function scopeQueryPair1($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx1', '<=', $x);
            $q->where('maxx1', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny1', '<=', $y);
            $q->where('maxy1', '>=', $y);
        });
    }

    public function scopeQueryPair2($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx2', '<=', $x);
            $q->where('maxx2', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny2', '<=', $y);
            $q->where('maxy2', '>=', $y);
        });
    }

    public function scopeQueryPair3($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx3', '<=', $x);
            $q->where('maxx3', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny3', '<=', $y);
            $q->where('maxy3', '>=', $y);
        });
    }

    public function scopeQueryPair4($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx4', '<=', $x);
            $q->where('maxx4', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny4', '<=', $y);
            $q->where('maxy4', '>=', $y);
        });
    }

    public function scopeQueryPair5($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx5', '<=', $x);
            $q->where('maxx5', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny5', '<=', $y);
            $q->where('maxy5', '>=', $y);
        });
    }

    public function scopeQueryPair6($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx6', '<=', $x);
            $q->where('maxx6', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny6', '<=', $y);
            $q->where('maxy6', '>=', $y);
        });
    }

    public function scopeQueryPair7($query, $x, $y)
    {
        return $query->orWhere(function($q) use ($x) {
            $q->where('minx7', '<=', $x);
            $q->where('maxx7', '>=', $x);
        })->where(function($q) use ($y) {
            $q->where('miny7', '<=', $y);
            $q->where('maxy7', '>=', $y);
        });
    }
}
