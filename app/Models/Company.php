<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'ifs_id', 'shop_id'];

    public function customers () {
        return $this->hasMany("App\Models\Customer", 'company_id', 'id');
    }
}
