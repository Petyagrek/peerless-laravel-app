<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    const CREATED_AT = 'INVOICE_DATE';

    protected $fillable = ["id", "customer_id", "CUSTOMER_INVOICE_HEAD", "INVOICE_NO", "INVOICE_DATE",
        "DUE_DATE", "GROSS_AMOUNT", "COMPANY", "INVOICE_ID", "ORDER_NO", "CUSTOMER_PO_NO",
        "ORDER_DATE", "CUSTOMER_NO", "CUSTOMER_REFERENCE", "OUR_REFERENCE", "TAX_IDENTITY",
        "SHIP_VIA", "FORWARD_AGENT", "DELIVERY_TERMS", "DELIVERY_DATE", "PAYMENT_TERMS", "LABEL_NOTES",
        "DELIVERY_ADDRESS_NO", "INVOICE_ADDRESS_NO", "DELIVERY_ADDRESS", "INVOICE_ADDRESS", "status"
    ];

    public $incrementing = false;


    /// Relation
    public function customer () {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }
}