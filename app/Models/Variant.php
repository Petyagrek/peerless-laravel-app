<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $fillable = ['id', 'product_id', 'variant_id', 'sku', 'shop_id'];

    public $timestamps = false;
}
