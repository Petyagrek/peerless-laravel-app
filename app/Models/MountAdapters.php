<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MountAdapters extends Model
{
    use Traits\MountsScopeTrait;

    protected $fillable = ['adapter_name','product_handle','av_model_display','brand','display_name'];

    public function exceptions()
    {
        return $this->hasMany(MountsException::class, 'product_handle', 'product_handle');
    }
}