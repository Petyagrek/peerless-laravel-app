<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = 'order_history';

    protected $fillable = ['id', 'order', 'customer_id','status', 'meta'];

    public function scopeStatus($query, $value) {
        if (is_array($value)) {
            return $query->whereIn('status', $value);
        } else {
            return $query->where('status', '=', $value);
        }
    }

    public function scopeSearch($query, $value) {
        $query->where(function ($q) use ($value) {
            $q->where(function ($q) use ($value) {
                $q->where('id', 'like', $value . '%');
            })->orWhere(function ($q) use ($value) {
                $q->where('customer_id', 'like', $value . '%');
            })->orWhere(function ($q) use ($value) {
                $q->where('meta', 'like', '%IFSOrderId\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('meta', 'like', '%OurCustomerNumber\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('meta', 'like', '%IFS_PO_number\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('order', 'like', '%sku\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('order', 'like', '%product_name\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('order', 'like', '%product_title\":\"'.$value.'%');
            })->orWhere(function ($q) use ($value) {
                $q->where('order', 'like', '%name\":\"#'.$value.'%');
            });
        });
    }

    /**
     * Customer relation
     * 
     */
    public function customer() {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }
}
