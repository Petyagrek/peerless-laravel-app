<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MountsException extends Model
{
    protected $fillable = [
        'product_handle', 'display_manufacturer',
        'display_part_number', 'display_name', 'status'
    ];
    public $timestamps = false;

    public function monitor_mounts()
    {
        return $this->hasMany(MonitorMounts::class, 'product_handle', 'product_handle');
    }
}
