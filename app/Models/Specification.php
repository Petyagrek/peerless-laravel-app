<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $fillable = ['name', 'default_value', 'lang', 'category_id', 'shop_id', 'active', 'position'];

    public function category()
    {
        return $this->hasOne('App\Models\SpecificationCategory', 'id', 'category_id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'specification_id', 'id');
    }
}