<?php

namespace App\Models;

use BaoPham\DynamoDb\DynamoDbModel;

class AWSOrder extends DynamoDbModel
{
    protected $table = "shopify_order";
}