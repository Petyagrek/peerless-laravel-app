<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['id', 'first_name',
        'last_name', 'email', 'phone', 'state', 'tags',
        'account_type', 'phone_pin', 'company_id',
        'industry', 'country', 'language', 'company_owner', 'activation_token', 'shop_id'
    ];

    public function company () {
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }

    public function customer_permission () {
        return $this->hasOne('App\Models\CustomerPermission', 'customer_id', 'id');
    }

    public function order_history () {
        return $this->hasOne('App\Models\OrderHistory', 'customer_id', 'id');
    }

    //---------------  Scopes -----------------//

    /**
     * Search in account type scopes
     * 
     * @param Illuminate\Database\Eloquent\Model $query
     * @param array $typesArr
     * 
     * @return void
     */
    public function scopeAccountType($query, $typesArr)
    {
        $query->whereIn('account_type', $typesArr);
    }

    public function scopeAdminSearch($query, $searchValue)
    {
        $query->where(function ($q) use ($searchValue) {
            $q->where('first_name', 'like', '%'.$searchValue.'%')
            ->orWhere('last_name', 'like', '%'.$searchValue.'%')
            ->orWhere('email', 'like', '%'.$searchValue.'%')
            ->orWhere('account_type', 'like', '%'.$searchValue.'%');
        });
    }
}
