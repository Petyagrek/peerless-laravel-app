<?php

namespace App\Helpers;

class States {
    const Disabled = 'disabled';
    const Invited  = 'invited';
    const Enabled  = 'enabled';
    const Declined = 'declined';
}

?>