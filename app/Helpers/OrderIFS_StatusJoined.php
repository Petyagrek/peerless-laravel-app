<?php

namespace App\Helpers;

class OrderIFS_StatusJoined extends OrderIFS_Status {
    const Canceled = [
        'statuses' => ['Canceled'],
        'key' => 'canceled',
        'name' => 'Canceled',
    ];

    const OrderCreated = [
        'statuses' => [self::Planned, self::Released],
        'key' => 'order_created',
        'name' => 'Order Created',
    ];
    const InProcessed  = [
        'statuses' => [self::Reserved, self::Picked],
        'key' => 'in_process',
        'name' => 'In Processed',
    ];
    const DeliveredArr = [
        'statuses' => [self::Delivered, self::InvoicedClosed],
        'key' => 'delivered_arr',
        'name' => 'Delivered',
    ];
}