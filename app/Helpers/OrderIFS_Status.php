<?php

namespace App\Helpers;

class OrderIFS_Status {
    // IFS statuses
    const Released       = 'Released'; 
    const Planned        = 'Planned';
    const Reserved       = 'Reserved';
    const Picked         = 'Picked';
    const Delivered      = 'Delivered';
    const InvoicedClosed = 'Invoiced/Closed';
}