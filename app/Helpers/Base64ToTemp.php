<?php
namespace App\Helpers;

class Base64ToTemp {

    private $fl_reg_ex = ['csv' => 'data:text/csv;base64,'];
    private $base64_data;
    private $format;


    function __construct($base64_data, $format = 'csv')
    {
        $this->format = $format;
        $this->base64_data = $this->parse_base64_data($base64_data);
    }

    private function parse_base64_data($base64_csv_data)
    {
        $parsed_b64_csv = str_replace($this->fl_reg_ex[$this->format], '', $base64_csv_data);
        return base64_decode(str_replace(' ', '+', $parsed_b64_csv));
    }


    public function get_b64_content()
    {
        $temp = fopen('php://temp', 'r+');
        fputs($temp, $this->base64_data);
        rewind($temp);
        return $temp;
    }
}