<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ShopifyAPIServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $shop = $this->app->request->shop;
        $this->app->bind('shop', function($app) {
            return \ShopifyApp::shop($app->request->header('shop'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
