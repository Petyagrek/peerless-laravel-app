<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\VesaCsvService;
use App\Services\VesaService;
use App\Services\MountsCsvService;
use App\Services\MountsService;
use App\Services\AdaptorsCsvService;
use App\Services\AdaptorsService;
use App\Services\ExceptionsCsvService;
use App\Services\ExceptionsService;

class VesaCsvProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Interfaces\VesaCsvInterface', function($app) {
            return new VesaCsvService();
        });
        $this->app->bind('App\Interfaces\VesaInterface', function($app) {
            return new VesaService();
        });

        $this->app->bind('App\Interfaces\MountsCsvInterface', function($app) {
            return new MountsCsvService();
        });
        $this->app->bind('App\Interfaces\MountsInterface', function($app) {
            return new MountsService();
        });

        $this->app->bind('App\Interfaces\AdaptorsCsvInterface', function($app) {
            return new AdaptorsCsvService();
        });
        $this->app->bind('App\Interfaces\AdaptorsInterface', function($app) {
            return new AdaptorsService();
        });

        $this->app->bind('App\Interfaces\ExceptionsCsvInterface', function($app) {
            return new ExceptionsCsvService();
        });
        $this->app->bind('App\Interfaces\ExceptionsInterface', function($app) {
            return new ExceptionsService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
