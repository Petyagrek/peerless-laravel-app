<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AdminShopifyCustomer;
use App\Services\ShopifyCustomerApi;
use App\Services\CompanyService;

class AdminCustomerController extends Controller
{
    protected $shop;

    private $adminShopifyCustomer;

    protected function setShop($request)
    {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    public function __construct(Request $request)
    {
        $this->setShop($request);
        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
        $this->adminShopifyCustomer = new AdminShopifyCustomer($this->shop);
        $this->shopifyCustomerApi = new ShopifyCustomerApi($this->shop->api());
        $this->companyService = new CompanyService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->adminShopifyCustomer->getCustomers(
            $request->input('searchValue'), $this->shop->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companyRequest = false;
        $companyMeta = (object)[];
        $customer = $this->adminShopifyCustomer->getCustomerByID($id, $this->shop->id);
        if (!$customer) {
            return [
                'customer'       => $customer,
                'companyRequest' => $companyRequest,
                'companyMeta'    => $companyMeta,
            ];
        }
        $shopifyCustomer = $this->shopifyCustomerApi->get($id);
        $customer['accepts_marketing'] = $shopifyCustomer->accepts_marketing;

        $companyMeta = $this->adminShopifyCustomer->getMeta($id, 'company');

        // check if user want to join company
        if (!$customer->company_id && $customer->account_type != 'B2C') {
            $companyRequest = true;
        }

        return [
            'customer'       => $customer,
            'companyRequest' => $companyRequest,
            'companyMeta'    => $companyMeta,
        ];
    }

    /**
     * Store a newly created customer in storage and Shopify.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = $request->only('customer');
        $customer['customer']['shop_id'] = $this->shop->id;
        return $this->adminShopifyCustomer
            ->superAdminCreateCustomer($customer, $this->shop->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = $request->only('customer');
        $customer['customer']['shop_id'] = $this->shop->id;
        return $this->adminShopifyCustomer
            ->adminUpdateCustomer($customer, $this->shop->id);
    }

    /**
     * Get companies for customer select
     * 
     * @return array \App\Models\Company
     */
    public function customerCompanies()
    {
        return $this->companyService->getCustomerCompanies($this->shop->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->adminShopifyCustomer->destroy(
            $request->input('items')
        );
    }

    private function debug ($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}
