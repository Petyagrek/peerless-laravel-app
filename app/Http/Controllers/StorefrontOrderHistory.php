<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\OrderIFS_StatusJoined;
use App\Models\AWSOrder;

class StorefrontOrderHistory extends Controller
{
    private $orders_keys = [
        'userId', 
        'OrderHistoryPermission', 
        'perPage', 
        'offset', 
        'searchStartDate', 
        'searchEndDate',
        'searchInputValue',
        'search',
        'status'
    ];

    private $orderReturnKeys = ['ordersToReturn', 'reason', 'orderID', 'userId', 'shop'];

    public function __construct (Request $request)
    {
        $this->shop = \ShopifyApp::shop($request->shop);
        $this->shopifyOrder = new \App\Services\ShopifyOrder($this->shop);
        $this->shopifyProduct = new \App\Services\ShopifyProduct($this->shop->api());
    }

    public function aws ()
    {

        $query = AWSOrder::all();
        // $query = AWSOrder::whereIn('shopify_order_id', ['922248839283', '916354170995'])->limit(2)->get();
        // try {
        //     $query = AWSOrder::where('shopify_order_id', '922248839283')->all();
        // } catch (\Exception $e) {
        //     // dd($e->getMessage());
        // }

        // dd($query);
        return $query;
        // $this->shopifyOrder->aws();
    }

    /**
     * Display listing of orders
     * 
     * @return \Illuminate\Http\Response
     */
    public function index (Request $request) {
        return [
            'allOrders' => $this->shopifyOrder->getOrders(
                $request->only($this->orders_keys)
            ),
            'allCount' => $this->shopifyOrder->count(
                $request->only($this->orders_keys)
            ),
            OrderIFS_StatusJoined::Canceled['key'] => $this->shopifyOrder->count(
                array_merge($request->only($this->orders_keys), ['status' => OrderIFS_StatusJoined::Canceled['statuses']])
            ),
            OrderIFS_StatusJoined::OrderCreated['key'] => $this->shopifyOrder->count(
                array_merge($request->only($this->orders_keys), ['status' => OrderIFS_StatusJoined::OrderCreated['statuses']])
            ),
            OrderIFS_StatusJoined::InProcessed['key'] => $this->shopifyOrder->count(
                array_merge($request->only($this->orders_keys), ['status' => OrderIFS_StatusJoined::InProcessed['statuses']])
            ),
            OrderIFS_StatusJoined::DeliveredArr['key'] => $this->shopifyOrder->count(
                array_merge($request->only($this->orders_keys), ['status' => OrderIFS_StatusJoined::DeliveredArr['statuses']])
            ),
            'firstCreatedAt' => $this->shopifyOrder->firstCreatedAt(
                $request->only($this->orders_keys)
            )
        ];
    }

    public function getInvoice(Request $request,$orderID) {
        $invoiceID = !empty($request->input('invoiceID')) ? $request->input('invoiceID') : null;
        return $this->shopifyOrder->getInvoice(
            $orderID, $request->input('ifsUserID'), $request->input('companyId'), $invoiceID
        );
    }

    public function getOrder(Request $request, $orderId) 
    {
        return $this->shopifyOrder->getOrder(
            array_merge($request->only($this->orders_keys), ['orderId' => $orderId])
        );
    }

    /**
     * Cancel order
     * 
     * @return \Illuminate\Http\Response
     */
    public function cancel (Request $request, $id) {
        return $this->shopifyOrder->cancel($request->id);
    }

    public function orderReturnRequest (Request $request)
    {
        return $this->shopifyOrder->orderReturnRequest(
            $request->only($this->orderReturnKeys)
        );
    }

    // Import all orders
    public function importOrders (Request $request)
    {
        $orders =  $this->shopifyOrder->get(null, [
            "limit" => 250,
            "status" => "any",
            "financial_status" => "any",
            "fulfillment_status" => "any",
        ]);
        $ordersCollection = collect($orders);

        if ($ordersCollection->count()) {
            $ordersCollection->each(function ($order) {
                if (!isset($order->customer)) return;
                $this->shopifyProduct->getAdditianalProductData($order);
                $this->shopifyProduct->renameOrderProductKeys($order, ['name' => 'product_name', 'title' => 'product_title']);
                $order->meta = $this->shopifyOrder->getIFSOrderData($order);
                $this->shopifyOrder->updateOrCreate($order);
            });
        }
        return ['message' => 'done'];
    }
}
