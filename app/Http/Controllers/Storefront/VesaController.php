<?php

namespace App\Http\Controllers\Storefront;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Interfaces\VesaInterface;

class VesaController extends Controller
{

    const SEARCH_KEYS = [
        'x', 'y'
    ];

    private $vesaService;

    function __construct(VesaInterface $vesaService)
    {
        $this->vesaService = $vesaService;
    }

    public function index(Request $request)
    {
        $items = $this->vesaService->search(
            $request->only(self::SEARCH_KEYS)
        );
        $uniquItems = $items->pluck('variant_id')->unique();

        return response()->json([
            'items' => array_values($uniquItems->toArray()),
            'total' => $uniquItems->count()
        ], 200);
    }
}
