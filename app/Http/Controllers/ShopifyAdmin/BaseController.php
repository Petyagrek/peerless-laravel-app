<?php
namespace App\Http\Controllers\ShopifyAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    protected $shop;

    public function __construct()
    {
        $this->shop = app('shop');
    }
}
