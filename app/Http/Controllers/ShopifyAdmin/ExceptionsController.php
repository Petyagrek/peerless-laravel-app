<?php
namespace App\Http\Controllers\ShopifyAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\ShopifyAdmin\VesaRequest;
use App\Jobs\ProcessExceptionsCsv;

class ExceptionsController extends BaseController
{

    public function upload(VesaRequest $request)
    {
        set_time_limit(0);
        ProcessExceptionsCsv::dispatch($request->file->path());
        return response()->json([
            'error' => false,
            'message' => "Exceptions import finished.",
        ], 200);
    }
}
