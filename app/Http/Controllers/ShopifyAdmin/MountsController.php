<?php
namespace App\Http\Controllers\ShopifyAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\ShopifyAdmin\VesaRequest;
use App\Jobs\ProcessMountsCsv;

class MountsController extends BaseController
{

    public function upload(VesaRequest $request)
    {
        set_time_limit(0);
        ProcessMountsCsv::dispatch($request->file->path());
        return response()->json([
            'error' => false,
            'message' => "Mounts import finished.",
        ], 200);
    }
}
