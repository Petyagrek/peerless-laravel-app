<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SyncController extends Controller
{
  public function __construct(Request $request)
  {
    $this->shop = \ShopifyApp::shop($request->shop);
  }

  public function langifySync (Request $request)
  {
    ini_set('max_execution_time', 0);

    if ((int)$this->shop->id == (int)env('MAIN_SHOP_ID')) {
      return response()->json(['succes' => false, 'message' => 'No need to copy metafield to main site']);
    }

    \Artisan::call('langifySync', [
      'shopID' => $this->shop->id
    ]); //->onConnection('database')->onQueue('commands');

    return response()->json([
      'success' => true,
      'message' => 'You will be notified when job is done.'
    ]);
  }
}