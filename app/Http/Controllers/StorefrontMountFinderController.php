<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorefrontMountFinderController extends Controller
{
    private $searchKeys = ['query', 'display_manufacturer', 'display_part_number',  'limit'];

    private $adaptorsKeys = ['display_name', 'product_handle'];

    public function __construct()
    {
        $this->mountFinderService = new \App\Services\MountFinderService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mounts = $this->mountFinderService->get(
            $request->only($this->searchKeys)
        );
        return [
            'items' => $mounts,
            'count' => $mounts->count(),
        ];
    }

    public function getProductByMountName(Request $request)
    {
        return $this->mountFinderService->getProductByMountName(
            $request->input('query'), $request->input('adaptersOnly')
        );
    }

    /**
     * Dispalay products by mount data
     * 
     * @return \Illuminate\Http\Response ['items']
     */
    // public function searchProductByMount (Request $request)
    // {
    //     return [
    //         'items' => $this->mountFinderService->get(
    //             $request->only($this->searchKeys)
    //         )
    //     ];
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adapterSearch(Request $request)
    {
        $display_manufacturer = $request->input('display_manufacturer');
        $display_part_number = $request->input('display_part_number');

        // $mountsException = $this->mountFinderService->getMountException(
        //     array_merge($request->only($this->adaptorsKeys), ['limit' => 1])
        // );

        // if ($mountsException->count()) {
        //     return response()->json([
        //         'message' => $display_manufacturer . " model “" . $display_part_number . "” is not compatible with this mount",
        //         'success' => false,
        //         'adaptor_needed' => false,
        //     ], 404);
        // }

        $mounts = $this->mountFinderService->get(
            array_merge($request->only($this->adaptorsKeys), ['limit' => 1])
        );
        if ($mounts->count()) {
            return [
                'message' => $display_manufacturer . " model “" . $display_part_number . "” is compatible with this mount",
                'success' => true,
                'items' => [],
                'total' => 0,
                'adaptor_needed' => false,
            ];
        }

        $adaptors = $this->mountFinderService->getAdaptors(
            $request->only($this->adaptorsKeys)
        );

        if ($adaptors['count']) {
            return [
                'items' => $adaptors['items'],
                'count' => $adaptors['count'],
                'adaptor_needed' => true,
                'success' => true,
                'message' => $display_manufacturer . " model “" . $display_part_number . "” needs an adapter. Compatible adapters should display, with the following information:",
            ];
        } else {
            return response()->json([
                'message' => $display_manufacturer . " model “" . $display_part_number . "” is not compatible with this mount",
                'success' => false,
                'adaptor_needed' => false,
            ], 404);
        }
    }

    public function monitorMountsImport(Request $request)
    {
        \Log::debug('monitor mounts import start');
        $data = $request->input('data');

        if (!empty($data)) {
            \App\Models\MonitorMounts::insert($data);

            //foreach ($data as $monitorMoutObject) {
            // $monitorMoutRecord = \App\Models\MonitorMounts::updateOrCreate(
            //     [
            //         'display_part_number' => $monitorMoutObject['display_part_number'],
            //         'product_handle' => $monitorMoutObject['product_handle'],
            //     ],
            //     [
            //         'display_manufacturer' => $monitorMoutObject['display_manufacturer'],
            //         'exception_enabled' => $monitorMoutObject['exception_enabled'],
            //     ]
            // );

            // $monitorMoutRecord = \App\Models\MonitorMounts::create(
            //     [
            //         'display_part_number' => $monitorMoutObject['display_part_number'],
            //         'display_name' => $monitorMoutObject['display_name'],
            //         'product_handle' => $monitorMoutObject['product_handle'],
            //         'display_manufacturer' => $monitorMoutObject['display_manufacturer'],
            //         'exception_enabled' => $monitorMoutObject['exception_enabled'],
            //     ]
            // );

            // $monitorMoutRecord->save();
            //}
        }
        \Log::debug('monitor mounts import end');
    }

    public function mountAdaptorsImport(Request $request)
    {
        \Log::debug('mount adapters import start');

        $data = $request->input('data');

        if (!empty($data)) {
            \App\Models\MountAdapters::insert($data);
        }

        \Log::debug('mount adapter import end');
    }

    public function mountExceptionsImport(Request $request)
    {
        \Log::debug('mount exceptions import start');

        $data = $request->input('data');

        if (!empty($data)) {
            \App\Models\MountsException::insert($data);
        }

        \Log::debug('mount exceptions import end');
    }
}
