<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NotificationService;

class StorefrontContactController extends Controller
{
  private $reserve_keys = ["first_name", "last_name", "email", "phone", 
    "title", "company", "city", "state", "country", "trainees", "location", "other", "lang"
  ];

  private $book_keys = ["first_name", "last_name", "title", "company", "meeting_purpose",
    "email", "phone", "city", "state", "country", "meeting_purpose_other",
    "notes", "prefered_time", "tradeShowLink", "industry", "site", "lang"
  ];

  private $customers_solutions_keys = ["body", "company",  "email", 
    "first_name", "last_name", "project_title", "site", "lang", "lang"
  ];

  private $hospitality_keys = ["first_name", "last_name", "email",
    "phone", "job_title", "company", "message", "site", "lang"
  ];

  private $literature_request_keys = [ "literature", "form_type", "first_name",
    "last_name", "title", "company", "industry", "phone",
    "email", "address", "city", "province", "zip", "country", "shop", "site", "lang"
  ];

  public function __construct(Request $request)
  {
    $this->shop = \ShopifyApp::shop($request->shop);
    $this->notificationService = new NotificationService($this->shop);
  }

  public function hospitality (Request $request)
  {
    $mail = $this->notificationService->hospitality(
      $request->only($this->hospitality_keys)
    );

    if ($mail['success']) {
      return [
        'success' => true,
        'message' => 'Thanks for reaching out! Our Hospitality team will be in touch shortly.'
      ];
    }
    return [
      'success' => false,
      'message' => 'Something went wrong, please reload page and try again'
    ];
  }

  public function reserveCertified(Request $request)
  {
    $customerMail = $this->notificationService->sendReserveCertifiedToCustomer(
      $request->only($this->reserve_keys)
    );
    
    $adminMail = $this->notificationService->sendReserveCertifiedToAdmin(
      $request->only($this->reserve_keys)
    );

    if ($customerMail['success'] && $adminMail['success']) {
      return [
        'success' => true,
        'message' => 'Thanks for you request! Our Training team will be in touch shortly.'
      ];
    }

    return [
      'success' => true,
      'message' => 'Thanks for you request! Our Training team will be in touch shortly.'
    ];
  }

  public function reserveSales (Request $request)
  {
    $customerMail = $this->notificationService->sendReserveSalesToCustomer(
      $request->only($this->reserve_keys)
    );
    
    $adminMail = $this->notificationService->sendReserveSalesToAdmin(
      $request->only($this->reserve_keys)
    );

    if ($customerMail['success'] && $adminMail['success']) {
      return [
        'success' => true,
        'message' => 'Thanks for you request! Our Training team will be in touch shortly.'
      ];
    }

    return [
      'success' => true,
      'message' => 'Thanks for you request! Our Training team will be in touch shortly.'
    ];
  }

  public function tradeShowMeeting (Request $request)
  {
    $customerMail = $this->notificationService->tradeShowMeetingRequestCustomer(
      $request->only($this->book_keys)
    );

    $adminMail = $this->notificationService->tradeShowMeetingRequestAdmin(
      $request->only($this->book_keys)
    );

    if ($customerMail['success'] && $adminMail['success']) {
      return [
        'success' => true,
        'message' => 'Thank You for your request! Our Scheduling Team will be in touch shortly'
      ];
    }

    return [
      'success' => false,
      'message' => 'Something went wrong, please reload page and try again'
    ];
  }

  public function customSolutions (Request $request) 
  {
    $mail = $this->notificationService->customSolutions(
      $request->only($this->customers_solutions_keys)
    );

    if ($mail['success']) {
      return [
        'success' => true,
        'message' => 'Thanks for you request! Our team will be in touch shortly.'
      ];
    }
    return [
      'success' => false,
      'message' => 'Something went wrong, please reload page and try again'
    ];
  }

  public function literatureRequest (Request $request)
  {
    $mail = $this->notificationService->literatureRequest(
      $request->only($this->literature_request_keys)
    );

    if ($mail['success']) {
      return [
        'success' => true,
        'message' => 'Thanks for you request! Our team will be in touch shortly.'
      ];
    }
    return [
      'success' => false,
      'message' => 'Something went wrong, please reload page and try again'
    ];
  }
}