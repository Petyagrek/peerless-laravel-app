<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CustomerManagement;

class StorefrontCustomersHandlerController extends Controller
{
    private $create_keys = [ 'first_name',
        'last_name', 'phone', 'email',
        'password', 'password_confirmation',
        'send_email_welcome', 'language', 'country', 
        'industry', 'company', 'phone_pin', 'tags', 
        'account_type', 'state', 'company_admin',
        'purchasing', 'reviews', 'pricing',
        'order_history', 'invoices', 'user_management',
        'registration_by_user', 'accepts_marketing'
    ];

    private $update_keys = [ 'id', 'first_name', 'email',
        'last_name', 'phone', 'language',
        'country', 'industry', 'phone_pin',
        'company', 'newsletter', 'state'
    ];

    private $permission_key = ["customer_id",
        "company_admin", "purchasing",
        "reviews", "pricing",
        "order_history", "invoices", "user_management",
    ];

    private $aprove_deny_keys = [
        'id', 'state', 'reason', 'company',
    ];

    private $enable_disable_keys = [
        'id', 'state', 'reason', 'company',
    ];

    function __construct(Request $request)
    {
        $this->shop = \ShopifyApp::shop($request->shop);
        $this->shopifyCustomer = new \App\Services\ShopifyCustomer($this->shop);
        $this->customerManagement = new CustomerManagement();
    }

    public function getManagementCustomers (Request $request) {
        return response()->json([
            'customers' => $this->customerManagement->getManagementCustomers($request->userId, $this->shop->id),
        ], 200);
    }

    public function create(Request $request)
    {
        $customerData = $request->only($this->create_keys);
        $customerData['shop_id'] = $this->shop->id;
        // Create customer in Shopify and in app
        return $this->shopifyCustomer->createCustomer($customerData);
    }

    public function adminCreate(Request $request)
    {
        $customerData = $request->only($this->create_keys);
        $customerData['shop_id'] = $this->shop->id;
        // Create customer in Shopify and in app
        return $this->shopifyCustomer->adminCreateCustomer($customerData);
    }

    /**
     * Update customer
     * 
     * @param object \Illuminate\Http\Request
     * @return object \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $customerData = $request->only($this->update_keys);
        return $this->shopifyCustomer->updateCustomer($customerData, $this->shop->id);
    }

    public function changePermissions (Request $request) 
    {
        return $this->shopifyCustomer->changePermissions(
            $request->only($this->permission_key)
        );
    }

    public function approve_deny (Request $request) 
    {
        return $this->shopifyCustomer->approve_deny(
            $request->only($this->aprove_deny_keys), $this->shop->id
        );
    }

    public function enable_disable (Request $request) 
    {
        return $this->shopifyCustomer->enable_disable(
            $request->only($this->enable_disable_keys), $this->shop->id
        );
    }

    public function resendInvite (Request $request)
    {
        return $this->shopifyCustomer->adminResendInvite($request->input('id'));
    }

    public function checkUser (Request $request)
    {
        $data = [
            'query' => "email:" . $request->input('email'),
            'email' => $request->input('email'),
            'company' => $request->input('company'),
            'fields' => 'id',
        ];
        return $this->shopifyCustomer->checkUser($data, $this->shop->id);
    }

    public function joinExistingUser (Request $request)
    {
        return $this->shopifyCustomer->joinExistingUser([
            'newCustomerId' => $request->input('newCustomerId'),
            'companyName' => $request->input('companyName'),
        ], $this->shop->id);
    }

    public function joinUserActivation (Request $request)
    {
        return $this->shopifyCustomer->joinUserActivation($request->input('token'), $this->shop->id);
    }
}
