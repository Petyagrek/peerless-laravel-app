<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SpecificationCategory;
use App\Models\Specification;
use Illuminate\Support\Facades\Redirect;

class SpecificationsController extends Controller
{
    protected $shop;

    private $sites = [
        'peerless-av.myshopify.com' => [
            'id' => 1,
            'product_spec_id' => 5,
            'package_spec_id' => 15,
        ],
        'france-peerless-av.myshopify.com' => [
            'id' => 11,
            'product_spec_id' => 31,
            'package_spec_id' => 41,
        ],
        'germany-peerless-av.myshopify.com' => [
            'id' => 21,
            'product_spec_id' => 51,
            'package_spec_id' => 61,
        ],
        'latin-america-peerless-av.myshopify.com' => [
            'id' => 31,
            'product_spec_id' => 71,
            'package_spec_id' => 81,
        ],
        'uk-peerless-av.myshopify.com' => [
            'id' => 41,
            'product_spec_id' => 91,
            'package_spec_id' => 101,
        ]
    ];

    protected function setShop ($request) {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->setShop($request);
        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $specification = Specification::with('category')->where('shop_id', $this->shop->id);

        if ($request->searchValue) {
            $specification->where("name", "LIKE", "%$request->searchValue%");
        }

        return $specification->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specificationExist = Specification::where([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'shop_id' => $this->shop->id
        ])->get();

        if ($specificationExist->count()) {
            return response()->json([
                'error' => true,
                'message' => "Specification with name=\"$request->name\" already exist",
            ], 200);
        }

        $specification = new Specification;
        $specification->name = $request->name;
        $specification->default_value = $request->default_value;
        $specification->category_id = $request->category_id;
        $specification->shop_id = $this->shop->id;
        $specification->active = 1;
        $specification->save();

        return response()->json([
            'error' => false,
            'message' => "Specification succesfuly created",
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Specification::with('category')
            ->where('id', $id)
            ->where('shop_id', $this->shop->id)
            ->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $specification = Specification::find($request->id);
        $specification->update($request->all());

        return response()->json([
            'error' => false,
            'message' => "Specification succesfuly updated",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // specification Ids
        $items = $request->items;

        Specification::destroy($items);

        // delete releted specification from product table
        $itemsCollection = collect($items);
        $itemsCollection->each(function ($specId) {
            $productRecord = \App\Models\Product::where([
                    'specification_id' => $specId,
                    'shop_id' => $this->shop->id,
                ])->delete();
        });

        response()->json(null, 204);
    }

    public function getCategories() {
        return \App\Models\SpecificationCategory::where('shop_id', $this->shop->id)
                ->select(\DB::raw('cat_name as label, id as value'))
                ->get();
    }

    public function import (Request $request)
    {
        \Log::debug('spec import start');
        \Log::debug(print_r($request->all(), true));

        $product_id = $request->input('product_id');
        $product_handle = $request->input('handle');
        $product_spec = $request->input('product_spec');
        $package_spec = $request->input('package_spec');
        $shopify_domain = $request->input('shopify_domain');

        if (empty($this->sites[$shopify_domain])) return;

        $siteData = $this->sites[$shopify_domain];
        \Log::debug(print_r($siteData, true));

        // exit();

        if (!empty($package_spec)) {
            foreach ($package_spec as $pack_spec) {
                $specification = Specification::firstOrCreate(
                    [
                        'name' => $pack_spec['name'],
                        'category_id' => $siteData['package_spec_id'],
                        'shop_id' => $siteData['id'],
                        'active' => 1
                    ]
                );
                $productRecord = \App\Models\Product::updateOrCreate(
                    [
                        'product_id' => $product_id,
                        'specification_id' => $specification->id, 
                        'shop_id' => $siteData['id'],
                        'handle' => $product_handle,
                    ],
                    [
                        'value' => $pack_spec['value'],
                    ]
                );

                $productRecord->save();
            }
        }

        if (!empty($product_spec)) {
            foreach ($product_spec as $prod_spec) {
                $specification = Specification::firstOrCreate(
                    [
                        'name' => $prod_spec['name'],
                        'category_id' => $siteData['product_spec_id'],
                        'shop_id' => $siteData['id'],
                        'active' => 1
                    ]
                );
                $productRecord = \App\Models\Product::updateOrCreate(
                    [
                        'product_id' => $product_id,
                        'specification_id' => $specification->id, 
                        'shop_id' => $siteData['id'],
                        'handle' => $product_handle,
                    ],
                    [
                        'value' => $prod_spec['value'],
                    ]
                );

                $productRecord->save();
            }
        }

        \Log::debug('spec import end');
    }
}