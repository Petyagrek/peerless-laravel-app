<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CarrierServiceController extends Controller
{
  private $api;
  private const API_URL = '/admin/api/2020-01/carrier_services';

  public function __construct()
  {
    # price Ranges
    $this->thousand           = 100000;
    $this->twoAndHalfThousand = 250000;
    $this->fiveThousand       = 500000;

    # Dealer Freight Program Zones
    $this->freightZone_1_Arr = ["IL", "IN", "IA", "MI", "MN", "MO", "WI"];
    $this->freightZone_2_Arr = ["AL", "AR", "AZ", "CA", "CO", "CT", "GA", "ID", "KS", "KY", "LA", "MS", "MT", "NE", "NJ", "NM", "NY", "ND", "NV", "OH", "OR", "OK", "PA", "SD", "TN", "TX", "UT", "WA", "WY"];
    $this->freightZone_3_Arr = ["DE", "DC", "FL", "ME", "MD", "MA", "NH", "NC", "RI", "SC", "VT", "VA", "WV"];
  }

  public function index()
  {
    // log the raw request -- this makes debugging much easier
    $input = file_get_contents('php://input');

    // parse the request
    $rates = json_decode($input, true);
    $destination = $rates['rate']['destination'];
    $cartItems = $rates['rate']['items'];
    $provinceCode = $destination['province'];

    $subtotalPriceCents = $this->calcSubTotal($cartItems);
    $subtotalPriceOnePercent = $subtotalPriceCents / 100;
    
    $rate = $this->getRateByZone($subtotalPriceCents, $subtotalPriceOnePercent, $provinceCode);

    // build the array of line items using the prior values
    $output = (object) array('rates' => array(
      (object) array(
        'service_name' => 'Dealer Freight Program',
        'service_code' => 'DFP',
        'total_price' => (string)$rate,
        // 'description' => 'test',
        'currency' => 'USD'
      ),
    ));

    // send it back to shopify
    return response()->json($output);
  }

  private function calcSubTotal($cartItems)
  {
    $subtotalPriceCents = 0;
    foreach ($cartItems as $items) {
      $subtotalPriceCents += $items['price'] * $items['quantity'];
    }
    return $subtotalPriceCents;
  }

  public function calcRate(
    $subtotalPriceCents, 
    $subtotalPriceOnePercent, 
    $firstRangePercent, 
    $secondRangePercent, 
    $thirdRangePercent, 
    $fourthRangePercent
  )
  {
    if ($subtotalPriceCents < $this->thousand)
      $rate = $subtotalPriceOnePercent * $firstRangePercent;

    elseif ($this->thousand && $subtotalPriceCents < $this->twoAndHalfThousand)
      $rate = $subtotalPriceOnePercent * $secondRangePercent;

    elseif ($subtotalPriceCents > $this->twoAndHalfThousand && $subtotalPriceCents < $this->fiveThousand)
      $rate = $subtotalPriceOnePercent * $thirdRangePercent;

    else
      $rate = $subtotalPriceOnePercent * $fourthRangePercent;

    return $rate;
  }

  public function getRateByZone($subtotalPriceCents, $subtotalPriceOnePercent, $provinceCode)
  {
    if (in_array($provinceCode, $this->freightZone_1_Arr))
     $rate = $this->calcRate($subtotalPriceCents, $subtotalPriceOnePercent, 10, 8, 6, 5);

    else if (in_array($provinceCode, $this->freightZone_2_Arr))
      $rate = $this->calcRate($subtotalPriceCents, $subtotalPriceOnePercent, 12, 10, 8, 6);

    else if (in_array($provinceCode, $this->freightZone_3_Arr))
      $rate = $this->calcRate($subtotalPriceCents, $subtotalPriceOnePercent, 13, 12, 10, 8);

    return $rate;
  }

  public function create()
  {
    $this->shop = app('shop');
    $this->api = $this->shop->api();

    $data = [
      "name" => "shipping_dealer_program",
      // "callback_url" => str_replace('https', 'http', route('carrier_shipping')),
      "callback_url" => route('carrier_shipping'),
      "service_discovery" => true,
      "active" => true,
    ];

    // dd($data);

    try {
      $url = self::API_URL . '.json';

      $resp = [
        'data' => $this->api->rest('POST', $url, ['carrier_service' => $data])->body->carrier_service,
        'success' => true
      ];
      \Log::info(print_r($resp, true));
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      $resp = [
        'error' => json_decode($e->getResponse()->getBody()->getContents())->errors,
        'success' => false
      ];
    }

    return $resp;
  }

  public function delete()
  {
    $this->shop = app('shop');
    $this->api = $this->shop->api();

    try {
      $url = self::API_URL . '/28372435059.json';

      $resp = $this->api->rest('DELETE', $url)->body;
      \Log::info(print_r($resp, true));
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      $resp = [
        'error' => json_decode($e->getResponse()->getBody()->getContents())->errors,
        'success' => false
      ];
    }

    return $resp;
  }
}
