<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class StorefrontProductSpecificationController extends Controller
{
    protected $shop;

    protected $copmareOrder;

    protected function setShop ($request) {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    protected $defaultLanguage = 'en';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->setShop($request);
        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $handle
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $handle = null)
    {
        if (!$handle) {
            return response()->json(['error' => true, 'message' => 'Please provide handle']);
        }
        $lang = $this->getLanguage($request->input('lang'));

        $this->haveSpecification = false;
        $specCats = $this->getSpecToProductPage($handle, $lang);
        if (!$this->haveSpecification && $lang != $this->defaultLanguage) // try to find $this->defaultLanguage spec
            $specCats = $this->getSpecToProductPage($handle, $this->defaultLanguage);

        $specCats = $specCats->toArray();
        foreach ($specCats as &$specCat) {
            $specCat['specification'] = collect($specCat['specification'])->keyBy('position');
        }

        return response()->json([
            'specCats' => $specCats,
        ], 200);
    }

    public function compare(Request $request) 
    {
        $products = $request->products;
        $products = explode(',', $products);

        $productsHandleArr = [];
        $productResp = [];
        foreach ($products as $_handle_product) {
            $productHandle = explode('_sp_', $_handle_product);
            $productsHandleArr[] = [
                'handle' => $productHandle[0],
                'id' => $productHandle[1],
            ];
        }

        $lang = $this->getLanguage($request->input('lang'));

        $specCats = $this->getSpecCats($lang);

        foreach ($productsHandleArr as $key => $prodHandle) {
            $tempSpecCats = clone $specCats;
            $this->haveSpecification = false;
            $productResp[$key] = $this->buildCompareItem($tempSpecCats, $prodHandle['handle'], $prodHandle['id'], $lang);

            // try to find $this->defaultLanguage spec 
            //if (!$this->haveSpecification && $lang != $this->defaultLanguage) {
                //$tempSpecCats = $this->getSpecCats($this->defaultLanguage);
                // $productResp[$key] = $this->buildCompareItem($tempSpecCats, $prodHandle['handle'], $prodHandle['id'], $this->defaultLanguage);
                // dd($productResp[$key]);
                //$productResp[$key] = array_merge($productResp[$key], $this->buildCompareItem($tempSpecCats, $prodHandle['handle'], $prodHandle['id'], $this->defaultLanguage));
            //}
        }

        return response()->json([
            'productResp' => $productResp
        ]);
    }

    private function getSpecCats ($lang) {
        return \App\Models\SpecificationCategory::with(['specification' => function ($query) {
            $query->where('specifications.active', 1)
                    ->orderBy('position');
        }])
        ->where([
            'shop_id' => $this->shop->id,
            'active' => 1,
            'lang' => $lang,
        ])
        ->orderBy('position')
        ->get();
    }

    private function buildCompareItem($specCats, $prodHandle, $prodId, $lang) {
        $resp = [];
        foreach ($specCats as $specKey => $specCat) {
            $tempSpecCat = [];
            foreach ($specCat->specification as $key => $specification) {
                $tempSpecification = clone $specification;
                $tempSpecification = (object) $tempSpecification;
                $productRecord = \App\Models\Product::where([
                    'handle' => $prodHandle,
                    'specification_id' => $tempSpecification['id'], 
                    'shop_id' => $this->shop->id,
                    'lang' => $lang,
                ])->first();

                if (isset($productRecord->value) || !empty($tempSpecification->default_value)) {
                    $tempSpecification->value = isset($productRecord->value) 
                        ? nl2br($productRecord->value) 
                        : nl2br($tempSpecification->default_value);
                    $tempSpecification->position = isset($productRecord->position)
                        ? $productRecord->position
                        : $tempSpecification->position;
                    $this->haveSpecification = true;
                } else {
                    $tempSpecification->value = '';
                }

                $tempSpecification->productHandle = $prodHandle.'_sp_'.$prodId;

                $tempSpecCat['specification'][$key] = $tempSpecification->toArray();
            }
            $tempSpecCat['handle'] = $prodHandle;
            $tempSpecCat['cat_name'] = $specCat->cat_name;

            $resp[$specKey] = $tempSpecCat;
        }

        $resp = $this->sortSpec($resp);

        return $resp;
    }

    /**
     * Sort spectfication by first product
     */
    private function sortSpec ($resp)
    {
        foreach ($resp as &$specCat) {
            $catName = str_slug($specCat['cat_name'], '_');
            if (!empty($this->copmareOrder) && count($this->copmareOrder) >= count($resp) && isset($this->copmareOrder[$catName])) {
                usort($specCat['specification'], function ($a, $b) use ($catName) {
                    $pos_a = array_search($a['id'], $this->copmareOrder[$catName]);
                    $pos_b = array_search($b['id'], $this->copmareOrder[$catName]);
                    return $pos_a - $pos_b;
                });
            } else {
                usort($specCat['specification'], [$this, 'sort']);
                // fill compareOrder by first product
                foreach($specCat['specification'] as $spec) {
                    $this->copmareOrder[$catName][] = $spec['id'];
                }
            }
        }
        return $resp;
    }

    private function sort($a, $b)
    {
        return $a['position'] - $b['position']; 
    }

    private function getSpecToProductPage($handle, $lang) {
        $specCats = $this->getSpecCats($lang);

        $specCats->map(function (&$specCat) use ($handle, $lang) {
            $specCat->specification->each(function ($specification, $key) use ($handle, $specCat, $lang) {
                $productRecord = \App\Models\Product::where([
                    'handle' => $handle,
                    'specification_id' => $specification['id'], 
                    'shop_id' => $this->shop->id,
                    'lang' => $lang,
                ])->first();

                if (isset($productRecord->value) || !empty($specification->default_value)) {
                    $specification->value = isset($productRecord->value) 
                        ? nl2br($productRecord->value) 
                        : nl2br($specification->default_value);

                    $specification->position = isset($productRecord->position)
                        ? $productRecord->position
                        : $specification->position;
                } else {
                    // remove product specification if we do not have value for it in product table
                    $specCat->specification->forget($key);
                }
            });
            if ($specCat->specification->count() && $this->haveSpecification == false) {
                $this->haveSpecification = true;
            }
            return $specCat;
        });

        return $specCats;
    }

    private function getLanguage($lang) {
        $langArr = [
            'US' => 'en',
            'DE' => 'de',
            'ES' => 'es',
            'FR' => 'fr',
        ];
        return isset($langArr[$lang]) ? $langArr[$lang] : $langArr['US'];
    }
}