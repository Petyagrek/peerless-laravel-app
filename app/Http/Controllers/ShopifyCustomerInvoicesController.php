<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ShopifyCustomerInvoicesController extends Controller
{
    private $invoice_keys = [
        'id', 
        'invoicesPermission', 
        'perPage', 
        'offset', 
        'searchStartDate', 
        'searchEndDate',
        'searchInputValue',
        'search',
    ];

    private $invoiceApi;

    function __construct (Request $request) {
        $this->shop = \ShopifyApp::shop($request->shop);
        $this->shopifyInvoices = new \App\Services\ShopifyInvoices($this->shop->api());
        $this->invoiceApi = new \App\Services\InvoiceApi();
    }

    /**
     * Display a listing of the invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $closed_array = ['completed' => true];
        return [
            'invoices' => $this->shopifyInvoices->get(
                $request->only($this->invoice_keys)
            ),
            'allCount' => $this->shopifyInvoices->count(
                $request->only($this->invoice_keys)
            ),
            'firstCreatedAt' => $this->shopifyInvoices->firstCreatedAt(
                $request->only($this->invoice_keys)
            )
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return $this->invoiceApi->getInvoice(2563162, 11, "B2BB2B");
    }

    public function getInvoiceStatusById() {
        return $this->invoiceApi->getInvoiceStatusById('1116750315635');
    }

    /**
     * Sync all invoices from IFS
     * 
     */
    public function sync() {
        return $this->shopifyInvoices->sync();
    }
}
