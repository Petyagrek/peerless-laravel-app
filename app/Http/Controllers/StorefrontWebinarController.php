<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ShopifyWebEx;

class StorefrontWebinarController extends Controller
{
  public function __construct(Request $request)
  {
    $this->shop = \ShopifyApp::shop($request->shop);
    $this->shopifyWebEx = new ShopifyWebEx('peerless-av', 'peerless-av', 'crecchia@peerless-av.com', '1218Jon!#', $this->shop);
    // $this->shopifyWebEx = new ShopifyWebEx('peerless-av', 'peerless-av', 'webinar@peerless-av.com', '1218Jon!#', $this->shop);
  }

  public function getEvents ()
  {
    return $this->shopifyWebEx->getEvents();
  }

  public function getEvent(Request $request, $id)
  {
    return $this->shopifyWebEx->getEvent($id);
  }

  public function getJoinUrlMeeting(Request $request, $id)
  {
    return $this->shopifyWebEx->getJoinUrlMeeting($id, $request->input('attendeeName'));
  }

  public function createMeetingAttendee(Request $request, $id)
  {
    return $this->shopifyWebEx->processMeetingAttendeeInvite($id, $request->input('attendeeEmail'));
  }
}
