<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CompanyService;

class CompanyController extends Controller
{
    protected $shop;

    private $companyService;

    protected function setShop ($request) {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->setShop($request);
        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
        $this->companyService = new CompanyService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->companyService->get(
            $request->only(['searchValue']), $this->shop->id
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->companyService->store(
            $request->only(['name','ifs_id']), $this->shop->id
        );
    }

    /**
     * Display the company resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->companyService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->companyService->update(
            $request->only(['name', 'ifs_id']),
            $id
        );
    }

    /**
     * Remove the company resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // company Ids
        $companyIds = $request->items;
        $this->companyService->destroy($companyIds);
        response()->json(null, 204);
    }
}
