<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Models\Product;
use App\Services\ShopifyProduct;

class ProductsController extends Controller
{
    protected $shop;
    protected $shopifyProduct;

    protected function setShop ($request) {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    /**
     * Create a new controller instance.
     *
    * @return void
     */
    public function __construct(Request $request)
    {
        $this->setShop($request);

        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
        $this->shopifyProduct = new ShopifyProduct($this->shop->api());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index (Request $request)
    {
        $apiUrl = 'admin/api/2020-01/products.json';

        $params = [
            'fields' => "id,images,title,handle"
        ];

        if ($request->productsPerPage)
            $params['limit'] = $request->productsPerPage;

        if ($request->page_info)
            $params['page_info'] = $request->page_info;
        

        if ($request->title)
            $params['title'] = $request->title;

        $products = $this->shop->api()->rest('GET', $apiUrl, $params);

        return response()->json($products, 200);
    }

    public function count (Request $request)
    {
        $productsCount = $this->shop->api()->rest('GET', '/admin/products/count.json');
        return response()->json($productsCount, 200);
    }

    public function product (Request $request, $id)
    {
        $product = $this->shop->api()->rest('GET', "/admin/products/$id.json");

        $specCats = \App\Models\SpecificationCategory::with('specification')
            ->where('shop_id', $this->shop->id)
            ->get();

        $specCats->each(function ($specCat) use ($product) {
            $specCat->specification->each(function ($specification) use ($product) {
                $productRecord = Product::where([
                    'product_id' => $product->body->product->id,
                    'specification_id' => $specification['id'], 
                    'shop_id' => $this->shop->id,
                ])->first();
                $specification->value = isset($productRecord->value) 
                    ? $productRecord->value 
                    : $specification->default_value;
                $specification->position = isset($productRecord->position)
                    ? $productRecord->position
                    : $specification->position;
            });
        });

        $specCats = $specCats->toArray();
        foreach ($specCats as &$specCat) {
            usort($specCat['specification'], [$this, 'sortSpec']);
        }

        return response()->json([
            'product' => $product,
            'specCats' => $specCats,
        ], 200);
    }

    public function productUpdate (Request $request, $id) {
        $product = $request->product;
        $specCats = $request->specCats;

        $this->shopifyProduct->productUpdate($product, $specCats, $this->shop);

        return response()->json([
            'error' => false,
            'message' => "Product specification saved.",
        ], 200);
    }

    private function sortSpec($a, $b) {
        return $a['position'] - $b['position']; 
    }
}
