<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SpecificationCategory;
use Illuminate\Support\Facades\Redirect;


class SpecificationCategoryController extends Controller
{
    protected $shop;

    protected function setShop ($request) {
        $this->shop = \ShopifyApp::shop($request->shop);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->setShop($request);

        if (is_null($this->shop)) {
            return redirect()->to('authenticate')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return SpecificationCategory::where('shop_id', $this->shop->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $catExist = SpecificationCategory::where([
                'cat_name' => $request->cat_name,
                'shop_id' => $this->shop->id,
            ])->get();

        if ($catExist->count()) {
            return response()->json([
                'error' => true,
                'message' => "Category with name=\"$request->cat_name\" already exist",
            ], 200);
        }

        $category = new SpecificationCategory;
        $category->cat_name = $request->cat_name;
        $category->lang = $request->lang;
        $category->position = $request->position;
        $category->shop_id = $this->shop->id;
        $category->active = 1;
        $category->save();

        return response()->json([
            'error' => false,
            'message' => "Category succesfuly created",
        ], 201);
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request, $id)
    {
        return SpecificationCategory::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecificationCategory $category)
    {
        $catExist = SpecificationCategory::where([
            'cat_name' => $request->cat_name,
            'shop_id' => $this->shop->id
        ])->where('id', '<>', $request->id)->get();

        if ($catExist->count()) {
            return response()->json([
                'error' => true,
                'message' => "Category with name=\"$request->cat_name\" already exist",
            ], 200);
        }

        $category = SpecificationCategory::find($request->id);
        $category->update($request->all());

        return response()->json([
            'error' => false,
            'message' => "Category succesfuly updated",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = $request->items;
        SpecificationCategory::destroy($items);

        response()->json(null, 204);
    }
}
