<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorefrontConfiguratorController extends Controller
{
  public function __construct()
  {
    $this->configuratorService = new \App\Services\ConfiguratorService();
  }

  public function configurations(Request $request, $id)
  {
    return $this->configuratorService->configurations($request->all(), $id);
  }

  public function ledConfigurations (Request $request, $id)
  {
    return $this->configuratorService->ledConfigurations($request->all(), $id);
  }

  public function deleteConfigurator (Request $request, $id)
  {
    return $this->configuratorService->deleteConfigurator($request->all(), $id);
  }

  public function ledDeleteConfigurator(Request $request, $id)
  {
    return $this->configuratorService->ledDeleteConfigurator($request->all(), $id);
  }
}