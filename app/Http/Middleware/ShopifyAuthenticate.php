<?php

namespace App\Http\Middleware;

use Closure;

class ShopifyAuthenticate
{
    protected $currentShop;

    protected function setShop($request)
    {
        $this->currentShop = \ShopifyApp::shop($request->header('shop'));
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setShop($request);
        if (is_null($this->currentShop))
            return redirect()->to('authenticate')->send();
        return $next($request);
    }
}
