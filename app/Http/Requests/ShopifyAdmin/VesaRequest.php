<?php

namespace App\Http\Requests\ShopifyAdmin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VesaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'file' => '',
            // 'base64_data' => 'required',
            'file_type' => [
                'required',
                Rule::in(['text/csv'])
            ]
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'base64_data.required' => 'Please provide csv file!',
            'file_type.required' => 'File type is required',
            'file_type.in' => 'Invalid file type.'
        ];
    }
}
