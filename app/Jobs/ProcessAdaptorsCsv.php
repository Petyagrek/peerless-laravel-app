<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Interfaces\AdaptorsCsvInterface;
use App\Interfaces\AdaptorsInterface;

class ProcessAdaptorsCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file_path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_path)
    {
        $this->file_path = $file_path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        AdaptorsCsvInterface $csvService,
        AdaptorsInterface $modelService
    )
    {
        $items = $csvService
                        ->set_file($this->file_path)
                            ->as_array();
        $modelService->createFromCsvArr($items);
    }
}
