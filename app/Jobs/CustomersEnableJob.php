<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\CustomerManagement;

use App\Services\ShopifyCustomerApi;
use App\Services\ShopifyCustomer;
use App\Helpers\States;

class CustomersEnableJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    public $shop;
    private $customerManagement;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $webhook    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
        $this->shop = \ShopifyApp::shop($this->shopDomain);
        $this->customerManagement = new CustomerManagement();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Do what you wish with the data
        \Log::debug('customer enable start');
        \Log::debug(print_r($this->data->id, true));
        $shopifyCustomerApi = new ShopifyCustomerApi($this->shop->api());
        $shopifyCustomer = new ShopifyCustomer($this->shop);
        $metafields = $shopifyCustomerApi->getMeta($this->data->id);

        foreach ($metafields as $metaArr) {
            if ($metaArr['namespace'] == 'global' && $metaArr['key'] == 'state') {
                $newState = States::Enabled;

                $tagsArr = $shopifyCustomer->getStatelessTags($this->data->tags, true);
                $tagsArr[] = $newState;
                $tags = implode(', ', $tagsArr);

                $shopifyCustomer->updateMetafield($metaArr, $newState);

                $this->customerManagement->updateCustomer([
                    'id' => $this->data->id,
                    'state' => $newState,
                    'tags' => $tags,
                ], $this->shop->id);

                try {
                    $shopifyCustomerApi->update([
                        'tags' => $tags,
                    ], $this->data->id);
                } catch (\Exception $e) {}
            }
        }
        // \Log::debug(print_r($metafields, true));
        \Log::debug('customer enable end');
    }
}
