<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\ShopifyVariant;

class ProductsCreateJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Shop's myshopify domain
   *
   * @var string
   */
  public $shopDomain;

  /**
   * The webhook data
   *
   * @var object
   */
  public $data;

  public $shop;

  /**
   * Create a new job instance.
   *
   * @param string $shopDomain The shop's myshopify domain
   * @param object $webhook    The webhook data (JSON decoded)
   *
   * @return void
   */
  public function __construct($shopDomain, $data)
  {
    $this->shopDomain = $shopDomain;
    $this->data = $data;
    $this->shop = \ShopifyApp::shop($this->shopDomain);
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    // Do what you wish with the data
    ShopifyVariant::storeVariants($this->data, $this->shop->id);

    \Log::debug('create product start');
    \Log::debug(print_r($this->data->id, true));
    \Log::debug('create product end');
  }
}
