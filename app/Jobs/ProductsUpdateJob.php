<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\ShopifyProductSync;
use App\Services\ShopifyVariant;

class ProductsUpdateJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Shop's myshopify domain
   *
   * @var string
   */
  public $shopDomain;

  /**
   * Shop instance
   *
   * @var ShopifyApp
   */
  public $shop;

  /**
   * The webhook data
   *
   * @var object
   */
  public $data;

  /**
   * Create a new job instance.
   *
   * @param string $shopDomain The shop's myshopify domain
   * @param object $webhook    The webhook data (JSON decoded)
   *
   * @return void
   */
  public function __construct($shopDomain, $data)
  {
    $this->shopDomain = $shopDomain;
    $this->data = $data;
    $this->shop = \ShopifyApp::shop($this->shopDomain);
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $this->shopifyProductSync = new ShopifyProductSync($this->shop->api());
    $this->shopifyProductSync->getMeta($this->data->id);
    $this->shopifyProductSync->syncFromShopify($this->shop->id, $this->data->handle, $this->data->id);

    ShopifyVariant::storeVariants($this->data, $this->shop->id);

    \Log::debug('update product start');
    \Log::debug(print_r($this->data->id, true));
    // \Log::debug(print_r($this->shopifyProductSync->getMeta($this->data->id), true));
    \Log::debug('update product end');
  }
}
