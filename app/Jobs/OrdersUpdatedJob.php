<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\ShopifyProduct;
use App\Services\ShopifyOrder;

class OrdersUpdatedJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Shop's myshopify domain
   *
   * @var string
   */
  public $shopDomain;

  /**
   * The webhook data
   *
   * @var object
   */
  public $data;
  
  /**
    * Shop instance
    *
    * @var ShopifyApp
    */
  public $shop;

  /**
   * ShopifyProduct instance
   * 
   * @var \App\Services\ShopifyProduct
   */
  public $shopifyProduct;

  /**
   * ShopifyOrder instance
   * 
   * @var \App\Services\ShopifyOrder
   */
  public $shopifyOrder;

  /**
   * Create a new job instance.
   *
   * @param string $shopDomain The shop's myshopify domain
   * @param object $webhook    The webhook data (JSON decoded)
   *
   * @return void
   */
  public function __construct($shopDomain, $data)
  {
    $this->shopDomain = $shopDomain;
    $this->data = $data;
    $this->shop = \ShopifyApp::shop($this->shopDomain);
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    if (!isset($this->data->customer)) return;
    // Do what you wish with the data
    $this->shopifyProduct = new ShopifyProduct($this->shop->api());
    $this->shopifyProduct->getAdditianalProductData($this->data);
    $this->shopifyProduct->renameOrderProductKeys($this->data, ['name' => 'product_name', 'title' => 'product_title']);

    $this->shopifyOrder = new ShopifyOrder($this->shop);
    $this->data->meta = $this->shopifyOrder->getIFSOrderData($this->data);
    \Log::debug('update order start');
    \Log::debug(print_r($this->data->id, true));
    // \Log::debug(print_r($this->shopDomain, true));
    \Log::debug('update order end');

    \App\Services\ShopifyOrder::updateOrCreate($this->data);
  }
}
