<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// use App\Models\Customer;
// use App\Models\CustomerPermission;
use App\Services\CustomerManagement;

use App\Services\ShopifyCustomerApi;
use App\Services\ShopifyCustomer;
use App\Helpers\States;

class CustomersUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;
    public $permisssionData;

    public $shop;
    public $shopifyCustomerApi;
    private $customerManagement;

    private $permissionKeys = [
        'company_admin', 'purchasing', 'reviews', 
        'pricing', 'order_history', 'invoices', 
        'user_management',  
    ];

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $webhook    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = (array)$data;
        $this->shop = \ShopifyApp::shop($this->shopDomain);
        $this->customerManagement = new CustomerManagement();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::debug('update customer start');
        $this->data['shop_id'] = $this->shop->id;

        $shopifyCustomerApi = new ShopifyCustomerApi($this->shop->api());
        $shopifyCustomer = new ShopifyCustomer($this->shop);
        /// get customer meta
        $metafields = $shopifyCustomerApi->getMeta($this->data['id']);

        foreach ($metafields as $metaArr) {
            if ($metaArr['namespace'] == 'global') {
                if (in_array($metaArr['key'], $this->permissionKeys)) {
                    $this->permisssionData[$metaArr['key']] = $metaArr['value'];
                } else if ($metaArr['key'] == 'state') {
                    $tagsArr = $shopifyCustomer->getStatelessTags($this->data['tags'], true);
                    $tagsArr[] = $metaArr['value'];
                    $this->data['tags'] = implode(', ', $tagsArr);
                } else {
                    $this->data[$metaArr['key']] = $metaArr['value'];
                }
            }
        }

        if (!$this->data['last_name']) $this->data['last_name'] = '';

        $this->customerManagement->updateOrCreateCustomer($this->data, $this->data['id']);

        if (!empty($this->permisssionData)) {
            $this->customerManagement
                ->updateOrCreatePermisions($this->permisssionData, $this->data['id']);
        }

        \Log::debug(print_r($this->data['id'], true));
        // \Log::debug(print_r($this->permisssionData, true));
        \Log::debug('--------------- update customer end');
    }
}
