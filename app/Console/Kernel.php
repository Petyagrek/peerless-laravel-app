<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\LoadInvoices::class,
        Commands\UpdataSpecPostion::class,
        Commands\LoadVariants::class,
        Commands\LangifySync::class,
        Commands\WebExInviter::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('load:invoices')->everyFifteenMinutes();

        $schedule->command('updateSpecPosition 1')->daily();
        
        $schedule->command('updateSpecPosition 11')->daily();

        $schedule->command('updateSpecPosition 21')->daily();

        $schedule->command('updateSpecPosition 31')->daily();

        $schedule->command('updateSpecPosition 41')->daily();

        // $schedule->command('webex:invite')->dailyAt('12:01');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
