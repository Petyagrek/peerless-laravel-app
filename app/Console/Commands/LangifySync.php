<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Console\Commands\Traits\ShopifySync;
use App\Services\ShopifyLangifySync;

class LangifySync extends Command
{
    use ShopifySync;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'langifySync {shopID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $shopID;
    protected $shopModel;
    protected $shop;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('sync started');
        $this->findShop();

        if ((int)$this->shopID == (int)env('MAIN_SHOP_ID')) {
            \Log::error('No need to copy metafield to main site');
            $this->error('No need to copy metafield to main site');
            return;
        }

        if (!$this->shopModel) {
            \Log::error("No shop with ID $this->shopID found");
            $this->error("No shop with ID $this->shopID found");
            return;
        }

        $this->setupShop();

        $shopifyLangifySync = new ShopifyLangifySync($this->shop);
        $shopifyLangifySync->sync();

        \Log::info('sync done');
    }
}
