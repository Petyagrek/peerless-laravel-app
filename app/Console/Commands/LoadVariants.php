<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ShopifyProductApi;
use App\Services\ShopifyVariant;

use App\Console\Commands\Traits\ShopifySync;

class LoadVariants extends Command
{
    use ShopifySync;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loadVariants {shopID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load variants and sku from Shopify';

    /**
     * Shopify shop ID
     * 
     * @var id $shopID
     */
    protected $shopID;
    protected $shopModel;
    protected $shop;
    protected $pageInfo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->findShop();

        if (!$this->shopModel) {
            $this->error("No shop with ID $this->shopID found");
            return;
        }

        $this->setupShop();

        $this->shopifyProductApi = new ShopifyProductApi($this->shop->api());
        $productCount = $this->shopifyProductApi->count();

        $this->info("Products count: $productCount");

        $this->bar = $this->output->createProgressBar($productCount);
        $productLimit = 50;

        do {
            $this->processProducts($productLimit);
        } while ($this->pageInfo);

        $this->bar->start();
        $this->bar->finish();

        $this->info("Total spec count $productCount");
    }

    private function processProducts($limit) {
        $args = [
            'fields' => 'variants,id,title',
            'limit'  => $limit
        ];
        if ($this->pageInfo) $args['page_info'] = $this->pageInfo;

        $resp = $this->shopifyProductApi->get(null, $args);
        $products = $resp->body->products;
        $this->pageInfo = $resp->link->next;

        foreach ($products as $product) {
            ShopifyVariant::storeVariants($product, $this->shopID);
            $this->bar->advance();
        }
    }
}
