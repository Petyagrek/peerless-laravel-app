<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ShopifyInvoices;

class LoadInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load invoices from IFS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->shopifyInvoices = new ShopifyInvoices();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoiceDoesNotExist = true;
        $this->shopifyInvoices->sync($invoiceDoesNotExist);
    }
}
