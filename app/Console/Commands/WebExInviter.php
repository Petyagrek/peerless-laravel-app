<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\WebExInviterService;

class WebExInviter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webex:invite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Invites to regiteres attendees';
    protected $webExInviter;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->webExInviter = new WebExInviterService('peerless-av', 'peerless-av', 'webinar@peerless-av.com', '1218Jon!#');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->webExInviter->handleSendInvite();
    }
}
