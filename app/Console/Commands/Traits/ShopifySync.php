<?php

namespace App\Console\Commands\Traits;

use OhMyBrew\ShopifyApp\Models\Shop;
use \OhMyBrew\ShopifyApp\Facades\ShopifyApp;

trait ShopifySync 
{
  private function findShop()
  {
    $this->shopID = $this->argument('shopID');
    $this->shopModel = Shop::find($this->shopID);
  }

  private function setupShop()
  {
    $this->shop = ShopifyApp::shop($this->shopModel->shopify_domain);
  }
}