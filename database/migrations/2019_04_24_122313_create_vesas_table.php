<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesasTable extends Migration
{
    static $range_start = 1;
    static $range_stop = 7;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vesas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_handle');

            foreach(range(self::$range_start, self::$range_stop) as $i) {
                $table->integer('minx'.$i)->default(0);
                $table->integer('miny'.$i)->default(0);
                $table->integer('maxx'.$i)->default(0);
                $table->integer('maxy'.$i)->default(0);
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vesas');
    }
}
