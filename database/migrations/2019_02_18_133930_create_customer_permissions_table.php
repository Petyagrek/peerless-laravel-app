<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_permissions', function (Blueprint $table) {
            $table->bigInteger('customer_id')->unique()->primary();
            $table->boolean('company_admin');
            $table->boolean('purchasing');
            $table->boolean('reviews');
            $table->enum('pricing', ['special_pricing', 'list', 'none']);
            $table->enum('order_history', ['company', 'self', 'none']);
            $table->enum('invoices', ['company', 'self', 'none']);
            $table->boolean('user_management');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_permissions');
    }
}
