<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTableNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('invoices');
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigInteger('id')->comment('Shopify order ID');
            $table->bigInteger('customer_id')->comment('Shopify customer ID');

            $table->string("CUSTOMER_INVOICE_HEAD")->nullable();
            $table->string("INVOICE_NO")->nullable();
            $table->timestamp("INVOICE_DATE")->nullable();
            $table->timestamp("DUE_DATE")->nullable();
            $table->string("GROSS_AMOUNT")->nullable();
            $table->string("COMPANY")->nullable();
            $table->string("INVOICE_ID")->nullable();
            $table->string("ORDER_NO")->nullable();
            $table->string("CUSTOMER_PO_NO")->nullable();
            $table->timestamp("ORDER_DATE")->nullable();
            $table->string("CUSTOMER_NO")->nullable();
            $table->string("CUSTOMER_REFERENCE")->nullable();
            $table->string("OUR_REFERENCE")->nullable();
            $table->string("TAX_IDENTITY")->nullable();
            $table->string("SHIP_VIA")->nullable();
            $table->string("FORWARD_AGENT")->nullable();
            $table->string("DELIVERY_TERMS")->nullable();
            $table->timestamp("DELIVERY_DATE")->nullable();
            $table->string("PAYMENT_TERMS")->nullable();
            $table->string("LABEL_NOTES")->nullable();
            $table->string("DELIVERY_ADDRESS_NO")->nullable();
            $table->string("INVOICE_ADDRESS_NO")->nullable();
            $table->text("DELIVERY_ADDRESS")->nullable();
            $table->text("INVOICE_ADDRESS")->nullable();
            $table->boolean("status")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
