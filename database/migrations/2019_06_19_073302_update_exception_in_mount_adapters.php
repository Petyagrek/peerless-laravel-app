<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExceptionInMountAdapters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mount_adapters', function (Blueprint $table) {
            $table->boolean('exception_enabled')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mount_adapters', function (Blueprint $table) {
            $table->dropColumn('exception_enabled');
        });
    }
}
