<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMountsExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mounts_exceptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_handle');
            $table->string('display_manufacturer');
            $table->string('display_part_number');
            $table->string('display_name');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mounts_exceptions');
    }
}
