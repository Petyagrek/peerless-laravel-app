<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id')->comment('Shopify product ID');
            $table->bigInteger('variant_id')->comment('Shopify variant ID');
            $table->string('sku')->comment('Shopify variant SKU');
            $table->integer('shop_id')->comment('Shopify Shop ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
