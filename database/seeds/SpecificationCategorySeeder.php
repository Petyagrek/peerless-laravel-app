<?php

use Illuminate\Database\Seeder;
use App\Models\SpecificationCategory;

class SpecificationCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SpecificationCategory::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            SpecificationCategory::create([
                'cat_name' => $faker->name,
                'position' => rand(0,10), 
                'shop_id' => 1, 
                'active' => 1,
            ]);
        }
    }
}
