<?php

use Illuminate\Database\Seeder;
use App\Models\Specification;

class SpecificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specification::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Specification::create([
                'name' => $faker->name, 
                'default_value' => '', 
                'category_id' => rand(0,10), 
                'shop_id' => 1, 
                'active' => 1, 
            ]);
        }
    }
}
