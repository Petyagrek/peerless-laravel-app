<?php

return [
  'default' => [
    'length' => 5,
    'width' => 200,
    'height' => 36,
    'quality' => 90,
    'math' => false,
  ]
];