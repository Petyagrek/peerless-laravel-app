@extends('shopify-app::layouts.default')

@section('styles')
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" crossorigin="anonymous">
<link rel="stylesheet" href="/css/app.css" /> -->
<link href="{{ config('app.url') }}/css/app.css" rel="stylesheet" />
@endsection

@section('content')
    <!-- <p>You are: {{ ShopifyApp::shop()->shopify_domain }}</p> -->
    <div id="root"></div>
@endsection

@section('scripts')
    @parent
    <script>
        var appUrl = "{{ config('app.url') }}";
        var hmac = "{{ Request::get('hmac')}}";
        var shop = "{{ Request::get('shop')}}";
        var timestamp = "{{ Request::get('timestamp')}}";
        var languages = <?php echo json_encode(config('languages.stored')) ?>;

        var objLength = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
    </script>
    <script src="{{ config('app.url') }}/js/app.js"></script>
    <script type="text/javascript">
        // ESDK page and bar title
        window.mainPageTitle = 'Welcome Page';
            ShopifyApp.ready(function() {
                ShopifyApp.Bar.initialize({
                    title: 'Welcome'
                });
                console.log(ShopifyApp);
            });
    </script>
@endsection