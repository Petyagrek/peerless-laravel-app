<?php
$menuLinks = [
    [
        'link'      => 'products',
        'class'     => 'fas fa-box',
        'link_text' => 'Products',
    ],
    [
        'link'      => 'repair_completion',
        'class'     => 'fas fa-tasks',
        'link_text' => 'Default Features',
    ],
    // [
    //     'link'      => 'settings',
    //     'class'     => 'fa-cogs',
    //     'link_text' => '',
    // ]
];
?>
<div class="navbar navbar-expand-lg navbar-dark bg-dark">
    <ul class="navbar-nav mr-auto">
        @foreach ($menuLinks as $linkItem)
        <li class="nav-item  @if (Request::is($linkItem['link']))active @endif">
            <a class="nav-link" href="{{ url('/'.$linkItem['link']) }}">
                <i class="fa {{$linkItem['class']}}" aria-hidden="true"></i>
                {{$linkItem['link_text']}}
            </a>
        </li>
        @endforeach
    </ul>
</div>