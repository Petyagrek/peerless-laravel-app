@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Cher/Chère {{$data['data']['first_name']}} {{$data['data']['last_name']}}, <br />
                  Merci de votre demande de réunion avec 
                  @if ($data['data']['title'])
                    {{$data['data']['title']}} de Peerless-AV.
                  @endif
                  ! <br />
                  Un interlocuteur vous contactera dès que possible pour confirmer votre demande et vous poser quelques questions.<br />
                  Localisation: 
                  @if ($data['data']['city'])
                    {{$data['data']['city']}}, 
                  @endif
                  @if (isset($data['data']['state']) && $data['data']['state'])
                    {{$data['data']['state']}}, 
                  @endif
                  @if ($data['data']['country'])
                  {{$data['data']['country']}} <br />
                  @endif
                  @if ($data['data']['meeting_purpose'])
                    Objet de la réunion: {{$data['data']['meeting_purpose']}} <br />
                  @endif
                  @if ($data['data']['meeting_purpose'] == 'Other' && $data['data']['meeting_purpose_other'])
                    Autre: {{$data['data']['meeting_purpose_other']}}
                  @endif
                  <br />
                  Heures de réunion préférées: 
                  @foreach ($data['data']['prefered_time'] as $timeKey => $time)
                    {{date('m/d/Y', strtotime($time))}} at
                    {{date('g:i A', strtotime($time))}} 
                    @if (count($data['data']['prefered_time']) > $timeKey+1), 
                    @endif <br />
                  @endforeach <br />
                  @if ($data['data']['tradeShowLink'])
                    Infos sur l’événement:  {{$data['data']['tradeShowLink']}}
                  @endif
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent