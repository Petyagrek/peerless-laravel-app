@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Un client a envoyé un message à l’équipe hôtellerie/restauration de Peerless-AV :
                  <br>Nom: {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  @if ($data['data']['company'])
                    <br>Entreprise: {{$data['data']['company']}}
                  @endif
                  <br>Adresse e-mail: {{$data['data']['email']}}
                  @if ($data['data']['phone'])
                    <br>Téléphone: {{$data['data']['phone']}}
                  @endif
                  @if ($data['data']['job_title'])
                    <br>Intitulé de poste: {{$data['data']['job_title']}}
                  @endif
                  <br>Message: {{$data['data']['message']}}
                  <br>Contactez le demandeur dès que possible pour donner suite à sa demande.
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent