@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Vous avez reçu une nouvelle demande de solutions sur mesure d’un client sur le site Web Peerless-AV:
                  <br>Nom:{{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Adresse e-mail: {{$data['data']['email']}}
                  <br>Entreprise:  @if ($data['data']['company']) {{$data['data']['company']}} @else S.O. @endif
                  <br>Nom du projet:  @if ($data['data']['project_title']) {{$data['data']['project_title']}} @else S.O. @endif
                  <br>Descriptif du projet: {{$data['data']['body']}}
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent