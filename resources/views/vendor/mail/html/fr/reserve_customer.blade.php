@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                Cher/Chère {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Merci pour votre demande de formation en face-à-face pour installateur agréé Peerless-AV. Le traitement de votre demande est en cours et nous vous contacterons dans les plus brefs délais.
                  <br>
                  <br>
                  <br>Cordialement,
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent