@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                Cher/Chère {{$data['owner']->first_name}},
                  <br>Un utilisateur a confirmé la connexion à votre compte d’entreprise :
                  <br>Nom: {{$data['customer']->first_name}} {{$data['customer']->last_name}}
                  <p>Adresse e-mail: {{$data['customer']->email}}
                  <p>Pour afficher les données de cet utilisateur, mettre à jour ses autorisations ou révoquer son accès, 
                    utiliser ce lien :  
                    <a href="https://{{$shop->shopify_domain}}/pages/user-management">
                      https://{{$shop->shopify_domain}}/pages/user-management
                    </a>
                    <br>
                    <br>
                    <br>Cordialement,
                    <br>Peerless-AV
                    <br>
                    <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent