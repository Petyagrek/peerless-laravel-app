@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                Cher/Chère {{$data['customer']->first_name}},
                  <br>Un administrateur vous a connecté sur le compte de l’entreprise <b>{{$data['company']->name}}</b>, et vous allez pouvoir bientôt commencer. 
                  Pour finir, cliquez sur ce lien pour confirmer cette connexion : 
                  <a href="{{$data['activation_token']}}">Se connecter à l’entreprise</a>.
                  <br>
                  <br>Si vous n’avez pas demandé cet accès ou avez reçu cet e-mail par erreur, 
                  contactez le service clientèle de Peerless-AV à l’adresse info@peerless-av.com.
                  <br>
                  <br>
                  <br>Cordialement,
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent