@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <div style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                    {{$data['data']['first_name']}} {{$data['data']['last_name']}} 
                    de {{$data['data']['company']}} a fait une demande de documentation papier. 
                    Coordonnez avec le client pour traiter la commande ci-dessous:
                    <br/>
                    <br/>
                    <div><b>Coordonnées du client</b></div>
                    <div>Nom du client: {{$data['data']['first_name']}} {{$data['data']['last_name']}} </div>
                    <div>Adresse e-mail: {{$data['data']['email']}}</div>
                    <div>Numéro de téléphone: {{$data['data']['phone']}}</div>
                    <div>Entreprise: {{$data['data']['company']}}</div>
                    @if ($data['data']['industry'])
                        <div>Secteur d’activité: {{$data['data']['industry']}}</div>
                    @endif
                    <div>Localisation: 
                        {{$data['data']['city']}}, 
                        {{$data['data']['province']}}, 
                        {{$data['data']['country']}}</div>
                    <br />
                    <div><b>Demande de documentation</b></div>
                        @foreach ($data['data']['literature'] as $literatureName => $literatureQuantity)
                            @if ($literatureQuantity > 0)
                                <div>{{$literatureName}}: {{$literatureQuantity}}</div>
                            @endif
                        @endforeach
                  <br>
                  <br>
                </div>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent