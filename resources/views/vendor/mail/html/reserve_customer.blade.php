@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Hi {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Thank your for your request for in-person certified installer training from Peerless-AV. We are reviewing your request and will be in touch shortly.
                  <br>
                  <br>
                  <br>Sincerely,
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent