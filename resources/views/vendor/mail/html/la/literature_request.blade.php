@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <div style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                    {{$data['data']['first_name']}} {{$data['data']['last_name']}} 
                    from {{$data['data']['company']}} has requested print literature. 
                    Please coordinate with the customer to fulfill the order below:
                    <br/>
                    <br/>
                    <div><b>Customer Details</b></div>
                    <div>Customer Name: {{$data['data']['first_name']}} {{$data['data']['last_name']}} </div>
                    <div>Email: {{$data['data']['email']}}</div>
                    <div>Phone Number: {{$data['data']['phone']}}</div>
                    <div>Company: {{$data['data']['company']}}</div>
                    @if ($data['data']['industry'])
                        <div>Industry: {{$data['data']['industry']}}</div>
                    @endif
                    <div>Location: 
                        {{$data['data']['city']}}, 
                        {{$data['data']['province']}}, 
                        {{$data['data']['country']}}</div>
                    <br />
                    <div><b>Requested Literature</b></div>
                        @foreach ($data['data']['literature'] as $literatureName => $literatureQuantity)
                            @if ($literatureQuantity > 0)
                                <div>{{$literatureName}}: {{$literatureQuantity}}</div>
                            @endif
                        @endforeach
                  <br>
                  <br>
                </div>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent