@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                    A new user has been connected to a company account. 
                    If necessary, please make sure this user is added to any customer systems of record (such as IFS):
                  <br>
                  <br>Name: {{$data['customer']->first_name}} {{$data['customer']->last_name}}
                  <br>Email: {{$data['customer']->email}}
                  <br>Company: {{$data['company']->name}}
                  <br>
                  For complete information on this user, view their profile here: 
                    <a href="https://{{$shop->shopify_domain}}/admin/customers">https://{{$shop->shopify_domain}}/admin/customers</a>
                  <br>
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent