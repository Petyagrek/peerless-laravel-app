@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Hi {{$data['customer']->first_name}},
                  <br>An administrator connected you to the <b>{{$data['company']->name}}</b> company account, and you're just about ready to get started. 
                  To finish setup, follow this link to confirm this connection: 
                  <a href="{{$data['activation_token']}}">Join to company</a>.
                  <br>
                  <br>If you did not request this access or believe you've received this email in error, 
                    please contact Peerless-AV Customer Care at info@peerless-av.com.
                  <br>
                  <br>
                  <br>Sincerely,
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent