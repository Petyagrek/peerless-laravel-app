@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  A customer has submitted a message for the Peerless-AV Hospitality team:
                  <br>Name: {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  @if ($data['data']['company'])
                    <br>Company: {{$data['data']['company']}}
                  @endif
                  <br>Email: {{$data['data']['email']}}
                  @if ($data['data']['phone'])
                    <br>Phone: {{$data['data']['phone']}}
                  @endif
                  @if ($data['data']['job_title'])
                    <br>Job title: {{$data['data']['job_title']}}
                  @endif
                  <br>Message: {{$data['data']['message']}}
                  <br>Please contact the requestor promptly to follow up on their request.
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent