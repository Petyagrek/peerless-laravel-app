<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Customer account confirmation</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" type="text/css" href="/assets/notifications/styles.css">
  <style>body {
    margin: 0;
    }
    h1 a:hover {
    font-size: 30px; color: #333;
    }
    h1 a:active {
    font-size: 30px; color: #333;
    }
    h1 a:visited {
    font-size: 30px; color: #333;
    }
    a:hover {
    text-decoration: none;
    }
    a:active {
    text-decoration: none;
    }
    a:visited {
    text-decoration: none;
    }
    .button__text:hover {
    color: #fff; text-decoration: none;
    }
    .button__text:active {
    color: #fff; text-decoration: none;
    }
    .button__text:visited {
    color: #fff; text-decoration: none;
    }
    a:hover {
    color: #ee3423;
    }
    a:active {
    color: #ee3423;
    }
    a:visited {
    color: #ee3423;
    }
    @media (max-width: 600px) {
      .container {
        width: 94% !important;
      }
      .main-action-cell {
        float: none !important; margin-right: 0 !important;
      }
      .secondary-action-cell {
        text-align: center; width: 100%;
      }
      .header {
        margin-top: 20px !important; margin-bottom: 2px !important;
      }
      .shop-name__cell {
        display: block;
      }
      .order-number__cell {
        display: block; text-align: left !important; margin-top: 20px;
      }
      .button {
        width: 100%;
      }
      .or {
        margin-right: 0 !important;
      }
      .apple-wallet-button {
        text-align: center;
      }
      .customer-info__item {
        display: block; width: 100% !important;
      }
      .spacer {
        display: none;
      }
      .subtotal-spacer {
        display: none;
      }
    }
    </style>
  <style>
    .button__cell { background: #ee3423; }
    a, a:hover, a:active, a:visited { color: #ee3423; }
    .shop-name__cell {text-align: center;}
    .footer__cell {
      background-color: rgb(127,127,127);
      color: #fff;
      text-align: center;
    }
    .soc-block {text-align: center; margin: 20px 0 10px;}
    .soc-block a {display: inline-block;}
    .soc-block a img {width: 40px;}
    .footer__cell  h3 {font-size: 16px; color: #fff;text-align: center;font-weight: bold;margin:0;}
    .disclaimer__subtext {color: #fff; text-align: center;font-size: 16px;}
    .home-url {text-align: center;}
    .home-url a {color: #fff; font-weight: bold;}
  </style>
</head>
  <body>
    <table class="body" style="max-width:600px;margin:auto;">
      <tr>
        <td>
          <table class="header row" style="width:100%; margin-top:20px;margin-bottom: 30px;">
            <tr>
              <td class="header__cell">
                <center>
                  <table class="container" style="width:100%">
                    <tr>
                      <td>
                        <table class="row" style="width:100%">
                          <tr>
                            <td class="shop-name__cell">
                              <img 
                                src="{{ URL::to('/') }}/img/peerless-logo.png" 
                                alt="Peerless-AV" 
                                width="350">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </center>
              </td>
            </tr>
          </table>

          <table class="row content">
            <tr>
              <td class="content__cell">
                <center>
                  <table class="container">
                      {{ $content ?? '' }}
                  </table>
                </center>
              </td>
            </tr>
          </table>

          <table class="row footer" 
            style="width: 100%; border-spacing: 0; border-collapse: collapse; border-top-width: 1px; border-top-color: #e5e5e5; border-top-style: solid;">
            <tr>
              <td class="footer__cell" 
                style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; color: #fff; padding: 35px 0;">
                <center>
                  <table class="container">
                    <tr>
                      <td>
                        <h3 style="font-weight: bold; font-size: 16px; color: #fff; margin: 0;">WE'RE SOCIAL!</h3>
                        <p class="disclaimer__subtext" style="color: #fff; line-height: 150%; font-size: 16px; margin: 0;">Follow us on your favorite social media platform.</p>
                        <div class="soc-block" style="margin: 20px 0 10px;">
                          <a href="https://twitter.com/PeerlessAV" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/twiter.png?13600137230459560940" alt="Twitter"/>
                          </a>
                          <a href="https://www.instagram.com/peerlessav/" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/inst.png?13600137230459560940" alt="Instagram"/>
                          </a>
                          <a href="https://www.linkedin.com/authwall?trk=bf&trkInfo=AQF6fOMgAjF6EgAAAWjxUy54uw9DXIx3p65SYVTXNrtuolI3mMohixdB580zjALqwjhPN_0QlcGdlOBVSygBGBC20nEsIbgJ8aO9jt75UARDGkMTx5gCEA7iYICIQFyf4iCFTUg=&originalReferer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F849802" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/linedin.png?13600137230459560940" alt="Linkedin"/>
                          </a>
                          <a href="https://www.facebook.com/PeerlessAV" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/facebook.png?13600137230459560940" alt="Facebook"/>
                          </a>
                          <a href="https://www.youtube.com/c/peerlessav" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/youtube.png?13600137230459560940" alt="Youtube"/>
                          </a>
                          <a href="https://blog.peerless-av.com/" target="_blank">
                            <img src="https://cdn.shopify.com/s/files/1/0026/4083/8771/files/wordpress.png?13600137230459560940" alt="Wordpress"/>
                          </a>
                        </div>
                        <div class="home-url">
                        @if (isset($shop))
                        <a href="https://{{$shop->shopify_domain}}/" target="_blank">{{$shop->shopify_domain}}</a>
                        @endif
                        </div>
                      </td>
                    </tr>
                  </table>
                </center>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
