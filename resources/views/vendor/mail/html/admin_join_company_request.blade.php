@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  New user has applied for a existing business account, please contact the requestor promptly to follow up on their request.
                  <br>
                  <br>Name: {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Company: {{$data['data']['company']}}
                  <br>Email: {{$data['data']['email']}}
                  @if ($data['data']['phone'])
                    <br>Phone: {{$data['data']['phone']}}
                  @endif
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent