@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  A customer has submitted a new request for in-person certified installer training from Peerless-AV:
                  <br>Name: {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Company: {{$data['data']['company']}}
                  <br>Location: {{$data['data']['city']}}, {{$data['data']['state']}}, {{$data['data']['country']}}
                  <br>Email: {{$data['data']['email']}}
                  <br>Phone: {{$data['data']['phone']}}
                  <br>Approximate Number of Trainees: {{$data['data']['trainees']}}
                  <br>Where the training will be held: {{$data['data']['location']}}
                  @if ($data['data']['title'])
                    <br>Title: {{$data['data']['title']}}
                  @endif
                  @if ($data['data']['other'])
                    <br>Other Details: {{$data['data']['other']}}
                  @endif
                  <br>Please contact the requestor promptly to follow up on their request or arrange training.
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent