@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Hi {{$data['owner']->first_name}},
                  <br>A user confirmed the connection to your company account:
                  <br>Name: {{$data['customer']->first_name}} {{$data['customer']->last_name}}
                  <p>Email: {{$data['customer']->email}}
                  <p>To view this user's information, update their permissions, or revoke their access, 
                    you can manage their information here: 
                    <a href="https://{{$shop->shopify_domain}}/pages/user-management">
                      https://{{$shop->shopify_domain}}/pages/user-management
                    </a>
                    <br>
                    <br>
                    <br>Sincerely,
                    <br>Peerless-AV
                    <br>
                    <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent