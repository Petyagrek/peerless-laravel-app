@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td>
        <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
        <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
          Hi {{$data['customer']->first_name}} {{$data['customer']->last_name}}, We've received your request to create an account with Peerless-AV, and you're just about ready to get started.
        </p>
        <p>
          If you did not request this access or believe you've received this email in error, please contact Peerless-AV Customer Care at info@peerless-av.com.
        </p>
        <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
          You can login <a href="https://{{$shop->shopify_domain}}/account/login" style="font-size: 16px; text-decoration: none; color: #ee3423;">here</a>.
        </p>
        <p>
          This is your credential:
          <br>
          <ul style="padding-left:20px;">
            <li>Login: {{$data['customer']->email}}</li>
            @isset($data['customer']->password)
              <li>Password: {{$data['customer']->password}}</li>
            @endisset
          </ul>
        </p>
        <br>
        <p>
          Sincerely,<br />
          Peerless-AV
        </p>
      </td>
    </tr>
  @endslot
@endcomponent
