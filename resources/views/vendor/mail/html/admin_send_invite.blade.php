@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <div style="color: #777; line-height: 150%; font-size: 16px; margin: 0 0 20px;">
                    Hi {{$data['data']->first_name}} {{$data['data']->last_name}},
                    <p>
                        An administrator has set up your new Peerless-AV account, 
                        and you're just about ready to get started. 
                        To finish setup, 
                        follow this link to set your password and authorize your account: 
                        <a href="{{$data['data']->activationURL}}">Activate your account</a>.
                    </p>
                    <p>If you did not request this access or believe you've received this email in error, 
                        please contact Peerless-AV Customer Care at info@peerless-av.com.</p>
                    <p>Sincerely,</p>
                    <p>Peerless-AV</p>
                </div>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent