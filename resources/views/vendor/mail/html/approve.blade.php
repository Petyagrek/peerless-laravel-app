@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Hi {{$data['customer']->first_name}},
                  <br>
                  <br>
                    An administrator approved your connection to the  <b>{{$data['company']->name}}</b> 
                    company account.
                  <br>
                  <br>
                    If you have any questions or concerns, please contact your company administrator. 
                    If you did not request this access or believe you've received this email in error, 
                    please contact Peerless-AV Customer Care at info@peerless-av.com
                  <br>
                  <br>Sincerely,
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent