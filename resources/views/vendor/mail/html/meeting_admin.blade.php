@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  @if ($data['data']['company'])
                    from {{$data['data']['company']}}
                  @endif
                  has requested a meeting with Peerless-AV 
                  @if ($data['data']['title'])
                    at {{$data['data']['title']}}. 
                  @endif
                  <br>

                  You can contact this customer at any time to confirm or deny their request using the contact information provided below:

                  @if ($data['data']['tradeShowLink'])
                    Event Details: {{$data['data']['tradeShowLink']}}
                  @endif
                  <br />

                  Customer: {{$data['data']['first_name']}} {{$data['data']['last_name']}} <br>
                  Email: {{$data['data']['email']}} <br>
                  @if ($data['data']['phone'])
                    Phone Number: {{$data['data']['phone']}} <br>
                  @endif
                  @if ($data['data']['company'])
                    Company: {{$data['data']['company']}} <br>
                  @endif
                  @if ($data['data']['industry'])
                    Industry: {{$data['data']['industry']}} <br>
                  @endif
                  Customer Location: 
                  @if ($data['data']['city'])
                    {{$data['data']['city']}}, 
                  @endif
                  @if (isset($data['data']['state']) && $data['data']['state'])
                    {{$data['data']['state']}}, 
                  @endif
                  @if ($data['data']['country'])
                    {{$data['data']['country']}}
                  @endif
                  <br />
                  @if ($data['data']['meeting_purpose'])
                    Purpose of Meeting: {{$data['data']['meeting_purpose']}} <br />
                  @endif
                  @if ($data['data']['meeting_purpose'] == 'Other' && $data['data']['meeting_purpose_other'])
                    Other: {{$data['data']['meeting_purpose_other']}}
                  @endif
                  <br />
                  Preferred Meeting Times: 
                  @foreach ($data['data']['prefered_time'] as $timeKey => $time)
                    {{date('m/d/Y', strtotime($time))}} at
                    {{date('g:i A', strtotime($time))}} 
                    @if (count($data['data']['prefered_time']) > $timeKey+1), 
                    @endif <br />
                  @endforeach

                  @if ($data['data']['notes'])
                    Notes: {{$data['data']['notes']}}
                  @endif
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent