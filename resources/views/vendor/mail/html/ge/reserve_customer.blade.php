@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Liebe(r) {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>vielen Dank für Ihren Antrag auf eine Teilnahme am persönlichen Zertifizierungskurs für Installationstechniker von Peerless-AV. 
                  Wir überprüfen Ihren Antrag derzeit und werden uns in Kürze bei Ihnen melden.
                  <br>
                  <br>
                  <br>Mit freundlichen Grüßen
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent