@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Liebe(r) {{$data['owner']->first_name}},
                  <br>ein Benutzer hat die Verbindung mit Ihrem Firmenkonto bestätigt:
                  <br>Name: {{$data['customer']->first_name}} {{$data['customer']->last_name}}
                  <p>E-Mail: {{$data['customer']->email}}
                  <p>Um die Informationen zu diesem Benutzer anzuzeigen, seine Berechtigungen zu aktualisieren oder seinen Zugriff zu widerrufen, 
                    können Sie die relevanten Informationen hier bearbeiten: 
                    <a href="https://{{$shop->shopify_domain}}/pages/user-management">
                      https://{{$shop->shopify_domain}}/pages/user-management
                    </a>
                    <br>
                    <br>
                    <br>Mit freundlichen Grüßen
                    <br>Peerless-AV
                    <br>
                    <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent