@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;"> 
                  Sie haben von einem Kunden eine neue Anforderung einer kundenspezifischen Lösung auf der Peerless-AV-Website erhalten:
                  <br>Name:{{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>E-Mail: {{$data['data']['email']}}
                  <br>Firma:  @if ($data['data']['company']) {{$data['data']['company']}} @else nicht zutr. @endif
                  <br>Name des Projekts:  @if ($data['data']['project_title']) {{$data['data']['project_title']}} @else nicht zutr. @endif
                  <br>Beschreibung des Projekts: {{$data['data']['body']}}
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent