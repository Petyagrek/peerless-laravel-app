@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <div style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                    {{$data['data']['first_name']}} {{$data['data']['last_name']}} 
                    von {{$data['data']['company']}} hat Druckmaterial angefordert. 
                    Bitte koordinieren Sie diese Anforderung mit dem Kunden, um die folgende Bestellung abwickeln zu können:
                    <br/>
                    <br/>
                    <div><b>Angaben zum Kunden</b></div>
                    <div>Name des Kunden: {{$data['data']['first_name']}} {{$data['data']['last_name']}} </div>
                    <div>E-Mail: {{$data['data']['email']}}</div>
                    <div>Telefonnummer: {{$data['data']['phone']}}</div>
                    <div>Firma: {{$data['data']['company']}}</div>
                    @if ($data['data']['industry'])
                        <div>Branche: {{$data['data']['industry']}}</div>
                    @endif
                    <div>Ort: 
                        {{$data['data']['city']}}, 
                        {{$data['data']['province']}}, 
                        {{$data['data']['country']}}</div>
                    <br />
                    <div><b>Angefordertes Lesematerial</b></div>
                        @foreach ($data['data']['literature'] as $literatureName => $literatureQuantity)
                            @if ($literatureQuantity > 0)
                                <div>{{$literatureName}}: {{$literatureQuantity}}</div>
                            @endif
                        @endforeach
                  <br>
                  <br>
                </div>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent