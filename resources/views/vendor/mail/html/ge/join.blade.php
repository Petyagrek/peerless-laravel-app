@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center>
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Liebe(r) {{$data['customer']->first_name}},
                  <br>ein Administrator hat Sie mit dem <b>{{$data['company']->name}}</b>-Firmenkonto verbunden, und Sie können gleich loslegen. 
                  Zum Abschluss der Kontoeinrichtung folgen Sie diesem Link, um die Verbindung zu bestätigen: 
                  <a href="{{$data['activation_token']}}">Mit Firmenkonto verbinden</a>.
                  <br>
                  <br>Wenn Sie diesen Zugang nicht angefordert haben oder glauben, diese E-Mail versehentlich erhalten zu haben, 
                  wenden Sie sich unter info@peerless-av.com bitte an den Peerless-AV-Kundendienst.
                  <br>
                  <br>
                  <br>Mit freundlichen Grüßen
                  <br>Peerless-AV
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent