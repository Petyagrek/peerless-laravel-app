@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Liebe(r) {{$data['data']['first_name']}} {{$data['data']['last_name']}}, <br />
                  wir haben Ihre Bitte um ein Treffen mit Peerless-AV 
                  @if ($data['data']['title'])
                    bei {{$data['data']['title']}} erhalten.
                  @endif
                  Dafür vielen Dank! <br />
                  Ein Vertreter sollte sich vor Ihrem Treffen mit Ihnen in Verbindung setzen, um Ihre Bitte zu bestätigen bzw. einen neuen Termin zu vereinbaren und etwas mehr über Sie in Erfahrung zu bringen. <br />
                  Ort: 
                  @if ($data['data']['city'])
                    {{$data['data']['city']}}, 
                  @endif
                  @if (isset($data['data']['state']) && $data['data']['state'])
                    {{$data['data']['state']}}, 
                  @endif
                  @if ($data['data']['country'])
                  {{$data['data']['country']}} <br />
                  @endif
                  @if ($data['data']['meeting_purpose'])
                    Zweck des Treffens: {{$data['data']['meeting_purpose']}} <br />
                  @endif
                  @if ($data['data']['meeting_purpose'] == 'Other' && $data['data']['meeting_purpose_other'])
                    Sonstiger: {{$data['data']['meeting_purpose_other']}}
                  @endif
                  <br />
                  Bevorzugte Gesprächszeiten: 
                  @foreach ($data['data']['prefered_time'] as $timeKey => $time)
                    {{date('m/d/Y', strtotime($time))}} at
                    {{date('g:i A', strtotime($time))}} 
                    @if (count($data['data']['prefered_time']) > $timeKey+1), 
                    @endif <br />
                  @endforeach <br />
                  @if ($data['data']['tradeShowLink'])
                    Angaben zur Veranstaltung: {{$data['data']['tradeShowLink']}}
                  @endif
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent