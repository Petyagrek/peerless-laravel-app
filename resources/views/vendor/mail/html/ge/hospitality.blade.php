@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  Ein Kunde hat uns eine Nachricht für das Hospitality-Team von Peerless-AV geschickt.
                  <br>Name: {{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  @if ($data['data']['company'])
                    <br>Firma: {{$data['data']['company']}}
                  @endif
                  <br>E-Mail: {{$data['data']['email']}}
                  @if ($data['data']['phone'])
                    <br>Tel: {{$data['data']['phone']}}
                  @endif
                  @if ($data['data']['job_title'])
                    <br>Stellentitel: {{$data['data']['job_title']}}
                  @endif
                  <br>Nachricht: {{$data['data']['message']}}
                  <br>Bitte setzen Sie sich zur weiteren Bearbeitung unverzüglich mit dem Kunden in Verbindung, der uns den Antrag geschickt hat.
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent