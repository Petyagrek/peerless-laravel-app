@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                    Ein Peerless-AV-Kunde hat darum gebeten, auf peerless-av.com eingekaufte Produkte zurückzuschicken. 
                    Bitte wenden Sie sich zwecks weiterer Informationen bzw. zum Genehmigen oder Ablehnen des Antrags an den Kunden:  <br />
                    Name: {{$data['data']['customer']->first_name}} {{$data['data']['customer']->last_name}} <br />
                    E-Mail: {{$data['data']['customer']->email}} <br />
                    @isset($data['data']['customer']->company->name)
                    Firma: {{$data['data']['customer']->company->name}} <br />
                    @endisset
                    Produkte: 
                    <table border="1">
                        <tr>
                            <th>Product Title</th>
                            <th>Variant title</th>
                            <th>Sku</th>
                            <th>Return Qty</th>
                        </tr>
                        @foreach ($data['data']['ordersToReturn'] as $product)
                            <tr>
                                <td>
                                  @isset($product['title']) {{$product['title']}} @endisset
                                  @isset($product['product_title']) {{$product['product_title']}} @endisset
                                </td>
                                <td>{{$product['variant_title']}}</td>
                                <td>{{$product['sku']}}</td>
                                <td>{{$product['qtyReturn']}}</td>
                            </tr>
                        @endforeach
                    </table>
                    <br>
                    Reason: {{$data['data']['reason']}} <br />
                    Click here to view this <a href="https://{{$data['data']['shop']}}/admin/orders/{{$data['data']['orderID']}}">order</a>
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent