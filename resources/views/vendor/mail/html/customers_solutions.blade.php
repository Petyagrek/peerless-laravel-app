@component('mail::layout', ['shop' => $shop])
  @slot('content')
    <tr>
      <td class="content__cell">
        <center style="padding: 0 20px;">
          <table class="container">
            <tr>
              <td>
                <h2 style="font-weight: normal; font-size: 24px; margin: 0 0 10px;">{{$data['subject']}}</h2>
                <p style="color: #777; line-height: 150%; font-size: 16px; margin: 0;">
                  You have received a new custom solutions request from a customer on the Peerless-AV website:
                  <br>Name:{{$data['data']['first_name']}} {{$data['data']['last_name']}}
                  <br>Email: {{$data['data']['email']}}
                  <br>Company:  @if ($data['data']['company']) {{$data['data']['company']}} @else N/A @endif
                  <br>Project Title:  @if ($data['data']['project_title']) {{$data['data']['project_title']}} @else N/A @endif
                  <br>Project Description: {{$data['data']['body']}}
                  <br>
                  <br>
                </p>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  @endslot
@endcomponent