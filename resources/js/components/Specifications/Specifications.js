import React, { Component } from "react";
import { Page, Card, ResourceList, TextStyle, Avatar, Button, Modal, TextContainer, Toast, SettingToggle } from "@shopify/polaris";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";


class Specifications extends Component {

    constructor () {
        super();
        this.state = {
            searchValue: '',
            selectedItems: [],
            items: [],
            redirectToAddSpecification: false,
            apiUrl: appUrl+'/api/specifications',
            deleteUrl: appUrl+'/api/specifications',
            modalActive: false,
            showToast: false,
            toastError: false,
            toastMessage: '',
            isSpecificationsLoading: true,
        };

        this.getSpecifications();
    }

    getSpecifications () {
        const { apiUrl, searchValue } = this.state;
        axios.get(apiUrl, {
            params: {
                shop: shop,
                searchValue: searchValue,
            }
        })
        .then(resp => {
            this.setState({
                items: resp.data,
                isSpecificationsLoading: false
            });
        })
        .catch(err => {
            console.log(err);
            this.handleToast('Error happened. Please reload and try again!', true);
        })
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.isLoading();
        });

        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.handleModalChange = this.handleModalChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.toggleToast = this.toggleToast.bind(this);
        this.handleToast = this.handleToast.bind(this);

        this.toggleSpecificationStatus = this.toggleSpecificationStatus.bind(this);

        this.renderItem = this.renderItem.bind(this);
    }

    render () {
        const {
            redirectToAddSpecification,
            items,
            selectedItems,
            modalActive,
            showToast,
            toastError,
            toastMessage,
            isSpecificationsLoading,
        } = this.state;

        const toastMarkup = showToast ? <Toast content={toastMessage} onDismiss={this.toggleToast} error={toastError} /> : null;

        const resourceName = {
            singular: 'specification',
            plural: 'specifications',
        };

        const selectedItemsCount = selectedItems.length;
        
        const modalTitle = selectedItemsCount > 1 
            ? `Delete ${selectedItemsCount} ${resourceName.plural}?` 
            : `Delete ${selectedItemsCount} ${resourceName.singular}?`;

        const bulkActions = [
            {
                content: 'Delete specifications',
                onAction: () => this.handleModalChange(),
            },
        ];

        const primaryAction = {
            content: "Add Specification",
            onAction: () => this.handleCatrgoryChange(),
        }

        const filterControl = (
            <ResourceList.FilterControl
                searchValue={this.state.searchValue}
                onSearchChange={this.handleSearchChange.bind(this)}
            />
        );

        if (redirectToAddSpecification) {
            return <Redirect to='/specification/create'/>;
        }

        return (
            <Page title="Specifications" primaryAction={primaryAction}>
                <Card  sectioned>
                    <ResourceList
                        resourceName={resourceName}
                        loading={isSpecificationsLoading}
                        items={items}
                        renderItem={this.renderItem}
                        selectedItems={this.state.selectedItems}
                        onSelectionChange={this.handleSelectionChange}
                        bulkActions={bulkActions}
                        filterControl={filterControl}
                    >
                    </ResourceList>
                </Card>

                <Modal
                    open={modalActive}
                    onClose={this.handleModalChange}
                    title={modalTitle}
                    primaryAction={{
                        content: 'Delete',
                        onAction: this.handleDelete,
                    }}
                    secondaryActions={[
                        {
                        content: 'Cancel',
                        onAction: this.handleModalChange,
                        },
                    ]}
                    >
                    <Modal.Section>
                        <TextContainer>
                            <p>Deleted specifications cannot be recovered. Do you still want to continue?</p>
                        </TextContainer>
                    </Modal.Section>
                </Modal>
                {toastMarkup}
            </Page>
        );
    }


    // search specification method
    handleSearchChange (searchValue) {
        this.setState({
            searchValue: searchValue, 
            isSpecificationsLoading: true,
        });
        setTimeout(() => {
            this.getSpecifications();
        }, 1000);
    }

    // trigger when change items
    handleSelectionChange(selectedItems) {
        this.setState({
            selectedItems: selectedItems
        });
    }

    /// trigger when click create category btn
    handleCatrgoryChange() {
        const { redirectToAddSpecification } = this.state;
        this.setState({
            redirectToAddSpecification: !redirectToAddSpecification
        })
    }

    /// open/close modal
    handleModalChange () {
        this.setState(({modalActive}) => ({modalActive: !modalActive}));
    };

    // set data for toast
    handleToast(message, error) {
        this.setState({
            toastError: error,
            toastMessage: message,
        });

        // show toast
        this.toggleToast();
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    };

    // thigger when delete category
    handleDelete() {
        const {deleteUrl, selectedItems, items} = this.state;

        axios.delete(deleteUrl, {
            params: {
                items: selectedItems,
                shop: shop,
            }
        })
        .then((resp) => {
            this.handleToast("Succesfuly deleted", false);

            // close modal
            this.handleModalChange();

            this.setState(({prevState}) => ({
                items: items.filter(item => selectedItems.indexOf(item.id) === -1),
                selectedItems: [],
            }));
        })
        .catch(err => {
            console.log(err)
            this.handleToast("Error happened", true);

            // close modal
            this.handleModalChange()
        });
    }

    toggleSpecificationStatus(event,id, active) {
        event.preventDefault();
        const newActive = !active;
        const { items } = this.state;
        const item = items.find((el) => el.id === id);

        axios.put(appUrl+'/api/specification/'+ id, {
            shop: shop,
            id: id,
            active: newActive,
        })
        .then(resp => {
            item.active = newActive ? 1 : 0;

            this.setState({
                items: items,
            });

            this.handleToast(resp.data.message, resp.data.error);
        })
        .catch(err => {
            console.log(err);
            this.handleToast('Error happened. Please reload and try again!', true);
        })
        
    }

    renderItem(item) {
        const {id, name, category, active} = item;
        const media = <Avatar customer size="medium" name={name} />;

        const contentStatus = active ? 'Disable' : 'Enable';
        const actionBtn = active 
            ? <Button onClick={(event) => {this.toggleSpecificationStatus(event, id, active);}}>{contentStatus}</Button>
            : <Button primary onClick={(event) => {this.toggleSpecificationStatus(event, id, active);}}>{contentStatus}</Button>;

        const categoryMarkup = category 
            ? <TextStyle variation="subdued">Category Name: <b>{category.cat_name}</b></TextStyle> 
            : null;

        return (
            <ResourceList.Item
                id={id}
                media={media}
                accessibilityLabel={`View details for ${name}`}
            >
                <div className="flexResourceContent">
                    <h3>
                        <TextStyle variation="strong">{name}</TextStyle>
                        <div>{categoryMarkup}</div>
                    </h3>
                    <div>
                        {actionBtn}
                        <Link className="resourceLink" to={'/specification/update/'+id} ><Button>Open</Button></Link>
                    </div>
                </div>
            </ResourceList.Item>
        );
    }
}


export default Specifications;