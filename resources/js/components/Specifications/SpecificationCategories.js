import React, { Component } from "react";
import { Page, Card, ResourceList, TextStyle, Avatar, Button, Modal, TextContainer, Toast } from "@shopify/polaris";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

class SpecificationCategories extends Component {

    constructor() {
        super();
        this.state = {
            selectedItems: [],
            items: [],
            redirectToAddCategory: false,
            apiUrl: appUrl+'/api/categories',
            deleteUrl: appUrl+'/api/categories',
            modalActive: false,
            showToast: false,
            toastError: false,
            toastMessage: '',
            isCategoryLoading: true,
        };

        this.getCategories();
    }

    getCategories () {
        const { apiUrl } = this.state;
        axios.get(apiUrl, {
            params: {
                shop: shop,
            }
        })
        .then(resp => {
            this.setState({
                items: resp.data,
                isCategoryLoading: false
            });
        })
        .catch(err => {
            console.log(err);
            this.handleToast('Error happened. Please reload and try again!', true)
        })
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.isLoading();
        });

        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.handleModalChange = this.handleModalChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.toggleToast = this.toggleToast.bind(this);
        this.handleToast = this.handleToast.bind(this);
    }

    render () {
        const {
            redirectToAddCategory,
            items,
            selectedItems,
            modalActive,
            showToast,
            toastError,
            toastMessage,
            isCategoryLoading,
        } = this.state;

        const toastMarkup = showToast ? <Toast content={toastMessage} onDismiss={this.toggleToast} error={toastError} /> : null;

        const resourceName = {
            singular: 'category',
            plural: 'categories',
        };

        const selectedItemsCount = selectedItems.length;
        
        const modalTitle = selectedItemsCount > 1 
            ? `Delete ${selectedItemsCount} ${resourceName.plural}?` 
            : `Delete ${selectedItemsCount} ${resourceName.singular}?`;

        const bulkActions = [
            {
                content: 'Delete categories',
                onAction: () => this.handleModalChange(),
            },
        ];

        const primaryAction = {
            content: "Add Category",
            onAction: () => this.handleCatrgoryChange(),
        }

        if (redirectToAddCategory) {
            return <Redirect to='/specification-category/create'/>;
        }

        return (
            <Page title="Specification Categories" primaryAction={primaryAction}>
                <Card  sectioned>
                    <ResourceList
                        resourceName={resourceName}
                        loading={isCategoryLoading}
                        items={items}
                        renderItem={this.renderItem}
                        selectedItems={this.state.selectedItems}
                        onSelectionChange={this.handleSelectionChange}
                        bulkActions={bulkActions}
                    >
                    </ResourceList>
                </Card>

                <Modal
                    open={modalActive}
                    onClose={this.handleModalChange}
                    title={modalTitle}
                    primaryAction={{
                        content: 'Delete',
                        onAction: this.handleDelete,
                    }}
                    secondaryActions={[
                        {
                        content: 'Cancel',
                        onAction: this.handleModalChange,
                        },
                    ]}
                    >
                    <Modal.Section>
                        <TextContainer>
                            <p>Deleted categories cannot be recovered. Do you still want to continue?</p>
                        </TextContainer>
                    </Modal.Section>
                </Modal>
                {toastMarkup}
            </Page>
        );
    }

    // trigger when change items
    handleSelectionChange(selectedItems) {
        this.setState({
            selectedItems: selectedItems
        });
    }

    /// trigger when click create category btn
    handleCatrgoryChange() {
        const { redirectToAddCategory } = this.state;
        this.setState({
            redirectToAddCategory: !redirectToAddCategory
        })
    }

    /// open/close modal
    handleModalChange () {
        this.setState(({modalActive}) => ({modalActive: !modalActive}));
    };

    // set data for toast
    handleToast(message, error) {
        this.setState({
            toastError: error,
            toastMessage: message,
        });

        // close modal
        this.handleModalChange()
        // show toast
        this.toggleToast();
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    };

    // thigger when delete category
    handleDelete() {
        const {deleteUrl, selectedItems, items} = this.state;

        axios.delete(deleteUrl, {
            params: {
                items: selectedItems,
                shop: shop,
            }
        })
        .then((resp) => {
            this.handleToast("Succesfuly deleted", false);

            this.setState(({prevState}) => ({
                items: items.filter(item => selectedItems.indexOf(item.id) === -1),
                selectedItems: [],
            }));
        })
        .catch(err => {
            console.log(err);
            this.handleToast("Error happened", true);
        });
    }

    renderItem(item) {
        const {id, cat_name} = item;
        const media = <Avatar customer size="medium" name={cat_name} />;

        return (
            <ResourceList.Item
                id={id}
                media={media}
                accessibilityLabel={`View details for ${cat_name}`}
            >
                <div className="flexResourceContent">
                    <h3>
                        <TextStyle variation="strong">{cat_name}</TextStyle>
                    </h3>
                    <Link className="resourceLink" to={'/specification-category/update/'+id} ><Button>Open</Button></Link>
                </div>
            </ResourceList.Item>
        );
    }
}

export default SpecificationCategories;