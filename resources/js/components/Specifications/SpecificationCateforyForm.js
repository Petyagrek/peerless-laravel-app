import React, { Component } from "react";
import { Page, Card, Form, FormLayout, TextField, Button, Toast, Select } from "@shopify/polaris";
import axios from "axios";


class SpecificationCateforyForm extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            cat_name: "",
            position: "",
            lang: languages[0],
            showToast: false,
            apiUrl: appUrl+'/api/category',
            langArr: [],
        };

        // this mean we update product
        if (this.props.match && this.props.match.params.id) {
            this.state.id = this.props.match.params.id;
            // load category
            this.getCategory();
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.createCategory = this.createCategory.bind(this);

        this.toggleToast = this.toggleToast.bind(this);
        this.handleToast = this.handleToast.bind(this);
    }

    componentDidMount() {
        this.prepareLangField();
    }

    prepareLangField() {
        const {langArr} = this.state;

        languages.forEach(function (lang) {
            langArr.push({label: lang, value: lang});
        }.bind(this));

        this.setState({langArr});
    }

    createCategory() {
        const {apiUrl, cat_name, lang, position} = this.state;

        axios.post(apiUrl, {
            cat_name,
            position,
            lang,
            shop,
        })
        .then(resp => {
            this.handleToast(resp.data.message, resp.data.error);
        })
        .catch(error => {
            console.log(error);
            this.handleToast("Error happened", true);
        });
    }
    
    updateCategory() {
        const {apiUrl, cat_name, lang, position, id} = this.state;

        axios.put(apiUrl+'/'+id, {
            id,
            cat_name,
            position,
            lang,
            shop,
        })
        .then(resp => {
            this.handleToast(resp.data.message, resp.data.error);
        })
        .catch(error => {
            console.log(error);
            this.handleToast("Error happened", true);
        });
    }

    getCategory() {
        const {apiUrl, id} = this.state;
        axios.get(apiUrl+'/'+id, {
            params: {
                shop: shop,
            }
        })
        .then(resp => {
            this.setState({
                cat_name: resp.data.cat_name,
                position: resp.data.position,
                lang: resp.data.lang,
            });
        })
        .catch(err => {
            console.log(err);
        })
    }

    render() {
        const {
            id,
            cat_name,
            position,
            lang,
            showToast,
            toastError,
            toastMessage,
            langArr,
        } = this.state;
        const toastMarkup = showToast ? <Toast 
                error={toastError}
                content={toastMessage} 
                onDismiss={this.toggleToast} /> : null;

        const title = id ? "Update category" : "Add new category";

        return (
            <Page title={title}>
                <Card sectioned>
                    <Form onSubmit={this.handleSubmit} preventDefault={true}>
                        <FormLayout>
                            <TextField
                                value={cat_name}
                                onChange={this.handleChange('cat_name')}
                                label="Category name"
                                type="text"
                            />
                            <TextField
                                value={position}
                                onChange={this.handleChange('position')}
                                label="Category position"
                                type="number"
                            />
                            <Select
                                label="Language"
                                options={langArr}
                                onChange={this.handleChange('lang')}
                                value={lang}
                                ></Select>
                            <Button submit>Submit</Button>
                        </FormLayout>
                    </Form>
                    {toastMarkup}
                </Card>
            </Page>
        );
    }

    // set data for toast
    handleToast(message, error) {
        this.setState({
            toastError: error,
            toastMessage: message,
        });

        // show toast
        this.toggleToast();
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    };

    handleSubmit(event) {
        const {cat_name, position, id } = this.state;
        let errors = [];
        if (cat_name.trim() == '') {
            errors.push('Category name canot be blank.');
        }

        if (position == '') {
            errors.push('Category position canot be blank.');
        }

        if (errors.length) {
            this.handleToast(errors.join(' '), true);
        } else {
            if (id) this.updateCategory();
            else this.createCategory();
        }
    };

    handleChange(field) {
        return (value) => this.setState({[field]: value});
    };
}

export default SpecificationCateforyForm;