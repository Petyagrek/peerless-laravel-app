import React, { Component } from "react";
import { Page, Card, Form, FormLayout, TextField, Button, Toast, Select } from "@shopify/polaris";
import axios from "axios";


class SpecificationForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: null,
      name: "",
      default_value: "",
      lang: languages[0],
      categories: null,
      category_id: '',
      position: '',
      showToast: false,
      apiUrl: appUrl + '/api/specification',
      langArr: [],
    };

    this.getCategories();

    // this mean we update product
    if (this.props.match && this.props.match.params.id) {
      this.state.id = this.props.match.params.id;
      // load category
      this.getSpecification();
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createSecification = this.createSecification.bind(this);

    this.toggleToast = this.toggleToast.bind(this);
    this.handleToast = this.handleToast.bind(this);
  }

  componentDidMount() {
      this.prepareLangField();
  }

  prepareLangField() {
    const {langArr} = this.state;

    languages.forEach(function (lang) {
      langArr.push({label: lang, value: lang});
    }.bind(this));

    this.setState({langArr});
  }

  createSecification() {
    const { apiUrl, name, category_id, default_value, lang, position } = this.state;

    axios.post(apiUrl, {
      name,
      lang,
      default_value,
      category_id,
      position,
      shop,
    })
    .then(resp => {
      this.handleToast(resp.data.message, resp.data.error);
    })
    .catch(error => {
      console.log(error);
      this.handleToast("Error happened", true);
    });
  }

  updateSpecification() {
    const { apiUrl, name, category_id, default_value, lang, id, position } = this.state;

    axios.put(apiUrl + '/' + id, {
      id,
      name,
      default_value,
      lang,
      category_id,
      position,
      shop,
    })
    .then(resp => {
      this.handleToast(resp.data.message, resp.data.error);
    })
    .catch(error => {
      console.log(error);
      this.handleToast("Error happened", true);
    });
  }

  getSpecification() {
    const { apiUrl, id } = this.state;
    axios.get(apiUrl + '/' + id, {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      const data = resp.data;
      this.setState({
        name: data.name,
        default_value: data.default_value !== null ? data.default_value : "",
        category_id: data.category !== null ? data.category.id : this.state.category_id,
        position: data.position,
        lang: resp.data.lang,
      });
    })
    .catch(err => {
      console.log(err);
    });
  }

  getCategories() {
    const { apiUrl, category_id } = this.state;
    axios.get(apiUrl + '-categories', {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      this.setState({
        categories: resp.data,
      });
      if (!category_id && resp.data[0]) {
        this.setState({
          category_id: resp.data[0].value,
        });
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  render() {
    const {
      id,
      name,
      default_value,
      lang,
      category_id,
      categories,
      showToast,
      toastError,
      toastMessage,
      position,
      langArr,
    } = this.state;
    const toastMarkup = showToast ? <Toast
      error={toastError}
      content={toastMessage}
      onDismiss={this.toggleToast} /> : null;

    const specDefaultValue = default_value ? default_value : '';

    const title = id ? "Update specification" : "Add new specification";

    return (
      <Page title={title}>
        <Card sectioned>
          <Form onSubmit={this.handleSubmit} preventDefault={true}>
            <FormLayout>
              <Select
                label="Specification Category"
                options={categories}
                onChange={this.handleChange('category_id')}
                value={category_id}
              />
              <TextField
                value={name}
                onChange={this.handleChange('name')}
                label="Specification name"
                type="text"
              />
              <TextField
                value={default_value}
                onChange={this.handleChange('default_value')}
                label="Specification default value"
                type="text"
                multiline={true}
              />
              <TextField
                  value={position}
                  onChange={this.handleChange('position')}
                  label="Specification position"
                  type="number"
              />
              <Select
                label="Language"
                options={langArr}
                onChange={this.handleChange('lang')}
                value={lang}
                ></Select>
              <Button submit>Submit</Button>
            </FormLayout>
          </Form>
          {toastMarkup}
        </Card>
      </Page>
    );
  }

  // set data for toast
  handleToast(message, error) {
    this.setState({
      toastError: error,
      toastMessage: message,
    });

    // show toast
    this.toggleToast();
  }

  // open/close toast
  toggleToast() {
    this.setState(({ showToast }) => ({ showToast: !showToast }));
  };

  handleSubmit(event) {
    const { name, category_id, id } = this.state;
    let errors = [];
    if (name.trim() == '') {
      errors.push('Specification name can not be blank.');
    }

    if (category_id == '') {
      errors.push('Specification Category can not be blank.');
    }

    if (errors.length) {
      this.handleToast(errors.join(' '), true);
    } else {
      if (id) this.updateSpecification();
      else this.createSecification();
    }
  };

  handleChange(field) {
    return (value) => this.setState({ [field]: value });
  };
}

export default SpecificationForm;