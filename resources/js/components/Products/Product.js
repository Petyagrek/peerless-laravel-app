import React, { Component } from 'react';
import { Page, Card, Toast, Thumbnail, Button, SkeletonBodyText, Form, TextField } from "@shopify/polaris";

import axios from "axios";

export default class Product extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            apiUrl: appUrl+'/api/product',
            product: {},
            specCats: [],
            toastError: null,
            toastMessage: null,
            showToast: false,
        }

        this.getProduct();

        this.toggleToast = this.toggleToast.bind(this);
        this.handleToast = this.handleToast.bind(this);
    }

    getProduct () {
        const { apiUrl, id } = this.state;
        axios.get(apiUrl+'/'+id, {
            params: {
                shop: shop,
            }
        })
        .then(resp => {
            this.setState({
                product: resp.data.product.body.product,
                specCats: resp.data.specCats,
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    render() {
        const { product, specCats, showToast, toastError, toastMessage } = this.state;
        const { title, handle, image } = product;
        const productLink = 'https://'+shop+'/products/'+handle;

        const toastMarkup = showToast ? <Toast 
                error={toastError}
                content={toastMessage} 
                onDismiss={this.toggleToast} /> : null;

        const content = title ? null : <SkeletonBodyText />;

        const media = image ? <Thumbnail
            source={image.src}
            alt={title}
            size="large"
            /> : null;

        const cardHtml = (
            <div>
                <Button 
                    url={productLink}
                    external={true}>Open product</Button>
            </div>
        );

        return (
            <Page title="Product" fullWidth>
                <Card sectioned title={title}>
                    {content}
                    {media}
                    <br />
                    {cardHtml}
                    {this.renderSpecCats(specCats)}
                    {toastMarkup}
                </Card>
            </Page>
        );
    }

    renderSpecCats (specCats) {
        if (!specCats) return null;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)}>
                {specCats.map((catItem, catKey) => this.renderCatItem(catItem, catKey) )}
                <Button submit primary>Save</Button>
            </Form>
        );
    }

    renderCatItem (catItem, catKey) {
        return (
            <div key={catKey} className="cat-block">
                <h4>{catItem.cat_name}</h4>
                {catItem.specification.map((specItem, specKey) => this.renderSpecItem(specItem, specKey, catKey) )}
            </div>
        );
    }

    renderSpecItem(specItem, specKey, catKey) {
        const value = specItem.value 
                ? specItem.value 
                : '';
        return (
            <div key={specKey} className="spec-block">
                <TextField 
                    label={specItem.name}
                    value={value}
                    onChange={this.handleChangeSpecification.bind(this, specKey, catKey)}
                    multiline={true}
                    //onChange={this.handleChangeSpecification(event, specItem, specKey, catKey)}
                    />
            </div>
        );
    }

    handleSubmit() {
        const { apiUrl, id, product, specCats } = this.state;

        axios.put(apiUrl + '/' + id, {
            product: product,
            specCats: specCats,
            shop: shop,
        })
        .then(resp => {
            this.handleToast(resp.data.message, resp.data.error);
        })
        .catch(err => {
            console.log(err);
            this.handleToast("Error happened", true);
        });
    }

    handleChangeSpecification(specKey, catKey) {
        const { specCats } = this.state;
        specCats[catKey].specification[specKey].value = event.target.value;
        this.setState({specCats});
    }
    
    // set data for toast
    handleToast(message, error) {
        this.setState({
            toastError: error,
            toastMessage: message,
        });

        // show toast
        this.toggleToast();
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    };
}