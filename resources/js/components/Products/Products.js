// TODO auth in Laravel

import React, { Component } from "react";
import { Page, Card, ResourceList, TextStyle, Thumbnail, Pagination, Button } from "@shopify/polaris";

import { Link } from "react-router-dom";

import DefaultImage from './DefaultImage'

class Products extends Component {
    constructor() {
        super();
        this.state = {
            error: null,
            isProductsLoading: true,
            searchValue: '',
            serchTimeout: null,
            items: [],
            page_info: null,
            link: {
                next: null,
                previous: null
            },
            productsPerPage: 50,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.isLoading();
        },1000);

        this.getProducts();
    }

    getProducts() {
        const {
            productsPerPage, 
            searchValue, 
            page_info
        } = this.state;
        
        let apiUrl = appUrl+'/api/products?shop='+shop;

        apiUrl += '&productsPerPage=' + productsPerPage;

        if (searchValue)
            apiUrl += '&title='+ searchValue;

        if (page_info)
            apiUrl += '&page_info='+page_info;

        fetch(apiUrl)
        .then(resp => resp.json())
        .then((result) => {
            this.setState({
                isProductsLoading: false,
                items: result.body.products,
                link:  result.link
            });
        },
        (error) => {
            this.setState({
                isProductsLoading: false,
                error
            });
        });
    }

    // search specification method
    handleSearchChange (searchValue) {
        let {serchTimeout} = this.state;
        this.setState({
            searchValue: searchValue, 
            isProductsLoading: true,
            page_info: null
        });
        clearTimeout(serchTimeout)
        
        serchTimeout = setTimeout(() => {
            this.getProducts();
        }, 1000);

        this.setState({serchTimeout});
    }

    render () {
        const { items, isProductsLoading } = this.state;

        const filterControl = (
            <ResourceList.FilterControl
                searchValue={this.state.searchValue}
                onSearchChange={this.handleSearchChange.bind(this)}
            />
        );

        return (
            <Page title="Products" fullWidth>
                <Card sectioned>
                    {this.renderPagination()}
                    <ResourceList
                        resourceName={{singular: 'product', plural: 'products'}}
                        items={items}
                        loading={isProductsLoading}
                        filterControl={filterControl}
                        renderItem={(item) => {
                        const {id, images, handle, title} = item;
                        const media = images.length ? <Thumbnail
                            source={images[0].src}
                            size="small"
                            alt={ title }
                        /> : <DefaultImage />;

                            return (
                                <ResourceList.Item
                                    id={id}
                                    media={media}
                                    accessibilityLabel={`View details for ${name}`}
                                    persistActions
                                >
                                    <div className="flexResourceContent">
                                        <h3>
                                            <TextStyle variation="strong">{title}</TextStyle>
                                        </h3>
                                        <Link className="resourceLink" to={'/product/'+id} ><Button>Open</Button></Link>
                                    </div>
                                </ResourceList.Item>
                            );
                        }}
                    />
                    {this.renderPagination()}
                </Card>
            </Page>
        );
    }

    renderPagination() {
        const {link} = this.state;
        return (
            <div style={{textAlign: 'center'}}>
                <Pagination
                    hasPrevious={link.previous}
                    onPrevious={() => {
                        this.setState({
                            isProductsLoading: true,
                            //items: [],
                            page_info: link.previous,
                        }, () => {
                            this.getProducts();
                        });
                    }}
                    hasNext={link.next} 
                    onNext={() => {
                        this.setState({
                            isProductsLoading: true,
                            //items: [],
                            page_info: link.next,
                        }, () => {
                            this.getProducts();
                        });
                        
                    }}
                />
            </div>
        );
    }
}


export default Products;