import React, { Component } from "react";
import { Page, Card, Button, TextContainer, Heading, EmptyState } from "@shopify/polaris";

class Home extends Component {

    componentDidMount() {
        setTimeout(() => {
            this.props.isLoading();
        },1000);
    }

    render () {
        return (
            <Page title="Peerles app">
                <Card sectioned>
                    <TextContainer>
                        <Heading>Pearless app to manage products</Heading>
                    </TextContainer>
                </Card>
                <EmptyState
                    heading="Manage product specification"
                    action={{content: ''}}
                    image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
                    >
                    </EmptyState>
            </Page>
        );
    }
}

export default Home;