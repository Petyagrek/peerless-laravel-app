import React, { Component } from "react";
import {
  Page,
  Card,
  Form,
  FormLayout,
  TextField,
  Button,
  Toast,
  Tag,
  Select,
  Tabs,
  Checkbox,
  Banner,
} from "@shopify/polaris";
import axios from "axios";

import countyOptions from '../Helpers/CountyOptions';


class CustomerForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      isCustomerLoading: false,
      selectedTab: 0,
      customer: {},
      companyRequest: false,
      companyMeta: {},
      defaultPermissions: {
        company_admin: 0,
        purchasing: 1,
        reviews: 1,
        pricing: '',
        order_history: 'self',
        invoices: 'company',
        user_management: 0,
      },
      defaultCustomer: {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        state: 'enabled',
        tags: '',
        account_type: '',
        company_id: 0,
        industry: '',
        country: '',
        language: '',
        accepts_marketing: true,
        customer_permission: {},
      },
      companies: [
        { value: 0, label: "Please select" }
      ],
      errors: {},
      countyOptions,
      industryOption: [
        { value: "", label: "Please Select" },
        { value: "Airports", label: "Airports" },
        { value: "Boats/RV", label: "Boats/RV" },
        { value: "Broadcast", label: "Broadcast" },
        { value: "Conferencing", label: "Conferencing" },
        { value: "Control Room", label: "Control Room" },
        { value: "Corporate", label: "Corporate" },
        { value: "Education", label: "Education" },
        { value: "Entertainment", label: "Entertainment" },
        { value: "Gaming", label: "Gaming" },
        { value: "Government", label: "Government" },
        { value: "Healthcare/Hospital", label: "Healthcare/Hospital" },
        { value: "Home", label: "Home" },
        { value: "Hospitality", label: "Hospitality" },
        { value: "House of Worship", label: "House of Worship" },
        { value: "Restaurant/Bar", label: "Restaurant/Bar" },
        { value: "Retail", label: "Retail" },
        { value: "Security", label: "Security" },
        { value: "Stadiums", label: "Stadiums" },
        { value: "Transportation", label: "Transportation" },
      ],
      languageOption: [
        { value: "", label: "Please Select" },
        { value: "English", label: "English" },
        { value: "French", label: "French" },
        { value: "Spanish", label: "Spanish" },
        { value: "German", label: "German" },
      ],
      tag: '',
      tags: [],
      showToast: false,
      apiUrl: appUrl + '/api/customer',
      companyApiUrl: appUrl + '/api/customers-companies',
      accoutTypes: ['B2C', 'B2B Admin', 'B2B Join', 'B2B Unidentified'],
    };
  }

  componentDidMount() {
    const { defaultCustomer } = this.state;
    this.getCompanies();

    // this mean we update customer
    if (this.props.match && this.props.match.params.id) {
      this.state.id = this.props.match.params.id;
      // load customer
      this.getCustomer();
    } else {
      this.setState({
        customer: Object.assign({}, defaultCustomer)
      })
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createCustomer = this.createCustomer.bind(this);

    this.toggleToast = this.toggleToast.bind(this);
    this.handleToast = this.handleToast.bind(this);
  }

  createCustomer() {
    const { apiUrl, customer } = this.state;

    axios.post(apiUrl, {
      customer: customer,
      shop: shop,
    })
      .then(resp => {
        this.handleToast(resp.data.message, resp.data.success);
        if (resp.data.error) {
          this.setState({ errors: resp.data.error, isCustomerLoading: false });
        } else {
          customer.id = resp.data.customer.id;
          this.setState({
            errors: {},
            isCustomerLoading: false, 
            id: resp.data.customer.id,
            customer: customer
          });
        }
      })
      .catch(error => {
        this.setState({ isCustomerLoading: false });
        this.handleToast("Error happened", true);
      });
  }

  updateCustomer() {
    const { apiUrl, customer, id } = this.state;

    axios.put(apiUrl + '/' + id, {
      id: id,
      customer: customer,
      shop: shop,
    })
      .then(resp => {
        this.handleToast(resp.data.message, resp.data.success);
        if (resp.data.error) {
          this.setState({ errors: resp.data.error, isCustomerLoading: false });
        } else {
          this.setState({ errors: {}, isCustomerLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({
          isCustomerLoading: false
        });
        this.handleToast("Error happened", false);
      });
  }

  getCustomer() {
    const { apiUrl, id } = this.state;
    axios.get(apiUrl + '/' + id, {
      params: {
        shop: shop,
      }
    })
      .then(resp => {
        const data = resp.data;
        this.setState({
          customer: data.customer,
          companyMeta: data.companyMeta,
          companyRequest: data.companyRequest,
          isCustomerLoading: false,
          tags: data.customer.tags.split(','),
        });
      })
      .catch(err => {
        this.setState({
          isCustomerLoading: false
        });
        this.handleToast("Customer loading error", false);
      });
  }

  getCompanies() {
    const { companyApiUrl, companies } = this.state;
    axios.get(companyApiUrl, {
      params: {
        shop: shop,
      }
    })
      .then(resp => {
        this.setState({
          companies: companies.concat(resp.data),
        });
        // if we do not have any category we select first one
        // if (!customer.company_id && resp.data[0]) {
        //   this.setState({
        //     companyId: resp.data[0].value,
        //   });
        // }
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleTabChange(selectedTabIndex) {
    this.setState({ selectedTab: selectedTabIndex });
  };

  render() {
    const {
      id,
      showToast,
      toastError,
      toastMessage,
      customer,
      companyMeta,
      companyRequest,
      tag,
      selectedTab,
    } = this.state;
    const toastMarkup = showToast ? <Toast
      error={toastError}
      content={toastMessage}
      onDismiss={this.toggleToast} /> : null;

    const title = id ? "Update customer" : "Add new customer";
    const tabs = [
      {
        id: "customer",
        content: "Customer",
        accessibilityLabel: "Customer",
        panelID: 'customer',
        data: this.renderCustomer(),
      }
    ];

    if (customer && customer.account_type == 'B2B Join') {
      tabs.push({
        id: 'customer-permison',
        content: "Customer Permission",
        accessibilityLabel: 'Customer Permission',
        panelID: 'customer-permison',
        data: this.renderCustomerPermison(),
      });
    }

    const requestBanner = companyRequest ? <Banner
      title={"Request to" +
        (customer.account_type == 'B2B Unidentified' ? ' create ' :
          customer.account_type == 'B2B Join' ? ' join ' : '') +
        (companyMeta && companyMeta.value ? '"' + companyMeta.value + '"' : '') + ' company'}
      status="info"
    >
    </Banner> : '';

    return (
      <Page title={title}>
        <Card sectioned>
          {requestBanner}
          <Tabs tabs={tabs} selected={selectedTab} onSelect={this.handleTabChange.bind(this)}>
            <Card.Section title={tabs[selectedTab].accessibilityLabel}>
              {tabs[selectedTab].data}
            </Card.Section>
          </Tabs>
          {toastMarkup}
        </Card>
      </Page>
    );
  }

  renderCustomer() {
    const {
      customer,
      languageOption,
      errors,
      id,
      isCustomerLoading
    } = this.state;
    return (
      <Form onSubmit={this.handleSubmit} preventDefault={true}>
        <FormLayout>
          <Select
            label="Account type"
            options={[
              { value: "", label: "Please Select" },
              { value: "B2C", label: "Consumer" },
              // { value: "B2B Unidentified", label: "Business Unidentified" },
              { value: "B2B Admin", label: "B2B Admin" },
              { value: "B2B Join", label: "B2B Join" },
            ]}
            onChange={this.handleCustomerChange('account_type')}
            value={customer.account_type || ""}
            error={errors['account_type'] ? errors['account_type'] : ''}
          />
          <TextField
            value={customer.first_name || null}
            onChange={this.handleCustomerChange('first_name')}
            label="First Name"
            type="text"
            error={errors['first_name'] ? errors['first_name'] : ''}
          />
          <TextField
            value={customer.last_name || null}
            onChange={this.handleCustomerChange('last_name')}
            label="Last Name"
            type="text"
            multiline={false}
            error={errors['last_name'] ? errors['last_name'] : ''}
          />
          <TextField
            value={customer.email || null}
            onChange={this.handleCustomerChange('email')}
            label="Email"
            type="email"
            error={
              errors['email'] ? errors['email'] == "has already been taken" ? "That email is already associated with an existing user." : errors['email'] : ''
            }
          />
          <TextField
            value={customer.phone || null}
            onChange={this.handleCustomerChange('phone')}
            label="Phone"
            type="text"
            error={errors['phone'] ? errors['phone'] : ''}
          />
          {this.renderTags()}
          {this.isB2CCustomer() ? this.isMainSite() ? '' : this.renderConsumarFields() : this.renderBussinessFields()}
          <Select
            label="Language"
            options={languageOption}
            onChange={this.handleCustomerChange('language')}
            value={customer.language || languageOption[0]}
            error={errors['language'] ? errors['language'] : ''}
          />
          <Checkbox
            checked={customer.accepts_marketing || false}
            label="Newsletter"
            onChange={this.handleCustomerChange('accepts_marketing')}
          />
          {this.showCustomerB2BUindentifiedWarning()}
          <Button loading={isCustomerLoading} submit>{id ? 'Update' : 'Create'}</Button>
        </FormLayout>
      </Form>
    );
  }

  isMainSite() {
    return 'peerless-av.myshopify.com' == shop;
  }

  isB2CCustomer() {
    const customer = this.state.customer;
    return customer && customer.account_type == "B2C"
  }

  renderBussinessFields() {
    const {
      customer,
      companies,
      industryOption,
      countyOptions,
      errors,
    } = this.state;

    customer.country = customer.country || countyOptions[0]['value'];
    customer.industry = customer.industry || industryOption[0]['value'];
    customer.state = customer.state || 'enabled';
    return (
      <>
        {/* <Checkbox
          checked={customer.company_owner || false}
          label="Company Owner"
          onChange={this.handleCustomerChange('company_owner')}
        />
        <br /> */}
        <Select
          label="Country"
          options={countyOptions}
          onChange={this.handleCustomerChange('country')}
          value={customer.country}
        />
        <br />
        <Select
          label="Industry"
          options={industryOption}
          onChange={this.handleCustomerChange('industry')}
          value={customer.industry}
        />
        <br />
        <Select
          label="Company"
          options={companies}
          onChange={this.handleCustomerChange('company_id')}
          value={customer.company_id && !isNaN(parseInt(customer.company_id)) ?
            parseInt(customer.company_id) : companies[0] || companies[0]}
          error={errors['company_id'] ? errors['company_id'] : ''}
        />
        {/* <TextField
          value={customer.company_id || null}
          onChange={this.handleCustomerChange('company_id')}
          label="Company tag"
          type="text"
          error={errors['company_id'] ? errors['company_id'] : ''}
        /> */}
        <br />
        <Select
          label="Status"
          options={[
            { value: 'enabled', label: 'Enabled' },
            { value: 'disabled', label: 'Disabled' },
          ]}
          onChange={this.handleCustomerChange('state')}
          value={customer.state}
        />
      </>
    );
  }

  renderConsumarFields() {
    const {
      customer,
      countyOptions,
      companyMeta,
      errors,
    } = this.state;
    
    customer.company_consumar = customer.company_consumar ? customer.company_consumar : companyMeta ? companyMeta.value : '';
    return (
      <>
        <Select
          label="Country"
          options={countyOptions}
          onChange={this.handleCustomerChange('country')}
          value={customer.country}
        />
        <br />
        <TextField
          value={customer.company_consumar || null}
          onChange={this.handleCustomerChange('company_consumar')}
          label="Company"
          type="text"
        />
      </>
    );
  }

  renderCustomerPermison() {
    const { customer, id, isCustomerLoading } = this.state;
    const customer_permission
      = customer.customer_permission  // need to store data in customer object
      = customer.customer_permission || Object.assign({}, this.state.defaultPermissions);
    return (
      <Form onSubmit={this.handleSubmit} preventDefault={true}>
        <FormLayout>
          <Checkbox
            checked={customer_permission.company_admin || 0}
            label="Company Admin"
            onChange={this.handlePermissionChange('company_admin')}
          />
          <Checkbox
            checked={customer_permission.purchasing || 0}
            label="Purchasing"
            onChange={this.handlePermissionChange('purchasing')}
          />
          <Checkbox
            checked={customer_permission.reviews || 0}
            label="Reviews"
            onChange={this.handlePermissionChange('reviews')}
          />
          {/* <Select
            label="Pricing"
            options={[
              { value: 'special_pricing', label: 'Special Pricing' },
              { value: 'list', label: 'List' },
              { value: 'none', label: 'None' },
            ]}
            onChange={this.handlePermissionChange('pricing')}
            value={customer_permission.pricing || 'list'}
          /> */}
          <Select
            label="Order History"
            options={[
              { value: 'company', label: 'Company' },
              { value: 'self', label: 'Self' },
              { value: 'none', label: 'None' },
            ]}
            onChange={this.handlePermissionChange('order_history')}
            value={customer_permission.order_history || 'self'}
          />
          <Select
            label="Invoices"
            options={[
              { value: 'company', label: 'Company' },
              // { value: 'self', label: 'Self' },
              { value: 'none', label: 'None' },
            ]}
            onChange={this.handlePermissionChange('invoices')}
            value={customer_permission.invoices || 'self'}
          />
          <Checkbox
            checked={customer_permission.user_management || 0}
            label="User management"
            onChange={this.handlePermissionChange('user_management')}
          />
          {this.showCustomerB2BUindentifiedWarning()}
          <Button loading={isCustomerLoading} submit>{id ? 'Update' : 'Create'}</Button>
        </FormLayout>
      </Form>
    );
  }

  showCustomerB2BUindentifiedWarning()
  {
    const {customer} = this.state;
    return (
      customer.account_type == 'B2B Admin' && customer.company_id == '0' ?
      <Banner
        title='If you did not specified company for "B2B Admin" customer it will become "B2B Uindentified"'
        status="warning">
      </Banner> 
      : ''
    );
  }

  renderTags() {
    const { tags, tag, accoutTypes } = this.state;
    return (
      <div className="tags">
        <div>
          <div className="Polaris-Labelled__LabelWrapper">
            <div className="Polaris-Label">
              <label id="Tags" htmlFor="Tags" className="Polaris-Label__Text">Tags</label>
            </div>
          </div>
          <div className="Polaris-TextField Polaris-TextField--hasValue">
            <input id="Tags"
              className="Polaris-TextField__Input"
              type="text"
              aria-label="Tags"
              aria-labelledby="TextField22Label"
              aria-invalid="false"
              value={tag}
              onChange={this.handleChangeTags.bind(this)}
              onKeyDown={this.handleTagKeyDown.bind(this)}
            />
            <div className="Polaris-TextField__Backdrop"></div>
          </div>
        </div>

        {tags ? tags.map((tag, tagKey) => {
          if (!accoutTypes.includes(tag) && tag.indexOf('CompanyID') === -1)
            return <Tag key={tagKey} onRemove={(e) => this.removeTag(e, tagKey)}>{tag.trim()}</Tag>
        }) : ''}
      </div>
    );
  }

  removeTag(e, tagKey) {
    const { tags, customer } = this.state;
    tags.splice(tagKey, 1);
    customer.tags = tags.join(', ');
    this.setState({
      tags: tags,
      customer: customer,
    });
  }

  // update customer data
  handleCustomerChange(field) {
    const { customer } = this.state;
    return (value) => {
      customer[field] = value;
      this.setState({ customer: customer });
    };
  }

  // update permission data
  handlePermissionChange(field) {
    const { customer } = this.state;
    return (value) => {
      customer.customer_permission[field] = value;
      this.setState({ customer: customer });
    };
  }

  handleTagKeyDown(e) {
    if (e.key != "Enter") return;
    e.preventDefault();
    let { tag, tags, customer } = this.state;
    // tag = tag.replace(',', '\,');
    tags.push(tag);
    customer.tags = tags.join(', ');
    tag = '';
    this.setState({
      tag: tag,
      tags: tags,
      customer: customer,
    });
  }

  // add tag to customer tags
  handleChangeTags(e) {
    this.setState({
      tag: e.currentTarget.value
    });
  }

  // set data for toast
  handleToast(message, success) {
    this.setState({
      toastError: !success,
      toastMessage: message,
    });

    // show toast
    this.toggleToast();
  }

  // open/close toast
  toggleToast() {
    this.setState(({ showToast }) => ({ showToast: !showToast }));
  };

  validateCustomerForm() {
    const { customer } = this.state;
    let errors = {};
    if (!customer.first_name || customer.first_name.trim() == '')
      errors['first_name'] = 'First Name can not be blank.'

    if (!customer.last_name || customer.last_name.trim() == '')
      errors['last_name'] = 'Last Name can not be blank.';

    if (!customer.email || customer.email.trim() == '')
      errors['email'] = 'Email can not be blank.';

    if ((customer.account_type == 'B2B Join' || customer.account_type == 'B2B Admin') && !customer.phone)
      errors['phone'] = 'Phone can not be blank.';

    if (!customer.language || customer.language.trim() == '')
      errors['language'] = 'Please select language.';

    if (customer.account_type == 'B2B Join' && !customer.company_id)
      errors['company_id'] = 'Please select company for joined user';

    if (!customer.account_type || customer.account_type.trim() == '')
      errors['account_type'] = "Please select account type.";

    return errors;
  }

  handleSubmit(event) {
    const { customer, id } = this.state;
    // if (customer.account_type == 'B2B Join' && !customer.customer_permission)
      // customer.customer_permission = Object.assign({}, this.state.defaultPermissions);

    const errors = this.validateCustomerForm();
    this.setState({'errors': errors});
    if (objLength(errors)) {
      this.handleToast('Please fill all needed data', false);
    } else {
      this.setState({
        isCustomerLoading: true
      });
      if (id) this.updateCustomer();
      else this.createCustomer();
    }
  };

  handleChange(field) {
    return (value) => this.setState({ [field]: value });
  };
}

export default CustomerForm;