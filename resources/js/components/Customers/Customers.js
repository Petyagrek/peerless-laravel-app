import React, { Component } from 'react';
import { Page, Card, ResourceList, TextStyle, Avatar, Button, Modal, TextContainer, Toast, SettingToggle, Tabs } from "@shopify/polaris";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

export default class Customer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: 0,
      searchValue: '',
      selectedItems: [],
      items: [],
      createCompanyCustomers: [],
      joinCompanyCustomers: [],
      consumerCustomers: [],
      redirectToAddCustomer: false,
      apiUrl: appUrl + '/api/customers',
      deleteUrl: appUrl + '/api/customers',
      modalActive: false,
      showToast: false,
      toastError: false,
      toastMessage: '',
      isCustomersLoading: true,
      searchTiemout: null,
    }

    this.getCustomers();
  }

  getCustomers() {
    const {
      apiUrl,
      searchValue
    } = this.state;
    axios.get(apiUrl, {
      params: {
        shop: shop,
        searchValue: searchValue,
      }
    })
    .then(resp => {
      // if (resp.data.length == 0) return;

      const createCompanyCustomers = this.sortCustomers(resp.data, 'createCompanyCustomers');
      const joinCompanyCustomers   = this.sortCustomers(resp.data, 'joinCompanyCustomers');
      const consumerCustomers = this.sortCustomers(resp.data, 'consumers')

      this.setState({
        items: resp.data,
        isCustomersLoading: false,
        createCompanyCustomers: createCompanyCustomers,
        joinCompanyCustomers: joinCompanyCustomers,
        consumerCustomers: consumerCustomers,
      });
    })
    .catch(err => {
      console.log(err);
      this.handleToast('Error happened. Please reload and try again!', true);
    })
  }

  sortCustomers(customers, type)
  {
    if (type == 'createCompanyCustomers') {
      return customers.filter(customer => {
        return customer.account_type == 'B2B Unidentified';
      });
    } else if (type == 'joinCompanyCustomers') {
      return customers.filter(customer => {
        return customer.account_type == "B2B Join" && customer.company_id == null;
      });
    } else if (type == 'consumers') {
      return customers.filter(customer => {
        return customer.account_type == "B2C";
      });
    }
  }

  handleTabChange(selectedTabIndex)  {
    this.setState({selected: selectedTabIndex});
  };

  // search customers method
  handleSearchChange(searchValue) {
    // stop serch timeout if we keep typing
    if (searchValue != this.state.searchValue)
      clearTimeout(this.state.searchTiemout);

    var searchTiemout = setTimeout(() => {
      this.getCustomers();
    }, 1000);

    this.setState({
      searchValue: searchValue,
      isCustomersLoading: true,
      searchTiemout: searchTiemout,
    });
  }

  // trigger when change items
  handleSelectionChange(selectedItems) {
    this.setState({
      selectedItems: selectedItems
    });
  }

  /// trigger when click create category btn
  handleAddCustomerRedirect() {
    const { redirectToAddCustomer } = this.state;
    this.setState({
      redirectToAddCustomer: !redirectToAddCustomer
    })
  }

  /// open/close modal
  handleModalChange() {
    this.setState(({ modalActive }) => ({ modalActive: !modalActive }));
  };

  // set data for toast
  handleToast(message, error) {
    this.setState({
      toastError: error,
      toastMessage: message,
    });

    // show toast
    this.toggleToast();
  }

  // open/close toast
  toggleToast() {
    this.setState(({ showToast }) => ({ showToast: !showToast }));
  };

  // thigger when delete category
  handleDelete() {
    const { deleteUrl, selectedItems, items } = this.state;

    axios.delete(deleteUrl, {
      params: {
        items: selectedItems,
        shop: shop,
      }
    })
    .then((resp) => {
      this.handleToast("Succesfuly deleted", false);

      // close modal
      this.handleModalChange();

      this.setState(({ prevState }) => ({
        items: items.filter(item => selectedItems.indexOf(item.id) === -1),
        selectedItems: [],
      }));
    })
    .catch(err => {
      console.log(err)
      this.handleToast("Error happened", true);

      // close modal
      this.handleModalChange()
    });
  }

  render() {
    const {
      redirectToAddCustomer,
      items,
      selectedItems,
      modalActive,
      showToast,
      toastError,
      toastMessage,
    } = this.state;

    const toastMarkup = showToast ? <Toast content={toastMessage} onDismiss={this.toggleToast.bind(this)} error={toastError} /> : null;

    const resourceName = {
      singular: 'customer',
      plural: 'customers',
    };

    const selectedItemsCount = selectedItems.length;

    const modalTitle = selectedItemsCount > 1
      ? `Delete ${selectedItemsCount} ${resourceName.plural}?`
      : `Delete ${selectedItemsCount} ${resourceName.singular}?`;

    const primaryAction = {
      content: "Add Customer",
      onAction: () => this.handleAddCustomerRedirect(),
    }

    if (redirectToAddCustomer) {
      return <Redirect to='/customer/create' />;
    }
    return (
      <Page title="Customers" primaryAction={primaryAction}>
        <Card sectioned>
          {this.renderTabs()}
        </Card>

        <Modal
          open={modalActive}
          onClose={this.handleModalChange}
          title={modalTitle}
          primaryAction={{
            content: 'Delete',
            onAction: this.handleDelete.bind(this)
          }}
          secondaryActions={[
            {
              content: 'Cancel',
              onAction: this.handleModalChange.bind(this),
            },
          ]}
        >
          <Modal.Section>
            <TextContainer>
              <p>Deleted customer cannot be recovered. Do you still want to continue?</p>
            </TextContainer>
          </Modal.Section>
        </Modal>
        {toastMarkup}
      </Page>
    );
  }

  renderTabs() {
    const {
      selected, 
      items, 
      createCompanyCustomers, 
      joinCompanyCustomers,
      consumerCustomers,
    } = this.state;
    const tabs = [
      {
        id: 'all-customers',
        content: "All",
        accessibilityLabel: 'All customers',
        panelID: 'all-customers-content',
        data: this.renderCustomers(items),
      },
      {
        id: 'new-customers',
        content: "Pending New B2B",
        accessibilityLabel: 'Pending New B2B',
        panelID: 'create-customers-content',
        data: this.renderCustomers(createCompanyCustomers),
      },
      {
        id: 'join-customers',
        content: "Pending Join B2B",
        accessibilityLabel: 'Pending Join B2B',
        panelID: 'join-customers-content',
        data: this.renderCustomers(joinCompanyCustomers)
      },
      {
        id: 'consumers',
        content: "B2C Users",
        accessibilityLabel: 'B2C Users',
        panelID: 'consumers',
        data: this.renderCustomers(consumerCustomers)
      },
    ];
    return (
      <Tabs tabs={tabs} selected={selected} onSelect={this.handleTabChange.bind(this)}>
        <Card.Section title={tabs[selected].accessibilityLabel}>
          {tabs[selected].data}
        </Card.Section>
      </Tabs>
    );
  }

  renderCustomers(customers) {
    const {isCustomersLoading} = this.state;
    const resourceName = {
      singular: 'customer',
      plural: 'customers',
    };

    const filterControl = (
      <ResourceList.FilterControl
          searchValue={this.state.searchValue}
          onSearchChange={this.handleSearchChange.bind(this)}
      />
    );
    const bulkActions = [
      {
        content: 'Delete customer',
        onAction: () => this.handleModalChange(),
      },
    ];
    return (
      <ResourceList
        resourceName={resourceName}
        loading={isCustomersLoading}
        items={customers}
        renderItem={this.renderItem}
        selectedItems={this.state.selectedItems}
        onSelectionChange={this.handleSelectionChange.bind(this)}
        bulkActions={bulkActions}
        filterControl={filterControl}
      >
    </ResourceList>
    );
  }

  renderItem(item) {
    const { id, first_name, last_name, company, email } = item;
    const media = <Avatar customer size="medium" name={first_name} />;

    return (
      <ResourceList.Item
        id={id}
        media={media}
        accessibilityLabel={`View details for ${first_name}`}
      >
        <div className="flexResourceContent">
          <h3>
            <TextStyle variation="strong">{first_name} {last_name}</TextStyle>
            <div><TextStyle variation="subdued">Email: {email}</TextStyle></div>
            {company && company.name ? 
              <div><TextStyle variation="subdued">Company: {company.name}</TextStyle></div> : ''}
            {company && company.ifs_id ? 
              <div><TextStyle variation="subdued">IFS ID: {company.ifs_id}</TextStyle></div> : ''}
          </h3>
          <div>
            <Link className="resourceLink" to={'/customer/update/' + id} ><Button>Open</Button></Link>
          </div>
        </div>
      </ResourceList.Item>
    );
  }
}