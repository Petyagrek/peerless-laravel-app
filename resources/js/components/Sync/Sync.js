import React, { Component } from "react";
import { Button } from "@shopify/polaris";

import axios from "axios";

export default class Sync extends Component {
  constructor () {
    super();
    this.state = {
      invoiceSync: false,
      langifySync: false,
    }
  }

  syncInvoices() {
    this.setState({invoiceSync: true});

    axios.get(`${appUrl}/api/invoices/sync`, {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      this.props.updateToast(resp.data.message, true, !resp.data.succes);

      this.setState({invoiceSync: false});
    })
    .catch(error => {
      console.log(error);
      this.props.updateToast("Error happened", true, true);

      this.setState({invoiceSync: false});
    });
  }

  syncLangify() {
    this.setState({langifySync: true});

    axios.get(`${appUrl}/api/langify/sync`, {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      this.props.updateToast(resp.data.message, true, !resp.data.success);

      this.setState({langifySync: false});
    })
    .catch(error => {
      console.log(error);
      this.props.updateToast("Error happened", true, true);

      this.setState({langifySync: false});
    });
  }

  render () {
    const { invoiceSync, langifySync } = this.state;
    return (
      <ul className="Polaris-Navigation__Section">
        <li className="Polaris-Navigation__ListItem" style={{
          padding: '0 10px',
          margin: '0 0 10px'
        }}>
          <Button loading={invoiceSync} onClick={this.syncInvoices.bind(this)} fullWidth>Sync Invoices</Button>
        </li>
        <li className="Polaris-Navigation__ListItem" style={{
          padding: '0 10px'
        }}>
          <Button loading={langifySync} onClick={this.syncLangify.bind(this)} fullWidth>Sync Langify</Button>
        </li>
      </ul>
    );
  }
}