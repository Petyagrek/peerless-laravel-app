import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ReactDOM, {Link} from 'react-dom';
import { AppProvider, Frame, TopBar, Card, ActionList, Loading } from '@shopify/polaris';

import PageNavigation from './Helpers/PageNavigation';
// import Icons from "./Helpers/Icons";
import Products from './Products/Products';
import Product from './Products/Product';
import SpecificationCategories from "./Specifications/SpecificationCategories";
import SpecificationCateforyForm from "./Specifications/SpecificationCateforyForm";
import Specifications from "./Specifications/Specifications";
import SpecificationForm from "./Specifications/SpecificationForm";
import Customers from "./Customers/Customers";
import CustomerForm from "./Customers/CustomerForm";
import Companies from "./Companies/Companies";
import CompanyForm from "./Companies/CompanyForm";
import Home from './Home/Home';

import '@shopify/polaris/styles.css';

export default class AppRouter extends Component {

    constructor() {
        super();

        this.defaultState = {
            emailFieldValue: 'dharma@jadedpixel.com',
            nameFieldValue: 'Jaded Pixel',
        };

        this.state = {
            showToast: false,
            isLoading: false,
            isDirty: false,
            searchActive: true,
            searchText: '',
            userMenuOpen: false,
            showMobileNavigation: false,
            modalActive: false,
            nameFieldValue: this.defaultState.nameFieldValue,
            emailFieldValue: this.defaultState.emailFieldValue,
            storeName: this.defaultState.nameFieldValue,
            supportSubject: '',
            supportMessage: '',
        };
    }

    handleSearchResultsDismiss() {
        this.setState(() => {
            return {
                searchActive: false,
                searchText: '',
            };
        });
    };

    toggleState(key) {
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };
    
    render () {
        const {
            showToast,
            isLoading,
            isDirty,
            searchActive,
            searchText,
            userMenuOpen,
            showMobileNavigation,
            nameFieldValue,
            emailFieldValue,
            modalActive,
            storeName,
        } = this.state;

        const navigationMarkup = (
            <PageNavigation isLoading={this.toggleState('isLoading')} />
        );

        const topBarMarkup = (
            <TopBar
                showNavigationToggle={true}
                userMenu={userMenuMarkup}
                searchResultsVisible={searchActive}
                searchField={searchFieldMarkup}
                searchResults={searchResultsMarkup}
                onSearchResultsDismiss={this.handleSearchResultsDismiss}
                onNavigationToggle={this.toggleState('showMobileNavigation')}
            />
        );

        const userMenuMarkup = (
            <TopBar.UserMenu
                actions={userMenuActions}
                name="Dharma"
                detail={storeName}
                initials="D"
                open={userMenuOpen}
                onToggle={this.toggleState('userMenuOpen')}
            />
        );

        const searchFieldMarkup = (
            <TopBar.SearchField
                onChange={this.handleSearchFieldChange}
                value={searchText}
                placeholder="Search"
            />
        );

        const searchResultsMarkup = (
            <Card>
                <ActionList
                    items={[
                        {content: 'Shopify help center'},
                        {content: 'Community forums'},
                    ]}
                />
            </Card>
        );

        const userMenuActions = [
            {
                items: [{content: 'Community forums'}],
            },
        ];

        const theme = {
            colors: {
                topBar: {
                    background: '#357997',
                },
            },
            logo: {
                width: 124,
                topBarSource:
                    '//cdn.shopify.com/s/files/1/0026/4083/8771/files/Peerless-AV_Logo_Red-PRINT_promo_items_462x.png',
                contextualSaveBarSource:
                '//cdn.shopify.com/s/files/1/0026/4083/8771/files/Peerless-AV_Logo_Red-PRINT_promo_items_462x.png',
                url: 'https://peerless-av.myshopify.com/',
                accessibilityLabel: 'Peerless AV',
            },
        };

        const loadingMarkup = isLoading ? <Loading /> : null;

        return (
            <AppProvider theme={theme} linkComponent={Link}>
                <Router>
                    <div>
                        <Frame
                            topBar={topBarMarkup}
                            navigation={navigationMarkup}
                            showMobileNavigation={showMobileNavigation}
                            onNavigationDismiss={this.toggleState('showMobileNavigation')}
                        >
                            {loadingMarkup}
                            <Route 
                                path="/home" 
                                render={(props) => <Home {...this.props} isLoading={this.toggleState('isLoading')} />}
                                />
                            <Route 
                                path="/products" 
                                render={(props) => <Products {...this.props} isLoading={this.toggleState('isLoading')} />} 
                                />
                            <Route 
                                path="/product/:id" 
                                component={Product}
                                />
                            <Route 
                                path="/specification-categories" 
                                render={(props) => <SpecificationCategories {...this.props} isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/specification-category/create" 
                                render={(props) => <SpecificationCateforyForm {...this.props} isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/specification-category/update/:id" 
                                component={SpecificationCateforyForm}
                                />
                            <Route 
                                path="/specifications" 
                                render={(props) => <Specifications {...this.props} isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/specification/create" 
                                render={(props) => <SpecificationForm {...this.props} isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/specification/update/:id" 
                                component={SpecificationForm}
                                />
                            <Route 
                                exact 
                                path="/"
                                component={Customers}
                                />
                            <Route 
                                path="/customer/create" 
                                render={(props) => <CustomerForm 
                                    {...props} 
                                    isLoading={this.state.isLoading} 
                                    isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/customer/update/:id" 
                                component={(props) => <CustomerForm 
                                    {...props} 
                                    isLoading={this.state.isLoading} 
                                    toggleLoading={this.toggleState('isLoading')} />}
                                />
                            <Route 
                                path="/companies"
                                render={(props) => <Companies  {...this.props} isLoading={this.toggleState('isLoading')} />}
                                />
                            <Route 
                                path="/company/create" 
                                render={(props) => <CompanyForm {...this.props} isLoading={this.toggleState('isLoading')} /> }
                                />
                            <Route 
                                path="/company/update/:id" 
                                component={CompanyForm}
                                />
                            {/* <Route 
                                path="/icons" 
                                component={Icons}
                                /> */}
                        </Frame>
                    </div>
                </Router>
            </AppProvider>
        );
    }
}


if (document.getElementById('root')) {
    ReactDOM.render(<AppRouter />, document.getElementById('root'));
}
