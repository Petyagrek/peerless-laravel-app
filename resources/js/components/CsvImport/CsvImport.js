import React from 'react';
import { Icon, Button, Popover, ActionList, Toast } from '@shopify/polaris';

import { CsvImportService } from '../../common/api.service';

export default class CsvImport extends React.Component {

    constructor() {
        super()

        this.state = {
            active: false,
            isLoading: false,
            percentUploaded: '0',
            uploadType: '',

            toastError: null,
            toastMessage: null,
            showToast: false,
        };

        this.toggleToast = this.toggleToast.bind(this);
    }
    
    togglePopover() {
        this.setState(({active}) => {
            return {active: !active};
        });
    }

    componentDidMount() { }

    uploadCSVBtn(item) {
        this.inputOpenFileRef.click();
        this.setState({ uploadType: item })
    }

    fileInputChanged(e) {
        const { uploadType } = this.state;
        if(window.confirm('Are you sure?')) {
            this.uploadFromBlob(e.target.files[0], this[`upload${uploadType}`].bind(this));
        } else {
            this.setState({ uploadType: '' });
        }
    }

    uploadFromBlob(file, callback) {
        let formData = new FormData();
        formData.append('file', file);
        formData.append('file_type', file.type);
        callback(formData);
    }

    uploadVesa(formdata) {
        this.setState({ isLoading: true });
        CsvImportService.uploadVesa(formdata, (e) => {
            this.calcPercentUploaded(e.loaded, e.total);
            this.setState({
                percentUploaded: this.calcPercentUploaded(e.loaded, e.total)
            })
        })
        .then((resp) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: resp.data.error,
                toastMessage: resp.data.message,
                showToast: true,
            });
        })
        .catch((err) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: true,
                toastMessage: "Error happened",
                showToast: true,
            });
        });
    }

    uploadMounts(formdata) {
        this.setState({ isLoading: true });
        CsvImportService.uploadMounts(formdata, (e) => {
            this.calcPercentUploaded(e.loaded, e.total);
            this.setState({
                percentUploaded: this.calcPercentUploaded(e.loaded, e.total)
            })
        })
        .then((resp) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: resp.data.error,
                toastMessage: resp.data.message,
                showToast: true,
            });
        })
        .catch((err) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: true,
                toastMessage: "Error happened",
                showToast: true,
            });
        });
    }

    uploadAdaptors(formdata) {
        this.setState({ isLoading: true });
        CsvImportService.uploadAdaptors(formdata, (e) => {
            this.calcPercentUploaded(e.loaded, e.total);
            this.setState({
                percentUploaded: this.calcPercentUploaded(e.loaded, e.total)
            })
        })
        .then((resp) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: resp.data.error,
                toastMessage: resp.data.message,
                showToast: true,
            });
        })
        .catch((err) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: true,
                toastMessage: "Error happened",
                showToast: true,
            });
        });
    }

    uploadExceptions(formdata) {
        this.setState({ isLoading: true });
        CsvImportService.uploadExceptions(formdata, (e) => {
            this.calcPercentUploaded(e.loaded, e.total);
            this.setState({
                percentUploaded: this.calcPercentUploaded(e.loaded, e.total)
            })
        })
        .then((resp) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: resp.data.error,
                toastMessage: resp.data.message,
                showToast: true,
            });
        })
        .catch((err) => {
            this.setState({ 
                isLoading: false,
                percentUploaded: '0',
                toastError: true,
                toastMessage: "Error happened",
                showToast: true,
            });
        });
    }

    calcPercentUploaded(current, total) {
        return ((current/total) * 100).toFixed(1);
    }

    renderPopover() {
        const activator = (
            <Button fullWidth onClick={this.togglePopover.bind(this)}>
                Import CSV... <div className="icon-csv-upload"><Icon source="chevronDown" /></div>
            </Button>
        );
        return (
            <div style={{height: '250px', paddingLeft: '10px', paddingRight: '10px'}}>
            <input onChange={ this.fileInputChanged.bind(this) } ref={(ref) => this.inputOpenFileRef = ref} type="file" style={{display:"none"}} accept=".csv" />
                <Popover
                    active={this.state.active}
                    activator={activator}
                    onClose={this.togglePopover.bind(this)}
                >
                <ActionList
                    sections={[
                    {
                        items: [
                        {
                            content: 'Import VESA',
                            helpText: 'Vesa data for filters.',
                            onAction: this.uploadCSVBtn.bind(this, 'Vesa')
                        },
                        {
                            content: 'Update MountFinder: Compatibility',
                            helpText: 'Mounts data for filters.',
                            onAction: this.uploadCSVBtn.bind(this, 'Mounts')
                        },
                        {
                            content: 'Update MountFinder: Adaptors',
                            helpText: 'Adaptors data for mountfinder.',
                            onAction: this.uploadCSVBtn.bind(this, 'Adaptors')
                        },
                        {
                            content: 'Update MountFinder: Exceptions',
                            helpText: 'Which mounts/adaptors should be excluded.',
                            onAction: this.uploadCSVBtn.bind(this, 'Exceptions')
                        },
                        ],
                    },
                    ]}
                />
                </Popover>
            </div>
        );
    }

    render () {
        const { isLoading, percentUploaded, uploadType, toastError, toastMessage, showToast, } = this.state;

        const toastMarkup = showToast ? <Toast 
                error={toastError}
                content={toastMessage} 
                onDismiss={this.toggleToast} /> : null;
        const importContent = !isLoading ? (
                this.renderPopover()
            ) : ( <div style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                <div className="Polaris-Button Polaris-Button--fullWidth Polaris-Button--disabled csv-import-preloader">
                    <div className="csv-import-progress" style={{ width: `${percentUploaded}%` }}></div>
                    <div className="csv-progress-info">
                        <div>{ `${percentUploaded}%` }</div>
                        <div><small>Import {uploadType}...</small></div>
                    </div>
            </div></div>);
        return (
            <>
                {importContent}
                {toastMarkup}
            </>
        );
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    }
}