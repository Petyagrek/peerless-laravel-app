import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { AppProvider, Page, Card, Button, Loading } from '@shopify/polaris';

export default class Example extends Component {
    render() {
        return (
            <AppProvider>
                <Page title="Example app">
                    <Card>
                        <Button onClick={() => alert('Button clicked')}>Press button</Button>
                    </Card>
                </Page>
            </AppProvider>
        );
    }
}

