import React, { Component } from 'react';
import { Icon, Badge, Toast, Button } from '@shopify/polaris';
import {
  SettingsMinor
} from '@shopify/polaris-icons';

import { NavLink } from 'react-router-dom';
import axios from "axios";

import CsvImport from '../CsvImport/CsvImport';
import Sync from "../Sync/Sync";

class PageNavigation extends Component {
  constructor(isLoading) {
    super();
    this.isLoading = isLoading;

    this.state = {
      productCount: 0,
      error: null,
      showToast: false,
      toastMessage: '',
      toastError: false,
    };
  }

  componentDidMount() {
    setTimeout(() => { this.props.isLoading(); }, 1000);

    fetch(appUrl + '/api/products-count?shop=' + shop)
      .then(resp => resp.json())
      .then((result) => {
        this.setState({
          productCount: result.body.count
        });
      },
        (error) => {
          this.setState({
            error
          });
        })
  }

  importOrders() {
    axios.get(`${appUrl}/api/order_history/import`, {
      params: {
        shop: shop,
      }
    })
      .then(resp => {
        // this.handleToast(resp.data.message, resp.data.error);
      })
      .catch(error => {
        console.log(error);
        // this.handleToast("Error happened", true);
      });
  }

  updateCustomers() {
    axios.get(`${appUrl}/api/customers/update`, {
      params: {
        shop: shop,
      }
    })
      .then(resp => {
        console.log(resp);
      })
      .catch(error => {
        console.log(error);
      });
  }

  createCarrierShipping() {
    axios.post(`${appUrl}/api/carrier-sipping/create`, {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      console.log(resp);
    })
    .catch(error => {
      console.log(error);
    });
  }

  deleteCarrierShipping() {
    axios.delete(`${appUrl}/api/carrier-sipping`, {
      params: {
        shop: shop,
      }
    })
    .then(resp => {
      console.log(resp);
    })
    .catch(error => {
      console.log(error);
    });
  }

  toggleToast() {
    this.setState(({ showToast }) => ({ showToast: !showToast }));
  };

  updateToast(toastMessage, showToast, toastError) {
    this.setState({
      toastMessage,
      showToast,
      toastError
    })
  }

  render() {
    const { isLoading } = this.props;
    const { productCount, showToast, toastMessage, toastError } = this.state;
    const productBadge = productCount > 0 ? <Badge>{productCount}</Badge> : null;
    const toastMarkup = showToast ? <Toast content={toastMessage} error={toastError}
      onDismiss={this.toggleToast.bind(this)} />
      : null;
    return (
      <div>
        <nav className="Polaris-Navigation">
          <div className="Polaris-Navigation__UserMenu"></div>
          <div className="Polaris-Navigation__PrimaryNavigation Polaris-Scrollable Polaris-Scrollable--vertical">
            <ul className="Polaris-Navigation__Section">
              <li className="Polaris-Navigation__SectionHeading">
                <span className="Polaris-Navigation__Text">Navigation</span>
              </li>
              {/* <li className="Polaris-Navigation__ListItem">
                <NavLink 
                  onClick={isLoading} 
                  className="Polaris-Navigation__Item" 
                  to="/">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="home" />
                  </div>
                  <span className="Polaris-Navigation__Text">Home</span>
                </NavLink>
              </li> */}
              <li className="Polaris-Navigation__ListItem">
                <NavLink onClick={isLoading} className="Polaris-Navigation__Item" to="/products">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="products" />
                  </div>
                  <span className="Polaris-Navigation__Text">Products</span>
                  {productBadge}
                </NavLink>
              </li>
              <li className="Polaris-Navigation__ListItem">
                <NavLink
                  onClick={isLoading}
                  className="Polaris-Navigation__Item"
                  to="/specification-categories">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="notes" />
                  </div>
                  <span className="Polaris-Navigation__Text">Specification Categories</span>
                </NavLink>
              </li>
              <li className="Polaris-Navigation__ListItem">
                <NavLink
                  onClick={isLoading}
                  className="Polaris-Navigation__Item"
                  to="/specifications">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="onlineStore" />
                  </div>
                  <span className="Polaris-Navigation__Text">Specifications</span>
                </NavLink>
              </li>
              <li className="Polaris-Navigation__ListItem">
                <NavLink
                  onClick={isLoading}
                  className="Polaris-Navigation__Item"
                  to="/">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="profile" />
                  </div>
                  <span className="Polaris-Navigation__Text">Customers</span>
                </NavLink>
              </li>
              <li className="Polaris-Navigation__ListItem">
                <NavLink
                  onClick={isLoading}
                  className="Polaris-Navigation__Item"
                  to="/companies">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="conversation" />
                  </div>
                  <span className="Polaris-Navigation__Text">Companies</span>
                </NavLink>
              </li>
              {/* <li className="Polaris-Navigation__ListItem">
                <NavLink
                  onClick={isLoading}
                  className="Polaris-Navigation__Item"
                  to="/settings">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source={SettingsMinor} />
                  </div>
                  <span className="Polaris-Navigation__Text">Settings</span>
                </NavLink>
              </li> */}
              {/* <li className="Polaris-Navigation__ListItem">
                <NavLink 
                  onClick={isLoading} 
                  className="Polaris-Navigation__Item" 
                  to="/icons">
                  <div className="Polaris-Navigation__Icon">
                    <Icon source="products" />
                  </div>
                  <span className="Polaris-Navigation__Text">Icons</span>
                </NavLink>
              </li> */}
              {/* <li className="Polaris-Navigation__ListItem">
                <Button onClick={this.importOrders} >Import Orders</Button>
              </li> */}
              {/* <li className="Polaris-Navigation__ListItem">
                <Button onClick={this.updateCustomers} >Update Customers</Button>
              </li> */}
              {/* <li className="Polaris-Navigation__ListItem">
                <Button onClick={this.createCarrierShipping} >Create Carrier Shipping</Button>
              </li>

              <li className="Polaris-Navigation__ListItem">
                <Button onClick={this.deleteCarrierShipping} >Delete Carrier Shipping</Button>
              </li> */}
              
            </ul>

            <Sync  updateToast={this.updateToast.bind(this)}/>

            <CsvImport />

          </div>
        </nav>
        {toastMarkup}
      </div>
    );
  }
}

export default PageNavigation;