import React, { Component } from "react";
import { Page, Card, Icon } from "@shopify/polaris";


export default class Icons extends Component {
    render() {
        return (
            <Page fullWidth>
                <Card>
                    <Icon source="add" />
                    <Icon source="alert" />
                    <Icon source="arrowDown" />
                    <Icon source="arrowLeft" />
                    <Icon source="arrowRight" />
                    <Icon source="arrowUp" />
                    <Icon source="arrowUpDown" />
                    <Icon source="calendar" />
                    <Icon source="cancel" />
                    <Icon source="cancelSmall" />
                    <Icon source="caretDown" />
                    <Icon source="caretUp" />
                    <Icon source="checkmark" />
                    <Icon source="chevronDown" />
                    <Icon source="chevronLeft" />
                    <Icon source="chevronRight" />
                    <Icon source="chevronUp" />
                    <Icon source="circleCancel" />
                    <Icon source="circleChevronDown" />
                    <Icon source="circleChevronLeft" />
                    <Icon source="circleChevronRight" />
                    <Icon source="circleChevronUp" />
                    <Icon source="circleInformation" />
                    <Icon source="circlePlus" />
                    <Icon source="circlePlusOutline" />
                    <Icon source="conversation" />
                    <Icon source="delete" />
                    <Icon source="disable" />
                    <Icon source="dispute" />
                    <Icon source="duplicate" />
                    <Icon source="embed" />
                    <Icon source="export" />
                    <Icon source="external" />
                    <Icon source="help" />
                    <Icon source="home" />
                    <Icon source="horizontalDots" />
                    <Icon source="import" />
                    <Icon source="logOut" />
                    <Icon source="notes" />
                    <Icon source="notification" />
                    <Icon source="onlineStore" />
                    <Icon source="orders" />
                    <Icon source="print" />
                    <Icon source="products" />
                    <Icon source="profile" />
                    <Icon source="refresh" />
                    <Icon source="risk" />
                    <Icon source="save" />
                    <Icon source="search" />
                    <Icon source="subtract" />
                    <Icon source="view"/>
                </Card>
            </Page>
        );
    }
}