import React, { Component } from "react";
import { Page, Card, ResourceList, TextStyle, Avatar, Button, Modal, TextContainer, Toast, SettingToggle } from "@shopify/polaris";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";


class Companies extends Component {

    constructor () {
        super();
        this.state = {
            searchValue: '',
            selectedItems: [],
            items: [],
            redirectToAddCopmany: false,
            apiUrl: appUrl+'/api/companies',
            deleteUrl: appUrl+'/api/companies',
            modalActive: false,
            showToast: false,
            toastError: false,
            toastMessage: '',
            isCompanyLoading: true,
            searchTiemout: null,
        };

        this.getCompanies();
    }

    getCompanies () {
        const { 
            apiUrl, 
            searchValue 
        } = this.state;
        axios.get(apiUrl, {
            params: {
                shop: shop,
                searchValue: searchValue,
            }
        })
        .then(resp => {
            this.setState({
                items: resp.data,
                isCompanyLoading: false
            });
        })
        .catch(err => {
            console.log(err);
            this.handleToast('Error happened. Please reload and try again!', true);
        })
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.isLoading();
        });

        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.handleModalChange = this.handleModalChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.toggleToast = this.toggleToast.bind(this);
        this.handleToast = this.handleToast.bind(this);

        this.renderItem = this.renderItem.bind(this);
    }

    render () {
        const {
            redirectToAddCopmany,
            items,
            selectedItems,
            modalActive,
            showToast,
            toastError,
            toastMessage,
            isCompanyLoading,
        } = this.state;

        const toastMarkup = showToast ? <Toast content={toastMessage} onDismiss={this.toggleToast} error={toastError} /> : null;

        const resourceName = {
            singular: 'company',
            plural: 'companies',
        };

        const selectedItemsCount = selectedItems.length;
        
        const modalTitle = selectedItemsCount > 1 
            ? `Delete ${selectedItemsCount} ${resourceName.plural}?` 
            : `Delete ${selectedItemsCount} ${resourceName.singular}?`;

        const bulkActions = [
            {
                content: 'Delete company',
                onAction: () => this.handleModalChange(),
            },
        ];

        const primaryAction = {
            content: "Add company",
            onAction: () => this.handleAddCompanyRedirect(),
        }

        const filterControl = (
            <ResourceList.FilterControl
                searchValue={this.state.searchValue}
                onSearchChange={this.handleSearchChange.bind(this)}
            />
        );

        if (redirectToAddCopmany) {
            return <Redirect to='/company/create'/>;
        }

        return (
            <Page title="Companies" primaryAction={primaryAction}>
                <Card sectioned>
                    <ResourceList
                        resourceName={resourceName}
                        loading={isCompanyLoading}
                        items={items}
                        renderItem={this.renderItem}
                        selectedItems={this.state.selectedItems}
                        onSelectionChange={this.handleSelectionChange}
                        bulkActions={bulkActions}
                        filterControl={filterControl}
                    >
                    </ResourceList>
                </Card>

                <Modal
                    open={modalActive}
                    onClose={this.handleModalChange}
                    title={modalTitle}
                    primaryAction={{
                        content: 'Delete',
                        onAction: this.handleDelete,
                    }}
                    secondaryActions={[
                        {
                        content: 'Cancel',
                        onAction: this.handleModalChange,
                        },
                    ]}
                    >
                    <Modal.Section>
                        <TextContainer>
                            <p>Deleted company cannot be recovered. Do you still want to continue?</p>
                        </TextContainer>
                    </Modal.Section>
                </Modal>
                {toastMarkup}
            </Page>
        );
    }


    // search specification method
    handleSearchChange (searchValue) {
        // stop serch timeout if we keep typing
        if (searchValue != this.state.searchValue)
            clearTimeout(this.state.searchTiemout);

        var searchTiemout = setTimeout(() => {
            this.getCompanies();
        }, 1000);

        this.setState({
            searchValue: searchValue, 
            isCompanyLoading: true,
            searchTiemout :searchTiemout,
        });
    }

    // trigger when change items
    handleSelectionChange(selectedItems) {
        this.setState({
            selectedItems: selectedItems
        });
    }

    /// trigger when click create category btn
    handleAddCompanyRedirect() {
        const { redirectToAddCopmany } = this.state;
        this.setState({
            redirectToAddCopmany: !redirectToAddCopmany
        })
    }

    /// open/close modal
    handleModalChange () {
        this.setState(({modalActive}) => ({modalActive: !modalActive}));
    };

    // set data for toast
    handleToast(message, error) {
        this.setState({
            toastError: error,
            toastMessage: message,
        });

        // show toast
        this.toggleToast();
    }

    // open/close toast
    toggleToast() {
        this.setState(({ showToast }) => ({ showToast: !showToast }));
    };

    // thigger when delete category
    handleDelete() {
        const {deleteUrl, selectedItems, items} = this.state;

        axios.delete(deleteUrl, {
            params: {
                items: selectedItems,
                shop: shop,
            }
        })
        .then((resp) => {
            this.handleToast("Succesfuly deleted", false);

            // close modal
            this.handleModalChange();

            this.setState(({prevState}) => ({
                items: items.filter(item => selectedItems.indexOf(item.id) === -1),
                selectedItems: [],
            }));
        })
        .catch(err => {
            console.log(err)
            this.handleToast("Error happened", true);

            // close modal
            this.handleModalChange()
        });
    }

    renderItem(item) {
        const {id, name} = item;
        const media = <Avatar customer size="medium" name={name} />;

        return (
            <ResourceList.Item
                id={id}
                media={media}
                accessibilityLabel={`View details for ${name}`}
            >
                <div className="flexResourceContent">
                    <h3>
                        <TextStyle variation="strong">{name}</TextStyle>
                        {/* <div>{categoryMarkup}</div> */}
                    </h3>
                    <div>
                        <Link className="resourceLink" to={'/company/update/'+id} ><Button>Open</Button></Link>
                    </div>
                </div>
            </ResourceList.Item>
        );
    }
}


export default Companies;