import React, { Component } from "react";
import {
  Page,
  Card,
  Form,
  FormLayout,
  TextField,
  Button,
  Toast,
  ResourceList,
  Avatar,
  TextStyle
} from "@shopify/polaris";
import { Link } from "react-router-dom";
import axios from "axios";


class CompanyForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: null,
      name: "",
      ifs_id: "",
      customers: [],
      showToast: false,
      apiUrl: appUrl + '/api/company',
    };

    // this.getCategories();

    // this mean we update product
    if (this.props.match && this.props.match.params.id) {
      this.state.id = this.props.match.params.id;
      // load category
      this.getCompany();
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createCompany = this.createCompany.bind(this);

    this.toggleToast = this.toggleToast.bind(this);
    this.handleToast = this.handleToast.bind(this);
  }

  createCompany() {
    const { apiUrl, name, ifs_id } = this.state;

    axios.post(apiUrl, {
      name: name,
      ifs_id: ifs_id,
      shop: shop,
    })
      .then(resp => {
        this.handleToast(resp.data.message, resp.data.error);
      })
      .catch(error => {
        console.log(error);
        this.handleToast("Error happened", true);
      });
  }

  updateCompany() {
    const { apiUrl, name, ifs_id, id } = this.state;

    axios.put(apiUrl + '/' + id, {
      id: id,
      name: name,
      ifs_id: ifs_id,
      shop: shop,
    })
      .then(resp => {
        this.handleToast(resp.data.message, resp.data.error);
      })
      .catch(error => {
        console.log(error);
        this.handleToast("Error happened", true);
      });
  }

  getCompany() {
    const { apiUrl, id } = this.state;
    axios.get(apiUrl + '/' + id, {
      params: {
        shop: shop,
      }
    })
      .then(resp => {
        const data = resp.data;
        this.setState({
          name: data.name,
          ifs_id: data.ifs_id,
          customers: data.customers,
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  // getCategories() {
  //     const {apiUrl, category_id} = this.state;
  //     axios.get(apiUrl+'-categories', {
  //         params: {
  //             shop: shop,
  //         }
  //     })
  //     .then(resp => {
  //         this.setState({
  //             categories: resp.data,
  //         });
  //         if (!category_id && resp.data[0]) {
  //             this.setState({
  //                 category_id: resp.data[0].value,
  //             }); 
  //         }
  //     })
  //     .catch(err => {
  //         console.log(err);
  //     })
  // }

  render() {
    const {
      id,
      name,
      ifs_id,
      customers,
      showToast,
      toastError,
      toastMessage,
    } = this.state;
    const toastMarkup = showToast ? <Toast
      error={toastError}
      content={toastMessage}
      onDismiss={this.toggleToast} /> : null;

    const title = id ? "Update company" : "Add new company";

    const usersMarkup = customers.length ? this.renderCustomers(customers) : "";
    return (
      <Page title={title}>
        <Card sectioned>
          <Form onSubmit={this.handleSubmit} preventDefault={true}>
            <FormLayout>
              <TextField
                value={name}
                onChange={this.handleChange('name')}
                label="Company name"
                type="text"
              />
              <TextField
                value={ifs_id}
                onChange={this.handleChange('ifs_id')}
                label="Company IFS ID"
                type="text"
                multiline={true}
              />
              <Button submit>Submit</Button>
            </FormLayout>
          </Form>
          <br />
          <br />
          {usersMarkup}
          {toastMarkup}
        </Card>
      </Page>
    );
  }

  renderCustomers(customers) {
    return (
      <ResourceList
        resourceName={{ 'singilar': 'customer', 'plural': 'customers' }}
        items={customers}
        renderItem={(customer) => {
          const { id, first_name, last_name } = customer;
          const media = <Avatar customer size="medium" name={first_name} />;
          return (
            <ResourceList.Item
              id={id}
              media={media}
              accessibilityLabel={`View details for ${first_name}`}
            >
              <div className="flexResourceContent">
                <h3>
                  <TextStyle variation="strong">{first_name} {last_name}</TextStyle>
                </h3>
                <div>
                  <Link className="resourceLink" to={'/customer/update/' + id} ><Button>Open</Button></Link>
                </div>
              </div>
            </ResourceList.Item>
          );
        }}
      />
    );
  }

  // set data for toast
  handleToast(message, error) {
    this.setState({
      toastError: error,
      toastMessage: message,
    });

    // show toast
    this.toggleToast();
  }

  // open/close toast
  toggleToast() {
    this.setState(({ showToast }) => ({ showToast: !showToast }));
  };

  handleSubmit(event) {
    const { name, ifs_id, id } = this.state;
    let errors = [];
    if (name.trim() == '') {
      errors.push('Company name can not be blank.');
    }

    if (ifs_id == '') {
      errors.push('Ifs ID can not be blank.');
    }

    if (errors.length) {
      this.handleToast(errors.join(' '), true);
    } else {
      if (id) this.updateCompany();
      else this.createCompany();
    }
  };

  handleChange(field) {
    return (value) => this.setState({ [field]: value });
  };
}

export default CompanyForm;