import axios from 'axios'

import { API_URL } from './config'

const ApiService = {

    init() {
        // axios.defaults.baseURL = API_URL
        this.setHeader()
      },
    
      setHeader() {
        axios.defaults.headers.common['Content-Type'] = 'application/json'
        axios.defaults.headers.common['shop'] = window.shop
      },
    
      query(resource, params) {
        return axios
            .get(`${API_URL}/${resource}`, {
                params: params
            })
          // .catch((error) => {
          //   throw new Error(`[RWV] ApiService ${error}`)
          // })
      },
    
    get(resource, slug = '') {
        return axios
            .get(`${API_URL}/${resource}/${slug}`)
            .catch((error) => {
                throw new Error(`[RWV] ApiService ${error}`)
            })
    },
    
    post(resource, params) {
        return axios.post(`${API_URL}/${resource}`, params)
    },
    
    update(resource, slug, params) {
        return axios.put(`${API_URL}/${resource}/${slug}`, params)
    },
    
    put(resource, params) {
        return axios
            .put(`${API_URL}/${resource}`, params)
    },
    
    delete(resource) {
        return axios
            .delete(`${API_URL}/${resource}`)
            .catch((error) => {
                throw new Error(`[RWV] ApiService ${error}`)
            })
    },

    upload(resource, formdata, progress) {
        return axios.post(`${API_URL}/${resource}`, formdata, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: progress
            })
    }
}


ApiService.init()

export default ApiService

export const CsvImportService = {

    uploadVesa(params, progress) {
        return ApiService.upload('vesa/upload', params, progress)
    },

    uploadExceptions(params, progress) {
        return ApiService.upload('exceptions/upload', params, progress)
    },

    uploadAdaptors(params, progress) {
        return ApiService.upload('adaptors/upload', params, progress)
    },

    uploadMounts(params, progress) {
        return ApiService.upload('mounts/upload', params, progress)
    }
}