<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('syncLangifyFields', function () {
    // $shopId = $this->ask('What is your shop ID?');
    // $password = $this->secret('What is your password ?');
    // $name = $this->anticipate('What is your name?', ['Taylor', 'Dayle']);
    // $this->line('Display this on the screen');
    // $this->info('Name is : '. $name);
    // $this->info('ShopID is: '. $shopId);
    // $this->info('Password is: '. $password);

    $headers = ['First Name', 'Last Name', 'Email'];

    $users = App\Models\Customer::all(['first_name', 'last_name', 'email'])->toArray();

    $this->table($headers, $users);
});