<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});


Route::get('/products', 'ProductsController@index');
Route::get('/product/{id}', 'ProductsController@product');
Route::put('/product/{id}', 'ProductsController@productUpdate');
Route::get('/products-count', 'ProductsController@count');

Route::get('/product-specification', 'ProductsController@productSpecification');


Route::get('/categories', 'SpecificationCategoryController@index');
Route::get('/category/{id}', 'SpecificationCategoryController@show');
Route::post('/category', 'SpecificationCategoryController@store');
Route::put('/category/{id}', 'SpecificationCategoryController@update');
Route::delete('/categories', 'SpecificationCategoryController@destroy');


Route::get('/specifications', 'SpecificationsController@index');
Route::get('/specification/{id}', 'SpecificationsController@show');
Route::get('/specification-categories', 'SpecificationsController@getCategories');
Route::post('/specification', 'SpecificationsController@store');
Route::put('/specification/{id}', 'SpecificationsController@update');
Route::delete('/specifications', 'SpecificationsController@destroy');


Route::get('/companies',    'CompanyController@index');
Route::get('/company/{id}', 'CompanyController@show');
Route::post('/company',     'CompanyController@store');
Route::put('/company/{id}', 'CompanyController@update');
Route::delete('/companies', 'CompanyController@destroy');

Route::get('/customers', 'AdminCustomerController@index');
Route::get('/customers-companies', 'AdminCustomerController@customerCompanies');
Route::get('/customer/{id}', 'AdminCustomerController@show');
Route::post('/customer', 'AdminCustomerController@store');
Route::put('/customer/{id}', 'AdminCustomerController@update');
Route::delete('/customers', 'AdminCustomerController@destroy');
Route::get('/customers/update', 'AdminCustomerController@updateCustomers');


Route::get('/order_history/import', 'StorefrontOrderHistory@importOrders');
Route::post('/specification/import', 'SpecificationsController@import');

Route::post('/monitorMounts/import', 'StorefrontMountFinderController@monitorMountsImport');
Route::post('/mountAdaptors/import', 'StorefrontMountFinderController@mountAdaptorsImport');
Route::post('/mountExceptions/import', 'StorefrontMountFinderController@mountExceptionsImport');

Route::get('/invoices/sync', 'ShopifyCustomerInvoicesController@sync');

Route::get('/langify/sync', 'SyncController@langifySync');

// Route::prefix('storefront')->group(function () {
Route::group(['prefix' => 'storefront',  'middleware' => 'cors'], function () {

  // Storefront User Management
  Route::get('/customers-management', 'StorefrontCustomersHandlerController@getManagementCustomers');
  Route::post('/customers/create', 'StorefrontCustomersHandlerController@create');
  Route::post('/customers/admin-create', 'StorefrontCustomersHandlerController@adminCreate');
  Route::post('/customers/update', 'StorefrontCustomersHandlerController@update');
  Route::post('/customers/change-permissions', 'StorefrontCustomersHandlerController@changePermissions');
  Route::put('/customers/approve_deny', 'StorefrontCustomersHandlerController@approve_deny');
  Route::put('/customers/enable_disable', 'StorefrontCustomersHandlerController@enable_disable');
  Route::post('/customers/resend-invite', 'StorefrontCustomersHandlerController@resendInvite');
  Route::get('/customers/check-user', 'StorefrontCustomersHandlerController@checkUser');

  Route::post('/customers/join-existing-user', 'StorefrontCustomersHandlerController@joinExistingUser');
  Route::post('/customers/join-user-activate', 'StorefrontCustomersHandlerController@joinUserActivation');


  // Customer Invoice management
  Route::get('/customers/invoices', 'ShopifyCustomerInvoicesController@index');
  // Customer Order Management
  Route::get('/customers/order_history', 'StorefrontOrderHistory@index');
  Route::get('/customers/invoice/{orderID}', 'StorefrontOrderHistory@getInvoice');
  Route::get('/customers/order_history', 'StorefrontOrderHistory@index');
  Route::get('/customers/order_history/{id}', 'StorefrontOrderHistory@getOrder');
  Route::get('/customers/order_history-aws', 'StorefrontOrderHistory@aws');
  Route::post('/customers/order_history/cancel/{id}', 'StorefrontOrderHistory@cancel');
  
  // Customer Order Management 
  Route::post('/customers/order_history/returnRequest', 'StorefrontOrderHistory@orderReturnRequest');

  // MonitorFinder
  Route::get('/monitor-finder/search', 'StorefrontMountFinderController@index');
  Route::get('/monitor-finder/search-product', 'StorefrontMountFinderController@getProductByMountName');
  Route::get('/monitor-adapters/search', 'StorefrontMountFinderController@adapterSearch');

  // Storefront product specification
  Route::get('/product-specification/{handle?}', 'StorefrontProductSpecificationController@show');
  Route::get('/product-compare', 'StorefrontProductSpecificationController@compare');

  // Storefront Contact form
  Route::post('/contact/reserve_certified', 'StorefrontContactController@reserveCertified');
  Route::post('/contact/reserve_sales', 'StorefrontContactController@reserveSales');
  Route::post('/contact/hospitality', 'StorefrontContactController@hospitality');
  Route::post('/contact/trade-show-meeting', 'StorefrontContactController@tradeShowMeeting');
  Route::post('/contact/custom-solutions', 'StorefrontContactController@customSolutions');
  Route::post('/contact/literature_request', 'StorefrontContactController@literatureRequest');

  // Storefront Webinars
  Route::get('/webinars/get-events', 'StorefrontWebinarController@getEvents');
  Route::get('/webinars/get-event/{id}', 'StorefrontWebinarController@getEvent');
  Route::get('/webinars/get-join-url-meeting/{id}', 'StorefrontWebinarController@getJoinUrlMeeting');
  Route::post('/webinars/create-meeting-attendee/{id}', 'StorefrontWebinarController@createMeetingAttendee');

  // Configurators
  Route::get('/configurations/{id}', 'StorefrontConfiguratorController@configurations');
  Route::delete('/configurations/{id}', 'StorefrontConfiguratorController@deleteConfigurator');

  Route::get('/led-configurations/{id}', 'StorefrontConfiguratorController@ledConfigurations');
  Route::delete('/led-configurations/{id}', 'StorefrontConfiguratorController@ledDeleteConfigurator');
});

Route::group(['prefix' => 'storefront',  'middleware' => 'cors'], function () {
  Route::get('/invoices', 'ShopifyCustomerInvoicesController@show');
});

Route::prefix('storefront')->namespace('Storefront')->group(function() {
  Route::middleware(['cors'])->group(function() {
    Route::get('/vesa', 'VesaController@index');
  });
});

Route::namespace('ShopifyAdmin')->group(function() {
  Route::middleware('shopify_auth')->group(function() {
    Route::post('/vesa/upload', 'VesaController@upload');
    Route::post('/mounts/upload', 'MountsController@upload');
    Route::post('/adaptors/upload', 'AdaptorsController@upload');
    Route::post('/exceptions/upload', 'ExceptionsController@upload');
  });
});
Route::group(['prefix' => 'carrier-sipping'], function () {
  Route::post('/create', 'CarrierServiceController@create');
  Route::delete('/', 'CarrierServiceController@delete');
  Route::post('/', 'CarrierServiceController@index')->name('carrier_shipping');
});

Route::get('/test-customer', function () {
  return App\Models\Customer::join('customer_permissions', 'customer_permissions.customer_id', '=', 'customers.id')
      ->join('companies', 'customers.company_id', '=', 'companies.id')
      ->where('customers.id', 1066239787123)
      ->where('customers.shop_id', 1)
      ->first(['companies.name as company_name', 'customers.*', 
        'customer_permissions.*', \DB::raw('DATE_FORMAT(customers.created_at, "%m/%d/%Y") as formated_created_at'), 
        \DB::raw('DATE_FORMAT(customers.updated_at, "%m/%d/%Y") as formated_updated_at')
      ]);
});


Route::group(['prefix' => 'backup'], function () {
  Route::get('/create', 'BackupController@create');
  Route::get('/create_db', 'BackupController@create_db');
  Route::get('/send', 'BackupController@send');
  Route::get('/clean', 'BackupController@clean');
  // Route::get('/destroy', 'BackupController@destroy');
});

Route::group(['prefix' => 'storefront',  'middleware' => ['cors']], function () {
  Route::post('/captcha', function () {
    return response()->json([
      'success' => app('captcha')->check_api(request()->input('captcha'), request()->input('key'))
    ]);
  });
  Route::get('/captcha', function () {
    return response()->json(app('captcha')->create('default', true));
  });
});