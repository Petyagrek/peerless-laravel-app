<?php
// if (env('APP_ENV') === 'production') {
//     URL::forceSchema('https');
// }
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',[
	'uses' => 'HomeController@index',
	'as' => 'home',
]);

// Route::get('/admin/{path?}',[
// 	'uses' => 'HomeController@index',
// 	'as' => 'home',
//     'where' => ['path' => '.*'],
// ]);


Route::get('/test', function () {
	\iContactApi::getInstance()->setConfig(array(
		'appId' => 'hN8EpIkAv09S9NHGnisVC8tAmJNQxt9E',
		'apiPassword' => 'qwerty1234',
		'apiUsername' => 'Perrless Shopify Sandbox'
	));

	$oiContact = \iContactApi::getInstance();
	try {
		var_dump($oiContact->addContact('joe@shmoe.com', null, null, 'Joe', 'Shmoe', null, '123 Somewhere Ln', 'Apt 12', 'Somewhere', 'NW', '12345', '123-456-7890', '123-456-7890', null));
	} catch (Exception $oException) { // Catch any exceptions
		// Dump errors
		var_dump($oiContact->getErrors());
		// Grab the last raw request data
		var_dump($oiContact->getLastRequest());
		// Grab the last raw response data
		var_dump($oiContact->getLastResponse());
	}
	print_r($oiContact);
});

Route::get('/phpinfo', function () {
	phpinfo();
});

Route::get('/sendmail', function () {
	\Illuminate\Support\Facades\Mail::send([],[], function ($message) {
		$message->to('ivanchuda@gmail.com')
			->subject('Test')
			->setBody('Test mail');
	});
});